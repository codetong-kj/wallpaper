package com.ruoyi.common.utils;

import com.ruoyi.common.constant.Constants;

public class RedisKeyUtils {

    //保存用户点赞数据的key
    public static final String MAP_KEY_WORK_LIKED = "MAP_WORK_LIKED";
    //保存用户被点赞数量的key
    public static final String MAP_KEY_WORK_LIKED_COUNT = "MAP_WORK_LIKED_COUNT";
    private static final String CAPTCHA_CODE_KEY = "PHONE_CODE";
    //用户浏览作品记录
    private static final String MAP_KEY_WORK_VISIT = "WORK_VISIT";

    /**
     * 拼接被点赞的用户id和点赞的人的id作为key。格式 222222::333333
     *
     * @param likedWorkId 作品id
     * @param likedPostId 点赞的人的id
     * @return 格式化的string
     */
    public static String getLikedKey(String likedWorkId, String likedPostId) {
        return likedWorkId +
                "::" +
                likedPostId;
    }

    /**
     *
     * @param phoneNumber 手机号
     * @return 格式化的string
     */
    public static String getPhoneCodeKey(String phoneNumber) {
        return CAPTCHA_CODE_KEY+":"+phoneNumber;
    }

    /**
     *
     * @param phoneNumber 手机号
     * @return 格式化的string
     */
//    public static String getVisitKey(String phoneNumber) {
//        return CAPTCHA_CODE_KEY+":"+phoneNumber;
//    }
}
