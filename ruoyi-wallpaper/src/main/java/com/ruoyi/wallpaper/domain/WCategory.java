package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文件分类对象 w_category
 * 
 * @author weiwei
 * @date 2022-03-29
 */
public class WCategory extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 类别id */
    private Integer cId;

    /** 类别名称 */
    @Excel(name = "类别名称")
    private String cName;

    /** 类别排序 */
    @Excel(name = "类别排序")
    private Integer sort;

    /** 所属类别 */
    @Excel(name = "所属类别")
    private Integer type;

    public void setcId(Integer cId) 
    {
        this.cId = cId;
    }

    public Integer getcId() 
    {
        return cId;
    }
    public void setcName(String cName) 
    {
        this.cName = cName;
    }

    public String getcName() 
    {
        return cName;
    }
    public void setSort(Integer sort) 
    {
        this.sort = sort;
    }

    public Integer getSort() 
    {
        return sort;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("cId", getcId())
            .append("cName", getcName())
            .append("sort", getSort())
            .append("type", getType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
