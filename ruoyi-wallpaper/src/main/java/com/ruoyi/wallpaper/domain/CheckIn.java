package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 签到对象 check_in
 *
 * @author weiwei
 * @date 2022-04-17
 */
public class CheckIn extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Integer id;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** cookie */
    @Excel(name = "cookie")
    private String cookie;

    /** 姓名 */
    @Excel(name = "姓名")
    private String uname;

    /** 纬度 */
    @Excel(name = "纬度")
    private String lat;

    /** 经度 */
    @Excel(name = "经度")
    private String lng;

    /** 不知道 */
    @Excel(name = "不知道")
    private String acc;

    /** 不晓得 */
    @Excel(name = "不晓得")
    private String res;

    /** 状态 */
    @Excel(name = "状态")
    private Integer status;

    /** gps地址 */
    @Excel(name = "gps地址")
    private String gpsAddr;

    /** 班级ID */
    @Excel(name = "班级ID")
    private Integer courseId;

    public void setId(Integer id)
    {
        this.id = id;
    }

    public Integer getId()
    {
        return id;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId()
    {
        return userId;
    }
    public void setCookie(String cookie)
    {
        this.cookie = cookie;
    }

    public String getCookie()
    {
        return cookie;
    }
    public void setUname(String uname)
    {
        this.uname = uname;
    }

    public String getUname()
    {
        return uname;
    }
    public void setLat(String lat)
    {
        this.lat = lat;
    }

    public String getLat()
    {
        return lat;
    }
    public void setLng(String lng)
    {
        this.lng = lng;
    }

    public String getLng()
    {
        return lng;
    }
    public void setAcc(String acc)
    {
        this.acc = acc;
    }

    public String getAcc()
    {
        return acc;
    }
    public void setRes(String res)
    {
        this.res = res;
    }

    public String getRes()
    {
        return res;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }
    public void setGpsAddr(String gpsAddr)
    {
        this.gpsAddr = gpsAddr;
    }

    public String getGpsAddr()
    {
        return gpsAddr;
    }
    public void setCourseId(Integer courseId)
    {
        this.courseId = courseId;
    }

    public Integer getCourseId()
    {
        return courseId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("userId", getUserId())
                .append("cookie", getCookie())
                .append("uname", getUname())
                .append("lat", getLat())
                .append("lng", getLng())
                .append("acc", getAcc())
                .append("res", getRes())
                .append("status", getStatus())
                .append("gpsAddr", getGpsAddr())
                .append("courseId", getCourseId())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}