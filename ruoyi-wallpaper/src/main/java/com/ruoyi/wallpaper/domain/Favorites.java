package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 个人收藏对象 favorites
 *
 * @author weiwei
 * @date 2022-04-07
 */
public class Favorites extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Integer fId;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 收藏夹名字 */
    @Excel(name = "收藏夹名字")
    private String fName;

    /** 排序 */
    @Excel(name = "排序")
    private Integer sort;

    /** 封面 */
    @Excel(name = "封面")
    private String cover;

    /** 介绍 */
    @Excel(name = "介绍")
    private String introduce;

    /** 是否公开 */
    @Excel(name = "是否公开")
    private Integer isOpen;

    /** 0默认图片收藏夹1默认视频收藏夹2用户视频收藏夹 */
    @Excel(name = "0默认图片收藏夹1默认视频收藏夹2用户视频收藏夹")
    private Integer type;

    /** 数量 */
    @Excel(name = "数量")
    private Integer count;

    public void setfId(Integer fId)
    {
        this.fId = fId;
    }

    public Integer getfId()
    {
        return fId;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId()
    {
        return userId;
    }
    public void setfName(String fName)
    {
        this.fName = fName;
    }

    public String getfName()
    {
        return fName;
    }
    public void setSort(Integer sort)
    {
        this.sort = sort;
    }

    public Integer getSort()
    {
        return sort;
    }
    public void setCover(String cover)
    {
        this.cover = cover;
    }

    public String getCover()
    {
        return cover;
    }
    public void setIntroduce(String introduce)
    {
        this.introduce = introduce;
    }

    public String getIntroduce()
    {
        return introduce;
    }
    public void setIsOpen(Integer isOpen)
    {
        this.isOpen = isOpen;
    }

    public Integer getIsOpen()
    {
        return isOpen;
    }
    public void setType(Integer type)
    {
        this.type = type;
    }

    public Integer getType()
    {
        return type;
    }
    public void setCount(Integer count)
    {
        this.count = count;
    }

    public Integer getCount()
    {
        return count;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("fId", getfId())
                .append("userId", getUserId())
                .append("fName", getfName())
                .append("sort", getSort())
                .append("cover", getCover())
                .append("introduce", getIntroduce())
                .append("isOpen", getIsOpen())
                .append("type", getType())
                .append("count", getCount())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}