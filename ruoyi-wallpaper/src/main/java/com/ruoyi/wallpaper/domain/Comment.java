package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户评论对象 comment
 * 
 * @author weiwei
 * @date 2022-02-22
 */
public class Comment extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 评论id */
    private Long commentId;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer isDelete;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 点赞次数 */
    @Excel(name = "点赞次数")
    private Long commentLikeCount;

    /** 顶级评论id */
    @Excel(name = "顶级评论id")
    private Long rootCommentId;

    /** 回复目标评论id */
    @Excel(name = "回复目标评论id")
    private Long toCommentId;

    /** 评论的作品id */
    @Excel(name = "评论的作品id")
    private String toWorkId;

    public void setCommentId(Long commentId) 
    {
        this.commentId = commentId;
    }

    public Long getCommentId() 
    {
        return commentId;
    }
    public void setContent(String content) 
    {
        this.content = content;
    }

    public String getContent() 
    {
        return content;
    }
    public void setIsDelete(Integer isDelete) 
    {
        this.isDelete = isDelete;
    }

    public Integer getIsDelete() 
    {
        return isDelete;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setCommentLikeCount(Long commentLikeCount) 
    {
        this.commentLikeCount = commentLikeCount;
    }

    public Long getCommentLikeCount() 
    {
        return commentLikeCount;
    }
    public void setRootCommentId(Long rootCommentId) 
    {
        this.rootCommentId = rootCommentId;
    }

    public Long getRootCommentId() 
    {
        return rootCommentId;
    }
    public void setToCommentId(Long toCommentId) 
    {
        this.toCommentId = toCommentId;
    }

    public Long getToCommentId() 
    {
        return toCommentId;
    }
    public void setToWorkId(String toWorkId) 
    {
        this.toWorkId = toWorkId;
    }

    public String getToWorkId() 
    {
        return toWorkId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("commentId", getCommentId())
            .append("content", getContent())
            .append("isDelete", getIsDelete())
            .append("userId", getUserId())
            .append("commentLikeCount", getCommentLikeCount())
            .append("rootCommentId", getRootCommentId())
            .append("toCommentId", getToCommentId())
            .append("toWorkId", getToWorkId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
