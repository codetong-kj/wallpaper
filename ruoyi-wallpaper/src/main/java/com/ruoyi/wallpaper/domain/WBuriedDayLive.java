package com.ruoyi.wallpaper.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 埋点日活对象 w_buried_day_live
 * 
 * @author weiwei
 * @date 2021-12-23
 */
public class WBuriedDayLive extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long dId;

    /** 埋点id */
    @Excel(name = "埋点id")
    private Long bId;

    /** 启动时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "启动时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 关闭时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "关闭时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** cpu */
    @Excel(name = "cpu")
    private String cpu;

    /** 分辨率 */
    @Excel(name = "分辨率")
    private String dpi;

    public void setdId(Long dId) 
    {
        this.dId = dId;
    }

    public Long getdId() 
    {
        return dId;
    }
    public void setbId(Long bId) 
    {
        this.bId = bId;
    }

    public Long getbId() 
    {
        return bId;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setCpu(String cpu) 
    {
        this.cpu = cpu;
    }

    public String getCpu() 
    {
        return cpu;
    }
    public void setDpi(String dpi) 
    {
        this.dpi = dpi;
    }

    public String getDpi() 
    {
        return dpi;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("dId", getdId())
            .append("bId", getbId())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("cpu", getCpu())
            .append("dpi", getDpi())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
