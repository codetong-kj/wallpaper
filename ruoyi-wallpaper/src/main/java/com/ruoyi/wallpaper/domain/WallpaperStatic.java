package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 静态壁纸对象 wallpaper_static
 * 
 * @author weiwei
 * @date 2021-11-10
 */
public class WallpaperStatic extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 壁纸id */
    private String wId;

    /** 文件名 */
    @Excel(name = "文件名")
    private String fileName;

    /** 类别 */
    @Excel(name = "类别")
    private String type;

    /** 描述 */
    @Excel(name = "描述")
    private String description;

    /** 收藏数 */
    @Excel(name = "收藏数")
    private Long stored;

    public void setwId(String wId) 
    {
        this.wId = wId;
    }

    public String getwId() 
    {
        return wId;
    }
    public void setFileName(String fileName) 
    {
        this.fileName = fileName;
    }

    public String getFileName() 
    {
        return fileName;
    }
    public void setType(String type) 
    {
        this.type = type;
    }

    public String getType() 
    {
        return type;
    }
    public void setDescription(String description) 
    {
        this.description = description;
    }

    public String getDescription() 
    {
        return description;
    }
    public void setStored(Long stored) 
    {
        this.stored = stored;
    }

    public Long getStored() 
    {
        return stored;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("wId", getwId())
            .append("fileName", getFileName())
            .append("type", getType())
            .append("description", getDescription())
            .append("stored", getStored())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
