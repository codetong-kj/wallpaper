package com.ruoyi.wallpaper.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 埋点用户登录对象 w_buried_login
 *
 * @author weiwei
 * @date 2021-12-23
 */
public class WBuriedLogin extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long lId;

    /** 登录时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "登录时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date loginTime;

    /** 埋点id */
    @Excel(name = "埋点id")
    private Long bId;

    /** 退出时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "退出时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date exitTime;

    /** 登录类型 */
    @Excel(name = "登录类型")
    private Integer loginType;

    public void setlId(Long lId)
    {
        this.lId = lId;
    }

    public Long getlId()
    {
        return lId;
    }
    public void setLoginTime(Date loginTime)
    {
        this.loginTime = loginTime;
    }

    public Date getLoginTime()
    {
        return loginTime;
    }
    public void setbId(Long bId)
    {
        this.bId = bId;
    }

    public Long getbId()
    {
        return bId;
    }
    public void setExitTime(Date exitTime)
    {
        this.exitTime = exitTime;
    }

    public Date getExitTime()
    {
        return exitTime;
    }
    public void setLoginType(Integer loginType)
    {
        this.loginType = loginType;
    }

    public Integer getLoginType()
    {
        return loginType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("lId", getlId())
                .append("loginTime", getLoginTime())
                .append("bId", getbId())
                .append("exitTime", getExitTime())
                .append("loginType", getLoginType())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}