package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户点赞对象 user_like
 * 
 * @author weiwei
 * @date 2022-03-13
 */
public class UserLike extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Integer id;

    /** 被点赞的用户的作品id */
    @Excel(name = "被点赞的用户的作品id")
    private String likedWorkId;

    public UserLike(String likedWorkId, String likedPostId, Integer status) {
        this.likedWorkId = likedWorkId;
        this.likedPostId = likedPostId;
        this.status = status;
    }

    public UserLike(Integer id, String likedWorkId, String likedPostId, Integer status) {
        this.id = id;
        this.likedWorkId = likedWorkId;
        this.likedPostId = likedPostId;
        this.status = status;
    }

    public UserLike() {
    }

    /** 点赞的用户id */
    @Excel(name = "点赞的用户id")
    private String likedPostId;

    /** 点赞状态，0取消，1点赞 */
    @Excel(name = "点赞状态，0取消，1点赞")
    private Integer status;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setLikedWorkId(String likedWorkId) 
    {
        this.likedWorkId = likedWorkId;
    }

    public String getLikedWorkId() 
    {
        return likedWorkId;
    }
    public void setLikedPostId(String likedPostId) 
    {
        this.likedPostId = likedPostId;
    }

    public String getLikedPostId() 
    {
        return likedPostId;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("likedWorkId", getLikedWorkId())
            .append("likedPostId", getLikedPostId())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
