package com.ruoyi.wallpaper.domain;

import com.ruoyi.common.annotation.Excel;

public class IpResult {

    /**
     * 城市
     */
    private String city;
    /**
     * 维度
     */
    private Double lat;

    /**
     * 经度
     */
    private Double lon;
    /**
     * 数量
     */
    private Integer total;

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Double getLat() {
        return lat;
    }

    public void setLat(Double lat) {
        this.lat = lat;
    }

    public Double getLon() {
        return lon;
    }

    public void setLon(Double lon) {
        this.lon = lon;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public IpResult() {
    }

    public IpResult(String city, Double lat, Double lon, Integer total) {
        this.city = city;
        this.lat = lat;
        this.lon = lon;
        this.total = total;
    }
}
