package com.ruoyi.wallpaper.domain;


import com.ruoyi.common.core.domain.BaseEntity;

/**
 * @author ke
 */
public class WorkResult extends BaseEntity {
    private String workId;
    private String userId;
    private String title;
    private String like;
    private String downloadUrl;
    private String nickName;
    private String avatar;
    private Integer flag;
    private Integer type;

    @Override
    public String toString() {
        return "WorkResult{" +
                "workId='" + workId + '\'' +
                ", userId='" + userId + '\'' +
                ", title='" + title + '\'' +
                ", like='" + like + '\'' +
                ", downloadUrl='" + downloadUrl + '\'' +
                ", nickName='" + nickName + '\'' +
                ", avatar='" + avatar + '\'' +
                ", flag=" + flag +
                ", type=" + type +
                '}';
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLike() {
        return like;
    }

    public void setLike(String like) {
        this.like = like;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public Integer getFlag() {
        return flag;
    }

    public void setFlag(Integer flag) {
        this.flag = flag;
    }
}
