package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 社会化用户 & 系统用户关系对象 social_user_auth
 * 
 * @author weiwei
 * @date 2022-01-12
 */
public class SocialUserAuth extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Integer id;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 社会化用户ID */
    @Excel(name = "社会化用户ID")
    private String socialUserId;

    public void setId(Integer id) 
    {
        this.id = id;
    }

    public Integer getId() 
    {
        return id;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setSocialUserId(String socialUserId) 
    {
        this.socialUserId = socialUserId;
    }

    public String getSocialUserId() 
    {
        return socialUserId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("userId", getUserId())
            .append("socialUserId", getSocialUserId())
            .toString();
    }
}
