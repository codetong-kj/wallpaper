package com.ruoyi.wallpaper.domain;

import java.util.List;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户作品对象 w_user_works
 * 
 * @author weiwei
 * @date 2022-02-11
 */
public class WUserWorks extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 作品id，对应w_pid */
    private String workId;

    /** 作品类型1 dinamci或0 static */
    @Excel(name = "作品类型1 dinamci或0 static")
    private Integer type;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 类别id */
    @Excel(name = "类别id")
    private Integer cId;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 简介 */
    @Excel(name = "简介")
    private String about;

    /** 点赞 */
    @Excel(name = "点赞")
    private Integer like;

    /** 收藏 */
    @Excel(name = "收藏")
    private Integer collection;

    /** 浏览 */
    @Excel(name = "浏览")
    private Integer visit;

    /** 标志 */
    @Excel(name = "标志")
    private Integer flag;

    /** 文件信息 */
    private List<WFile> wFileList;

    public void setWorkId(String workId) 
    {
        this.workId = workId;
    }

    public String getWorkId() 
    {
        return workId;
    }
    public void setType(Integer type) 
    {
        this.type = type;
    }

    public Integer getType() 
    {
        return type;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }
    public void setcId(Integer cId) 
    {
        this.cId = cId;
    }

    public Integer getcId() 
    {
        return cId;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setAbout(String about) 
    {
        this.about = about;
    }

    public String getAbout() 
    {
        return about;
    }
    public void setLike(Integer like) 
    {
        this.like = like;
    }

    public Integer getLike() 
    {
        return like;
    }
    public void setCollection(Integer collection) 
    {
        this.collection = collection;
    }

    public Integer getCollection() 
    {
        return collection;
    }
    public void setVisit(Integer visit) 
    {
        this.visit = visit;
    }

    public Integer getVisit() 
    {
        return visit;
    }
    public void setFlag(Integer flag) 
    {
        this.flag = flag;
    }

    public Integer getFlag() 
    {
        return flag;
    }

    public List<WFile> getWFileList()
    {
        return wFileList;
    }

    public void setWFileList(List<WFile> wFileList)
    {
        this.wFileList = wFileList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("workId", getWorkId())
            .append("type", getType())
            .append("userId", getUserId())
            .append("cId", getcId())
            .append("title", getTitle())
            .append("about", getAbout())
            .append("like", getLike())
            .append("collection", getCollection())
            .append("visit", getVisit())
            .append("flag", getFlag())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("wFileList", getWFileList())
            .toString();
    }
}
