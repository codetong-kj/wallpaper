package com.ruoyi.wallpaper.domain;

import java.math.BigDecimal;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 埋点支付对象 w_buried_pay
 * 
 * @author weiwei
 * @date 2021-12-23
 */
public class WBuriedPay extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long vId;

    /** 埋点id */
    @Excel(name = "埋点id")
    private Long bId;

    /** 支付类型（什么卡） */
    @Excel(name = "支付类型", readConverterExp = "什=么卡")
    private Integer form;

    /** 支付金额 */
    @Excel(name = "支付金额")
    private BigDecimal money;

    /** 是否成功 */
    @Excel(name = "是否成功")
    private Integer isSuccess;

    /** 支付类型 */
    @Excel(name = "支付类型")
    private Integer payType;

    public void setvId(Long vId) 
    {
        this.vId = vId;
    }

    public Long getvId() 
    {
        return vId;
    }
    public void setbId(Long bId) 
    {
        this.bId = bId;
    }

    public Long getbId() 
    {
        return bId;
    }
    public void setForm(Integer form) 
    {
        this.form = form;
    }

    public Integer getForm() 
    {
        return form;
    }
    public void setMoney(BigDecimal money) 
    {
        this.money = money;
    }

    public BigDecimal getMoney() 
    {
        return money;
    }
    public void setIsSuccess(Integer isSuccess) 
    {
        this.isSuccess = isSuccess;
    }

    public Integer getIsSuccess() 
    {
        return isSuccess;
    }
    public void setPayType(Integer payType) 
    {
        this.payType = payType;
    }

    public Integer getPayType() 
    {
        return payType;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("vId", getvId())
            .append("bId", getbId())
            .append("form", getForm())
            .append("money", getMoney())
            .append("isSuccess", getIsSuccess())
            .append("payType", getPayType())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
