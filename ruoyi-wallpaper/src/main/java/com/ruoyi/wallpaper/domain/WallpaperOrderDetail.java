package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 订单明细对象 wallpaper_order_detail
 *
 * @author weiwei
 * @date 2021-12-17
 */
public class WallpaperOrderDetail extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 订单号 */
    @Excel(name = "订单号")
    private String orderNumber;

    /** 订单名称 */
    @Excel(name = "订单名称")
    private String ordertitle;

    /** 外键，用户id */
    @Excel(name = "外键，用户id")
    private String userId;

    /** 交易流水号 */
    @Excel(name = "交易流水号")
    private String tradeno;

    /** 付款人 */
    @Excel(name = "付款人")
    private String username;

    /** 付款金额 */
    @Excel(name = "付款金额")
    private String price;

    /** 付款方式：1支付宝，2微信 */
    @Excel(name = "付款方式：1支付宝，2微信")
    private String paymethod;

    public void setId(Long id)
    {
        this.id = id;
    }

    public Long getId()
    {
        return id;
    }
    public void setOrderNumber(String orderNumber)
    {
        this.orderNumber = orderNumber;
    }

    public String getOrderNumber()
    {
        return orderNumber;
    }
    public void setOrdertitle(String ordertitle)
    {
        this.ordertitle = ordertitle;
    }

    public String getOrdertitle()
    {
        return ordertitle;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId()
    {
        return userId;
    }
    public void setTradeno(String tradeno)
    {
        this.tradeno = tradeno;
    }

    public String getTradeno()
    {
        return tradeno;
    }
    public void setUsername(String username)
    {
        this.username = username;
    }

    public String getUsername()
    {
        return username;
    }
    public void setPrice(String price)
    {
        this.price = price;
    }

    public String getPrice()
    {
        return price;
    }
    public void setPaymethod(String paymethod)
    {
        this.paymethod = paymethod;
    }

    public String getPaymethod()
    {
        return paymethod;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("id", getId())
                .append("orderNumber", getOrderNumber())
                .append("ordertitle", getOrdertitle())
                .append("userId", getUserId())
                .append("tradeno", getTradeno())
                .append("createTime", getCreateTime())
                .append("updateTime", getUpdateTime())
                .append("username", getUsername())
                .append("price", getPrice())
                .append("paymethod", getPaymethod())
                .toString();
    }
}