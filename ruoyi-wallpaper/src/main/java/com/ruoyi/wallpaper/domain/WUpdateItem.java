package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 更新明细对象 w_update_item
 * 
 * @author weiwei
 * @date 2021-11-18
 */
public class WUpdateItem extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long itemId;

    /** 外键 */
    @Excel(name = "外键")
    private Long updateId;

    /** 标识 */
    @Excel(name = "标识")
    private String symbel;

    /** md5 */
    @Excel(name = "md5")
    private String md;

    /** 下载链接 */
    @Excel(name = "下载链接")
    private String downloadUrl;

    public void setItemId(Long itemId) 
    {
        this.itemId = itemId;
    }

    public Long getItemId() 
    {
        return itemId;
    }
    public void setUpdateId(Long updateId) 
    {
        this.updateId = updateId;
    }

    public Long getUpdateId() 
    {
        return updateId;
    }
    public void setSymbel(String symbel) 
    {
        this.symbel = symbel;
    }

    public String getSymbel() 
    {
        return symbel;
    }
    public void setMd(String md) 
    {
        this.md = md;
    }

    public String getMd() 
    {
        return md;
    }
    public void setDownloadUrl(String downloadUrl) 
    {
        this.downloadUrl = downloadUrl;
    }

    public String getDownloadUrl() 
    {
        return downloadUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("itemId", getItemId())
            .append("updateId", getUpdateId())
            .append("symbel", getSymbel())
            .append("md", getMd())
            .append("downloadUrl", getDownloadUrl())
            .toString();
    }
}
