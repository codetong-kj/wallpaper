package com.ruoyi.wallpaper.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 用户评论对象 comment
 * 
 * @author weiwei
 * @date 2022-02-22
 */
public class CommentResult extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 评论id */
    private Long commentId;

    /** 内容 */
    @Excel(name = "内容")
    private String content;

    /** 是否删除 */
    @Excel(name = "是否删除")
    private Integer isDelete;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 点赞次数 */
    @Excel(name = "点赞次数")
    private Long commentLikeCount;

    /** 顶级评论id */
    @Excel(name = "顶级评论id")
    private Long rootCommentId;

    /** 回复目标评论id */
    @Excel(name = "回复目标评论id")
    private Long toCommentId;

    /** 评论的作品id */
    @Excel(name = "评论的作品id")
    private String toWorkId;

    /** 用户昵称 */
    @Excel(name = "用户昵称")
    private String nickName;
    /** 头像地址 */
    @Excel(name = "头像地址")
    private String avatar;

    @Excel(name="子评论的数目")
    private Integer commentCount;



    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getCommentId() {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getIsDelete() {
        return isDelete;
    }

    public void setIsDelete(Integer isDelete) {
        this.isDelete = isDelete;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Long getCommentLikeCount() {
        return commentLikeCount;
    }

    public void setCommentLikeCount(Long commentLikeCount) {
        this.commentLikeCount = commentLikeCount;
    }

    public Long getRootCommentId() {
        return rootCommentId;
    }

    public void setRootCommentId(Long rootCommentId) {
        this.rootCommentId = rootCommentId;
    }

    public Long getToCommentId() {
        return toCommentId;
    }

    public void setToCommentId(Long toCommentId) {
        this.toCommentId = toCommentId;
    }

    public String getToWorkId() {
        return toWorkId;
    }

    public void setToWorkId(String toWorkId) {
        this.toWorkId = toWorkId;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public CommentResult() {
    }

    public CommentResult(Long commentId, String content, Integer isDelete, String userId, Long commentLikeCount, Long rootCommentId, Long toCommentId, String toWorkId, String nickName, String avatar, Integer commentCount) {
        this.commentId = commentId;
        this.content = content;
        this.isDelete = isDelete;
        this.userId = userId;
        this.commentLikeCount = commentLikeCount;
        this.rootCommentId = rootCommentId;
        this.toCommentId = toCommentId;
        this.toWorkId = toWorkId;
        this.nickName = nickName;
        this.avatar = avatar;
        this.commentCount = commentCount;
    }

    public Integer getCommentCount() {
        return commentCount;
    }

    public void setCommentCount(Integer commentCount) {
        this.commentCount = commentCount;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("commentId", getCommentId())
            .append("content", getContent())
            .append("isDelete", getIsDelete())
            .append("userId", getUserId())
            .append("commentLikeCount", getCommentLikeCount())
            .append("rootCommentId", getRootCommentId())
            .append("toCommentId", getToCommentId())
            .append("toWorkId", getToWorkId())
            .append("nickName", getNickName())
            .append("avatar", getAvatar())
            .append("commentCount", getCommentCount())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
