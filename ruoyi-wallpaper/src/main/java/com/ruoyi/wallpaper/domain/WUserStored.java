package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户壁纸库存中间对象 w_user_stored
 * 
 * @author weiwei
 * @date 2021-12-17
 */
public class WUserStored extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 壁纸id */
    @Excel(name = "壁纸id")
    private String wPid;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    public void setwPid(String wPid) 
    {
        this.wPid = wPid;
    }

    public String getwPid() 
    {
        return wPid;
    }
    public void setUserId(String userId) 
    {
        this.userId = userId;
    }

    public String getUserId() 
    {
        return userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("wPid", getwPid())
            .append("userId", getUserId())
            .toString();
    }
}
