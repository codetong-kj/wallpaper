package com.ruoyi.wallpaper.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 公告管理对象 announce
 * 
 * @author weiwei
 * @date 2022-03-28
 */
public class Announce extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 公告id */
    private Integer aid;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 是否需要评论 */
    @Excel(name = "是否需要评论")
    private Integer needComment;

    /** 开始时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 结束时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "结束时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 类型 */
    @Excel(name = "类型")
    private String category;

    /** 状态0不可用1可用 */
    @Excel(name = "状态0不可用1可用")
    private Integer status;

    /** 公告信息信息 */
    private List<AnnounceInfo> announceInfoList;

    public void setAid(Integer aid) 
    {
        this.aid = aid;
    }

    public Integer getAid() 
    {
        return aid;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setNeedComment(Integer needComment) 
    {
        this.needComment = needComment;
    }

    public Integer getNeedComment() 
    {
        return needComment;
    }
    public void setStartTime(Date startTime) 
    {
        this.startTime = startTime;
    }

    public Date getStartTime() 
    {
        return startTime;
    }
    public void setEndTime(Date endTime) 
    {
        this.endTime = endTime;
    }

    public Date getEndTime() 
    {
        return endTime;
    }
    public void setCategory(String category) 
    {
        this.category = category;
    }

    public String getCategory() 
    {
        return category;
    }
    public void setStatus(Integer status) 
    {
        this.status = status;
    }

    public Integer getStatus() 
    {
        return status;
    }

    public List<AnnounceInfo> getAnnounceInfoList()
    {
        return announceInfoList;
    }

    public void setAnnounceInfoList(List<AnnounceInfo> announceInfoList)
    {
        this.announceInfoList = announceInfoList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("aid", getAid())
            .append("title", getTitle())
            .append("needComment", getNeedComment())
            .append("startTime", getStartTime())
            .append("endTime", getEndTime())
            .append("category", getCategory())
            .append("status", getStatus())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .append("announceInfoList", getAnnounceInfoList())
            .toString();
    }
}
