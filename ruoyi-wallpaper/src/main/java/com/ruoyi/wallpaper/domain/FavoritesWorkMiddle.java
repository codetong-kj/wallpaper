package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 收藏夹与作品之间的关联对象 favorites_work_middle
 * 
 * @author weiwei
 * @date 2022-03-16
 */
public class FavoritesWorkMiddle extends BaseEntity
{
    private static final long serialVersionUID = 1L;



    /** 收藏夹中间表id */
    private Integer mId;

    /** 收藏夹id */
    private Integer fId;

    /** 作品id */
    @Excel(name = "作品id")
    private String workId;

    public void setfId(Integer fId) 
    {
        this.fId = fId;
    }

    public Integer getfId() 
    {
        return fId;
    }
    public void setWorkId(String workId) 
    {
        this.workId = workId;
    }

    public String getWorkId() 
    {
        return workId;
    }
    public Integer getmId() {
        return mId;
    }

    public void setmId(Integer mId) {
        this.mId = mId;
    }
    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("mId", getmId())
            .append("fId", getfId())
            .append("workId", getWorkId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
