package com.ruoyi.wallpaper.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 文件对象 w_file
 *
 * @author weiwei
 * @date 2022-02-11
 */
public class WFileVideoInfo extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 文件id
     */
    private String fId;

    /**
     * 外键，作品id
     */
    @Excel(name = "外键，作品id")
    private String workId;

    /**
     * 文件名
     */
    @Excel(name = "文件名")
    private String fileName;

    /**
     * 下载链接
     */
    @Excel(name = "下载链接")
    private String downloadUrl;
    /**
     * 缩略图
     */
    @Excel(name = "缩略图")
    private String thumbnail;
    /**
     * 视频时长
     */
    @Excel(name = "视频时长")
    private String duration;
    /**
     * 视频高度
     */
    @Excel(name = "视频高度")
    private Integer height;
    /**
     * 视频宽度
     */
    @Excel(name = "视频宽度")
    private Integer width;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getHeight() {
        return height;
    }

    public void setHeight(Integer height) {
        this.height = height;
    }

    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
        this.width = width;
    }

    public String getfId() {
        return fId;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public String getWorkId() {
        return workId;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setDownloadUrl(String downloadUrl) {
        this.downloadUrl = downloadUrl;
    }

    public String getDownloadUrl() {
        return downloadUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("fId", getfId())
                .append("workId", getWorkId())
                .append("fileName", getFileName())
                .append("downloadUrl", getDownloadUrl())
                .append("thumbnail", getThumbnail())
                .append("duration", getDuration())
                .append("width", getWidth())
                .append("height", getHeight())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
