package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 文件对象 w_file
 * 
 * @author weiwei
 * @date 2022-02-11
 */
public class WFile extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 文件id */
    private String fId;

    /** 外键，作品id */
    @Excel(name = "外键，作品id")
    private String workId;

    /** 文件名 */
    @Excel(name = "文件名")
    private String fileName;

    /** 下载链接 */
    @Excel(name = "下载链接")
    private String downloadUrl;

    public String getfId() {
        return fId;
    }

    public void setfId(String fId) {
        this.fId = fId;
    }

    public void setWorkId(String workId)
    {
        this.workId = workId;
    }

    public String getWorkId() 
    {
        return workId;
    }
    public void setFileName(String fileName) 
    {
        this.fileName = fileName;
    }

    public String getFileName() 
    {
        return fileName;
    }
    public void setDownloadUrl(String downloadUrl) 
    {
        this.downloadUrl = downloadUrl;
    }

    public String getDownloadUrl() 
    {
        return downloadUrl;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("fId", getfId())
            .append("workId", getWorkId())
            .append("fileName", getFileName())
            .append("downloadUrl", getDownloadUrl())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
