package com.ruoyi.wallpaper.domain;

public class LikedCountDTO {
    String workId;
    Integer sum;

    @Override
    public String toString() {
        return "LikedCountDTO{" +
                "workId='" + workId + '\'' +
                ", sum=" + sum +
                '}';
    }

    public String getWorkId() {
        return workId;
    }

    public void setWorkId(String workId) {
        this.workId = workId;
    }

    public Integer getSum() {
        return sum;
    }

    public void setSum(Integer sum) {
        this.sum = sum;
    }

    public LikedCountDTO() {
    }

    public LikedCountDTO(String workId, Integer sum) {
        this.workId = workId;
        this.sum = sum;
    }
}
