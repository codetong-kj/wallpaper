package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 视频详情对象 w_video_info
 * 
 * @author weiwei
 * @date 2022-02-28
 */
public class WVideoInfo extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 视频id */
    private Integer vId;

    /** 文件id */
    @Excel(name = "文件id")
    private String fId;

    /** 时长 */
    @Excel(name = "时长")
    private String duration;

    /** 宽 */
    @Excel(name = "宽")
    private Integer width;

    /** 高 */
    @Excel(name = "高")
    private Integer height;

    /** 缩略图资源 */
    @Excel(name = "缩略图资源")
    private String thumbnail;

    public void setvId(Integer vId) 
    {
        this.vId = vId;
    }

    public Integer getvId() 
    {
        return vId;
    }
    public void setfId(String fId)
    {
        this.fId = fId;
    }

    public String getfId()
    {
        return fId;
    }
    public void setDuration(String duration) 
    {
        this.duration = duration;
    }

    public String getDuration() 
    {
        return duration;
    }
    public void setWidth(Integer width) 
    {
        this.width = width;
    }

    public Integer getWidth() 
    {
        return width;
    }
    public void setHeight(Integer height) 
    {
        this.height = height;
    }

    public Integer getHeight() 
    {
        return height;
    }
    public void setThumbnail(String thumbnail) 
    {
        this.thumbnail = thumbnail;
    }

    public String getThumbnail() 
    {
        return thumbnail;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("vId", getvId())
            .append("fId", getfId())
            .append("duration", getDuration())
            .append("width", getWidth())
            .append("height", getHeight())
            .append("thumbnail", getThumbnail())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("remark", getRemark())
            .toString();
    }
}
