package com.ruoyi.wallpaper.domain;

import java.util.List;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 软件更新对象 w_update
 * 
 * @author weiwei
 * @date 2021-11-18
 */
public class WUpdate extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    @Excel(name = "id")
    private Long updateId;

    /** 版本 */
    @Excel(name = "版本")
    private Long version;

    /** 更新日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "更新日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date updateDate;

    /** 更新描述 */
    @Excel(name = "更新描述")
    private String updateDescription;

    /** 更新明细信息 */
    private List<WUpdateItem> wUpdateItemList;

    public void setUpdateId(Long updateId) 
    {
        this.updateId = updateId;
    }

    public Long getUpdateId() 
    {
        return updateId;
    }
    public void setVersion(Long version) 
    {
        this.version = version;
    }

    public Long getVersion() 
    {
        return version;
    }
    public void setUpdateDate(Date updateDate) 
    {
        this.updateDate = updateDate;
    }

    public Date getUpdateDate() 
    {
        return updateDate;
    }
    public void setUpdateDescription(String updateDescription) 
    {
        this.updateDescription = updateDescription;
    }

    public String getUpdateDescription() 
    {
        return updateDescription;
    }

    public List<WUpdateItem> getWUpdateItemList()
    {
        return wUpdateItemList;
    }

    public void setWUpdateItemList(List<WUpdateItem> wUpdateItemList)
    {
        this.wUpdateItemList = wUpdateItemList;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("updateId", getUpdateId())
            .append("version", getVersion())
            .append("updateDate", getUpdateDate())
            .append("updateDescription", getUpdateDescription())
            .append("remark", getRemark())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())
            .append("wUpdateItemList", getWUpdateItemList())
            .toString();
    }
}
