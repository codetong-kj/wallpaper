package com.ruoyi.wallpaper.domain;

import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 个人收藏对象 favorites
 *
 * @author weiwei
 * @date 2022-03-16
 */
public class FavoritesChoose extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    private Integer mId;
    private Integer fId;
    /**
     * 收藏夹名字
     */
    @Excel(name = "收藏夹名字")
    private String fName;
    /**
     * 数量
     */
    @Excel(name = "数量")
    private Integer count;

    /**
     * 是否选择
     */
    private Boolean checked;
    private Integer type;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getfId() {
        return fId;
    }

    public void setfId(Integer fId) {
        this.fId = fId;
    }

    public String getfName() {
        return fName;
    }

    public void setfName(String fName) {
        this.fName = fName;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Boolean getChecked() {
        return checked;
    }

    public void setChecked(Boolean checked) {
        this.checked = checked;
    }


    public Integer getmId() {
        return mId;
    }

    public void setmId(Integer mId) {
        this.mId = mId;
    }

    @Override
    public String toString() {
        return "FavoritesChoose{" +
                "mId=" + mId +
                ", fId=" + fId +
                ", fName='" + fName + '\'' +
                ", count=" + count +
                ", checked=" + checked +
                ", type=" + type +
                '}';
    }

}
