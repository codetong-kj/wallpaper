package com.ruoyi.wallpaper.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 会员对象 w_vip
 *
 * @author weiwei
 * @date 2022-03-29
 */
public class WVip extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** vip_id */
    private Long vipId;

    /** 用户id */
    @Excel(name = "用户id")
    private String userId;

    /** 会员等级 */
    @Excel(name = "会员等级")
    private Integer vipLevel;

    /** 开始日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "开始日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date startTime;

    /** 截至日期 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "截至日期", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;

    /** 状体 */
    @Excel(name = "状体")
    private Integer status;

    public void setVipId(Long vipId)
    {
        this.vipId = vipId;
    }

    public Long getVipId()
    {
        return vipId;
    }
    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public String getUserId()
    {
        return userId;
    }
    public void setVipLevel(Integer vipLevel)
    {
        this.vipLevel = vipLevel;
    }

    public Integer getVipLevel()
    {
        return vipLevel;
    }
    public void setStartTime(Date startTime)
    {
        this.startTime = startTime;
    }

    public Date getStartTime()
    {
        return startTime;
    }
    public void setEndTime(Date endTime)
    {
        this.endTime = endTime;
    }

    public Date getEndTime()
    {
        return endTime;
    }
    public void setStatus(Integer status)
    {
        this.status = status;
    }

    public Integer getStatus()
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
                .append("vipId", getVipId())
                .append("userId", getUserId())
                .append("vipLevel", getVipLevel())
                .append("startTime", getStartTime())
                .append("endTime", getEndTime())
                .append("status", getStatus())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}