package com.ruoyi.wallpaper.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 社会化用户对象 social_user
 * 
 * @author weiwei
 * @date 2022-01-12
 */
public class SocialUser extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 主键 */
    private Long id;

    /** 第三方系统的唯一ID */
    @Excel(name = "第三方系统的唯一ID")
    private String uuid;

    /** 第三方用户来源 */
    @Excel(name = "第三方用户来源")
    private String source;

    /** 用户的授权令牌 */
    @Excel(name = "用户的授权令牌")
    private String accessToken;

    /** 第三方用户的授权令牌的有效期 */
    @Excel(name = "第三方用户的授权令牌的有效期")
    private Integer expireIn;

    /** 刷新令牌 */
    @Excel(name = "刷新令牌")
    private String refreshToken;

    /** 第三方用户的 open id */
    @Excel(name = "第三方用户的 open id")
    private String openId;

    /** 第三方用户的 ID */
    @Excel(name = "第三方用户的 ID")
    private Long uid;

    /** 个别平台的授权信息 */
    @Excel(name = "个别平台的授权信息")
    private String accessCode;

    /** 第三方用户的 union id */
    @Excel(name = "第三方用户的 union id")
    private String unionId;

    /** 第三方用户授予的权限 */
    @Excel(name = "第三方用户授予的权限")
    private String scope;

    /** 个别平台的授权信息 */
    @Excel(name = "个别平台的授权信息")
    private String tokenType;

    /** id token */
    @Excel(name = "id token")
    private String idToken;

    /** 小米平台用户的附带属性 */
    @Excel(name = "小米平台用户的附带属性")
    private String macAlgorithm;

    /** 小米平台用户的附带属性 */
    @Excel(name = "小米平台用户的附带属性")
    private String macKey;

    /** 用户的授权code */
    @Excel(name = "用户的授权code")
    private String code;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setUuid(String uuid) 
    {
        this.uuid = uuid;
    }

    public String getUuid() 
    {
        return uuid;
    }
    public void setSource(String source) 
    {
        this.source = source;
    }

    public String getSource() 
    {
        return source;
    }
    public void setAccessToken(String accessToken) 
    {
        this.accessToken = accessToken;
    }

    public String getAccessToken() 
    {
        return accessToken;
    }
    public void setExpireIn(Integer expireIn) 
    {
        this.expireIn = expireIn;
    }

    public Integer getExpireIn() 
    {
        return expireIn;
    }
    public void setRefreshToken(String refreshToken) 
    {
        this.refreshToken = refreshToken;
    }

    public String getRefreshToken() 
    {
        return refreshToken;
    }
    public void setOpenId(String openId) 
    {
        this.openId = openId;
    }

    public String getOpenId() 
    {
        return openId;
    }
    public void setUid(Long uid) 
    {
        this.uid = uid;
    }

    public Long getUid() 
    {
        return uid;
    }
    public void setAccessCode(String accessCode) 
    {
        this.accessCode = accessCode;
    }

    public String getAccessCode() 
    {
        return accessCode;
    }
    public void setUnionId(String unionId) 
    {
        this.unionId = unionId;
    }

    public String getUnionId() 
    {
        return unionId;
    }
    public void setScope(String scope) 
    {
        this.scope = scope;
    }

    public String getScope() 
    {
        return scope;
    }
    public void setTokenType(String tokenType) 
    {
        this.tokenType = tokenType;
    }

    public String getTokenType() 
    {
        return tokenType;
    }
    public void setIdToken(String idToken) 
    {
        this.idToken = idToken;
    }

    public String getIdToken() 
    {
        return idToken;
    }
    public void setMacAlgorithm(String macAlgorithm) 
    {
        this.macAlgorithm = macAlgorithm;
    }

    public String getMacAlgorithm() 
    {
        return macAlgorithm;
    }
    public void setMacKey(String macKey) 
    {
        this.macKey = macKey;
    }

    public String getMacKey() 
    {
        return macKey;
    }
    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getCode() 
    {
        return code;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("uuid", getUuid())
            .append("source", getSource())
            .append("accessToken", getAccessToken())
            .append("expireIn", getExpireIn())
            .append("refreshToken", getRefreshToken())
            .append("openId", getOpenId())
            .append("uid", getUid())
            .append("accessCode", getAccessCode())
            .append("unionId", getUnionId())
            .append("scope", getScope())
            .append("tokenType", getTokenType())
            .append("idToken", getIdToken())
            .append("macAlgorithm", getMacAlgorithm())
            .append("macKey", getMacKey())
            .append("code", getCode())
            .toString();
    }
}
