package com.ruoyi.wallpaper.util;

import com.ruoyi.wallpaper.service.IUserLikeService;
import com.ruoyi.wallpaper.service.LikeService;

public class LikeUtil {
    LikeService likeService;
    IUserLikeService userLikeService;

    public LikeUtil() {
    }

    public LikeUtil(LikeService likeService, IUserLikeService userLikeService) {
        this.likeService = likeService;
        this.userLikeService = userLikeService;
    }

    public LikeService getLikeService() {
        return likeService;
    }

    public void setLikeService(LikeService likeService) {
        this.likeService = likeService;
    }

    public IUserLikeService getUserLikeService() {
        return userLikeService;
    }

    public void setUserLikeService(IUserLikeService userLikeService) {
        this.userLikeService = userLikeService;
    }

    /**
     * @Description 判断用户是否点赞过对此作品 true表示用户点过赞了 false表示没有点赞
     * @Author weiwei
     * @Date 2022/3/14 15:30
     * @Param
     * @Return
     * @Exception
     */
    public boolean isLiked(String workId, String userId) {
        if (userId == null || workId == null) {
            return false;
        }
        Integer integer = likeService.selectLiked(workId, userId);
        if (integer==null){
            integer=0;
        }
        Integer likedByUserIdAndWorkId = userLikeService.isLikedByUserIdAndWorkId(workId, userId);
        //没有点赞
        return integer != 0 || likedByUserIdAndWorkId != 0;
    }
}
