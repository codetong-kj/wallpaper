package com.ruoyi.wallpaper.util;


import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTCreator;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.wallpaper.domain.WUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

@Component
public class JwtUtils {
    // token时效：24小时
    public static final int EXPIRE = 96;
    // 签名哈希的密钥，对于不同的加密算法来说含义不同
    public static final String APP_SECRET = "ukc8BDbRigUDaYt86pZFfWus2jZWLPHOsdadasdasfdssfeweee";

    public static RedisCache staticCache;
    @Autowired
    RedisCache redisCache;

    @PostConstruct
    public void init() {
        staticCache = this.redisCache;
    }

    public static String getJwtTokenByUserId(String id) {
        JWTCreator.Builder builder = JWT.create();
        builder.withClaim("id", id);
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.HOUR, EXPIRE);
        builder.withExpiresAt(instance.getTime());
        return builder.sign(Algorithm.HMAC256(APP_SECRET));
    }

    /**
     * @Description 生成token
     * @Author weiwei
     * @Date 2022/1/27 17:01
     * @Param map参数
     * @Return 返回token
     * @Exception
     */
    public static String getJwtToken(Map<String, String> map) {
        JWTCreator.Builder builder = JWT.create();
        map.forEach(builder::withClaim);
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.HOUR, EXPIRE);
        builder.withExpiresAt(instance.getTime());
        return builder.sign(Algorithm.HMAC256(APP_SECRET));
    }

    /**
     * 判断token是否存在与有效
     *
     * @param jwtToken token字符串
     * @return 返回 DecodedJWT对象
     */
    public static void verify(String jwtToken) {
        JWT.require(Algorithm.HMAC256(APP_SECRET)).build().verify(jwtToken);
    }

    /**
     * @Description 根据token获取用户id
     * @Author weiwei
     * @Date 2022/3/27 10:43
     * @Param
     * @Return
     * @Exception
     */
    public static String getUserId() {
        HttpServletRequest request = ServletUtils.getRequest();
        String token = request.getHeader("token");
        //先从redis里查
        WUser wUser = staticCache.getCacheObject(Constants.LOGIN_USER_TOKEN_KEY + token);
        String userId = wUser.getUserId();
        if (StringUtils.isNotEmpty(userId)) {
            System.out.println("返回redisToken" + userId);
            return userId;
        }
        Map<String, String> tokenInfo = null;
        try {
            tokenInfo = JwtUtils.getTokenInfo(token);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        assert tokenInfo != null;
        return tokenInfo.get("id");
    }

    /**
     * 获取token信息
     *
     * @param token
     * @return
     */
    public static Map<String, String> getTokenInfo(String token) throws UnsupportedEncodingException {
        DecodedJWT verify = JWT.require(Algorithm.HMAC256(APP_SECRET)).build().verify(token);
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String exp = sdf1.format(verify.getExpiresAt());
        Map<String, Claim> claims = verify.getClaims();
        Map<String, String> m = new HashMap<>();
        claims.forEach((k, v) -> {
            m.put(k, v.asString());
        });
        m.put("exp", exp);
        return m;
    }
}
