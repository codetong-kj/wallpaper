package com.ruoyi.wallpaper.service;

import java.util.List;
import com.ruoyi.wallpaper.domain.WUserWorks;
import com.ruoyi.wallpaper.domain.WorkResult;
import org.apache.ibatis.annotations.Param;

/**
 * 用户作品Service接口
 * 
 * @author weiwei
 * @date 2022-02-11
 */
public interface IWUserWorksService 
{
    /**
     * @Description
     * @Author  weiwei
     * @Date   2022/2/15 22:57
     * @Param
     * @Return      返回用户作品列表
     * @Exception
     *
     */
    public List<WorkResult> selectWorksList(WUserWorks wUserWorks);

    /**
     * 查询用户作品
     * 
     * @param workId 用户作品主键
     * @return 用户作品
     */
    public WUserWorks selectWUserWorksByWorkId(String workId);

    /**
     * @Description
     * @Author  weiwei
     * @Date   2022/2/26 20:29
     * @Param  userId 用户id
     * @Return    作品列表
     * @Exception   没有
     *
     */
    public List<WUserWorks> selectWUserWorksImagesByUserId(String userId);

    /**
     * @Description 修改用户作品点赞数量
     * @Author  weiwei
     * @Date   2022/3/13 23:02
     * @Param
     * @Return
     * @Exception
     *
     */
    public int  updateLikeByWorkId(String workId,Integer like);
    /**
     * @Description 查询用户收藏夹下的某搜藏id下的所有图片
     * @Author  weiwei
     * @Date   2022/3/21 19:42
     * @Param
     * @Return
     * @Exception
     *
     */
    public  List<WorkResult> selectWorkImgByFavorites(String userId,Integer fId);
    /**
     * @Description 查询用户收藏夹下的某搜藏id下的所有图片
     * @Author  weiwei
     * @Date   2022/3/21 19:42
     * @Param
     * @Return
     * @Exception
     *
     */
    public  List<WorkResult> selectWorkVideoByFavorites(String userId,Integer fId);
    /**
     * @Description
     * @Author  weiwei
     * @Date   2022/2/26 20:29
     * @Param  userId 用户id
     * @Return    作品列表
     * @Exception   没有
     *
     */
    public List<WUserWorks> selectWUserWorksVideoByUserId(String userId);
    /**
     * 查询用户作品列表
     * 
     * @param wUserWorks 用户作品
     * @return 用户作品集合
     */
    public List<WUserWorks> selectWUserWorksList(WUserWorks wUserWorks);

    /**
     * 新增用户作品
     * 
     * @param wUserWorks 用户作品
     * @return 结果
     */
    public int insertWUserWorks(WUserWorks wUserWorks);

    /**
     * 修改用户作品
     * 
     * @param wUserWorks 用户作品
     * @return 结果
     */
    public int updateWUserWorks(WUserWorks wUserWorks);

    /**
     * 批量删除用户作品
     * 
     * @param workIds 需要删除的用户作品主键集合
     * @return 结果
     */
    public int deleteWUserWorksByWorkIds(String[] workIds);

    /**
     * 删除用户作品信息
     * 
     * @param workId 用户作品主键
     * @return 结果
     */
    public int deleteWUserWorksByWorkId(String workId);
}
