package com.ruoyi.wallpaper.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wallpaper.domain.LikedCountDTO;
import com.ruoyi.wallpaper.service.IWUserWorksService;
import com.ruoyi.wallpaper.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.UserLikeMapper;
import com.ruoyi.wallpaper.domain.UserLike;
import com.ruoyi.wallpaper.service.IUserLikeService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 用户点赞Service业务层处理
 *
 * @author weiwei
 * @date 2022-03-13
 */
@Service
public class UserLikeServiceImpl implements IUserLikeService {
    @Autowired
    private UserLikeMapper userLikeMapper;

    @Autowired
    private LikeService likeService;

    @Autowired
    private IWUserWorksService userWorksService;

    /**
     * 查询用户点赞
     *
     * @param id 用户点赞主键
     * @return 用户点赞
     */
    @Override
    public UserLike selectUserLikeById(Integer id) {
        return userLikeMapper.selectUserLikeById(id);
    }

    /**
     * 删除用户点赞
     *
     * @param workId 作品id；userId 用户id
     * @param userId
     * @return 结果
     */
    @Override
    public int deleteUserLikeByUserIdAndWorkId(String workId, String userId) {
        return userLikeMapper.deleteUserLikeByUserIdAndWorkId(workId, userId);
    }

    /**
     * @param workId
     * @param userId
     * @Description 查询用户是否对此作品点过赞
     * @Author weiwei
     * @Date 2022/3/13 11:15
     * @Param
     * @Return
     * @Exception
     */
    @Override
    public Integer isLikedByUserIdAndWorkId(String workId, String userId) {
        return userLikeMapper.isLikedByUserIdAndWorkId(workId, userId);
    }

    /**
     * 查询用户点赞列表
     *
     * @param userLike 用户点赞
     * @return 用户点赞
     */
    @Override
    public List<UserLike> selectUserLikeList(UserLike userLike) {
        return userLikeMapper.selectUserLikeList(userLike);
    }

    /**
     * 新增用户点赞
     *
     * @param userLike 用户点赞
     * @return 结果
     */
    @Override
    public int insertUserLike(UserLike userLike) {
        userLike.setCreateTime(DateUtils.getNowDate());
        return userLikeMapper.insertUserLike(userLike);
    }

    /**
     * 修改用户点赞
     *
     * @param userLike 用户点赞
     * @return 结果
     */
    @Override
    public int updateUserLike(UserLike userLike) {
        userLike.setUpdateTime(DateUtils.getNowDate());
        return userLikeMapper.updateUserLike(userLike);
    }

    /**
     * 批量删除用户点赞
     *
     * @param ids 需要删除的用户点赞主键
     * @return 结果
     */
    @Override
    public int deleteUserLikeByIds(Integer[] ids) {
        return userLikeMapper.deleteUserLikeByIds(ids);
    }

    /**
     * 删除用户点赞信息
     *
     * @param id 用户点赞主键
     * @return 结果
     */
    @Override
    public int deleteUserLikeById(Integer id) {
        return userLikeMapper.deleteUserLikeById(id);
    }

    /**
     * @Description 将redis中的数据转移到mysql
     * @Author weiwei
     * @Date 2022/3/13 12:49
     * @Param
     * @Return
     * @Exception
     */
    @Override
    @Transactional
    public void transLikeFromRedis() {
        List<UserLike> likedDataFromRedis = likeService.getLikedDataFromRedis();
        for (UserLike item : likedDataFromRedis
        ) {
            if (item.getStatus() == 1) {
                insertUserLike(item);
            } else {
                deleteUserLikeByUserIdAndWorkId(item.getLikedWorkId(), item.getLikedPostId());
            }
        }
    }

    @Override
    @Transactional
    public void transLikeCountFromRedis() {
        List<LikedCountDTO> count = likeService.getLikedCountFromRedis();
        for (LikedCountDTO item : count
        ) {
            if (item.getSum() == 0) {
                continue;
            }
            //sql 需要改一下 修改的时候进行查询与累加
            userWorksService.updateLikeByWorkId(item.getWorkId(), item.getSum());
        }


    }

}
