package com.ruoyi.wallpaper.service;

import java.util.List;
import com.ruoyi.wallpaper.domain.WVideoInfo;

/**
 * 视频详情Service接口
 * 
 * @author weiwei
 * @date 2022-02-28
 */
public interface IWVideoInfoService 
{
    /**
     * 新增视频详情
     *
     * @param videoInfos 视频详情列表
     * @return 结果
     */
    public int insertVideoInfoList(List<WVideoInfo> videoInfos);
    /**
     * 查询视频详情
     * 
     * @param vId 视频详情主键
     * @return 视频详情
     */
    public WVideoInfo selectWVideoInfoByVId(Integer vId);

    /**
     * 查询视频详情列表
     * 
     * @param wVideoInfo 视频详情
     * @return 视频详情集合
     */
    public List<WVideoInfo> selectWVideoInfoList(WVideoInfo wVideoInfo);

    /**
     * 新增视频详情
     * 
     * @param wVideoInfo 视频详情
     * @return 结果
     */
    public int insertWVideoInfo(WVideoInfo wVideoInfo);

    /**
     * 修改视频详情
     * 
     * @param wVideoInfo 视频详情
     * @return 结果
     */
    public int updateWVideoInfo(WVideoInfo wVideoInfo);

    /**
     * 批量删除视频详情
     * 
     * @param vIds 需要删除的视频详情主键集合
     * @return 结果
     */
    public int deleteWVideoInfoByVIds(Integer[] vIds);

    /**
     * 删除视频详情信息
     * 
     * @param vId 视频详情主键
     * @return 结果
     */
    public int deleteWVideoInfoByVId(Integer vId);
}
