package com.ruoyi.wallpaper.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wallpaper.domain.FavoritesChoose;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.FavoritesMapper;
import com.ruoyi.wallpaper.domain.Favorites;
import com.ruoyi.wallpaper.service.IFavoritesService;

/**
 * 个人收藏Service业务层处理
 *
 * @author weiwei
 * @date 2022-03-16
 */
@Service
public class FavoritesServiceImpl implements IFavoritesService {
    @Autowired
    private FavoritesMapper favoritesMapper;

    /**
     * 查询个人收藏
     *
     * @param fId 个人收藏主键
     * @return 个人收藏
     */
    @Override
    public Favorites selectFavoritesByFId(Integer fId) {
        return favoritesMapper.selectFavoritesByFId(fId);
    }

    /**
     * @param userId
     * @param workId
     * @Description 查询操作用户某个人作品下的收藏列表以及选中状态
     * @Author weiwei
     * @Date 2022/3/20 14:18
     * @Param userId 用户id,workId 作品id
     * @Return
     * @Exception
     */
    @Override
    public List<FavoritesChoose> selectCheckList(String userId, String workId,Integer type) {
        return favoritesMapper.selectCheckList(userId, workId,type);
    }

    /**
     * 查询个人收藏列表
     *
     * @param favorites 个人收藏
     * @return 个人收藏
     */
    @Override
    public List<Favorites> selectFavoritesList(Favorites favorites) {
        return favoritesMapper.selectFavoritesList(favorites);
    }

    /**
     * 新增个人收藏
     *
     * @param favorites 个人收藏
     * @return 结果
     */
    @Override
    public int insertFavorites(Favorites favorites) {
        favorites.setCreateTime(DateUtils.getNowDate());
        return favoritesMapper.insertFavorites(favorites);
    }

    /**
     * 修改个人收藏
     *
     * @param favorites 个人收藏
     * @return 结果
     */
    @Override
    public int updateFavorites(Favorites favorites) {
        favorites.setUpdateTime(DateUtils.getNowDate());
        return favoritesMapper.updateFavorites(favorites);
    }

    /**
     * 批量删除个人收藏
     *
     * @param fIds 需要删除的个人收藏主键
     * @return 结果
     */
    @Override
    public int deleteFavoritesByFIds(Integer[] fIds) {
        return favoritesMapper.deleteFavoritesByFIds(fIds);
    }

    /**
     * 删除个人收藏信息
     *
     * @param fId 个人收藏主键
     * @return 结果
     */
    @Override
    public int deleteFavoritesByFId(Integer fId) {
        return favoritesMapper.deleteFavoritesByFId(fId);
    }
}
