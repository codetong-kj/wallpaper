package com.ruoyi.wallpaper.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.aliyun.dysmsapi20170525.Client;
import com.aliyun.dysmsapi20170525.models.SendSmsRequest;
import com.aliyun.dysmsapi20170525.models.SendSmsResponse;
import com.aliyun.teaopenapi.models.Config;
import com.ruoyi.wallpaper.service.ISendPhoneCode;
import org.springframework.stereotype.Service;

/**
 * @author ke
 */
@Service
public class SendPhoneCodeImpl implements ISendPhoneCode {
    @Override
    public boolean sendCode(String phoneNumber, String code) {
        Client client = createClient("LTAI5tHro66hABKFFL8Gsu2q", "dRyXy9yjYBzg6QKXvHELUbJ7SxoO7u");
        SendSmsRequest sendSmsRequest = new SendSmsRequest()
                .setSignName("我的学习管理")
                .setTemplateCode("SMS_237200463")
                .setPhoneNumbers(phoneNumber)
                .setTemplateParam("{\"code\":\"" + code + "\"}");
        // 复制代码运行请自行打印 API 的返回值
        SendSmsResponse sendSmsResponse = null;
        try {
            sendSmsResponse = client.sendSms(sendSmsRequest);
            if ("OK".equals(sendSmsResponse.getBody().code)) {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    /**
     * 使用AK&SK初始化账号Client
     *
     * @param accessKeyId
     * @param accessKeySecret
     * @return Client
     * @throws Exception
     */
    public Client createClient(String accessKeyId, String accessKeySecret) {
        Config config = new Config()
                // 您的AccessKey ID
                .setAccessKeyId(accessKeyId)
                // 您的AccessKey Secret
                .setAccessKeySecret(accessKeySecret);
        // 访问的域名
        config.endpoint = "dysmsapi.aliyuncs.com";
        try {
            return new Client(config);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
