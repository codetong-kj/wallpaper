package com.ruoyi.wallpaper.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.wallpaper.domain.AnnounceInfo;
import com.ruoyi.wallpaper.mapper.AnnounceMapper;
import com.ruoyi.wallpaper.domain.Announce;
import com.ruoyi.wallpaper.service.IAnnounceService;

/**
 * 公告管理Service业务层处理
 *
 * @author weiwei
 * @date 2022-03-28
 */
@Service
public class AnnounceServiceImpl implements IAnnounceService {
    @Autowired
    private AnnounceMapper announceMapper;

    /**
     * 查询公告管理
     *
     * @param aid 公告管理主键
     * @return 公告管理
     */
    @Override
    public Announce selectAnnounceByAid(Integer aid) {
        return announceMapper.selectAnnounceByAid(aid);
    }

    /**
     * 查询公告信息
     *
     * @param aid 公告管理主键
     * @return 公告管理
     */
    @Override
    public AnnounceInfo selectAnnounceInfo(Integer aid) {
        return announceMapper.selectAnnounceInfo(aid);
    }

    /**
     * 查询公告管理列表
     *
     * @param announce 公告管理
     * @return 公告管理
     */
    @Override
    public List<Announce> selectAnnounceList(Announce announce) {
        return announceMapper.selectAnnounceList(announce);
    }

    /**
     * 新增公告管理
     *
     * @param announce 公告管理
     * @return 结果
     */
    @Transactional
    @Override
    public int insertAnnounce(Announce announce) {
        announce.setCreateTime(DateUtils.getNowDate());
        int rows = announceMapper.insertAnnounce(announce);
        insertAnnounceInfo(announce);
        return rows;
    }

    /**
     * 修改公告管理
     *
     * @param announce 公告管理
     * @return 结果
     */
    @Transactional
    @Override
    public int updateAnnounce(Announce announce) {
        announce.setUpdateTime(DateUtils.getNowDate());
        announceMapper.deleteAnnounceInfoByAid(announce.getAid());
        insertAnnounceInfo(announce);
        return announceMapper.updateAnnounce(announce);
    }

    /**
     * 批量删除公告管理
     *
     * @param aids 需要删除的公告管理主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteAnnounceByAids(Integer[] aids) {
        announceMapper.deleteAnnounceInfoByAids(aids);
        return announceMapper.deleteAnnounceByAids(aids);
    }

    /**
     * 删除公告管理信息
     *
     * @param aid 公告管理主键
     * @return 结果
     */
    @Override
    public int deleteAnnounceByAid(Integer aid) {
        announceMapper.deleteAnnounceInfoByAid(aid);
        return announceMapper.deleteAnnounceByAid(aid);
    }

    /**
     * 新增公告信息信息
     *
     * @param announce 公告管理对象
     */
    public void insertAnnounceInfo(Announce announce) {
        List<AnnounceInfo> announceInfoList = announce.getAnnounceInfoList();
        Integer aid = announce.getAid();
        if (StringUtils.isNotNull(announceInfoList)) {
            List<AnnounceInfo> list = new ArrayList<AnnounceInfo>();
            for (AnnounceInfo announceInfo : announceInfoList) {
                announceInfo.setAid(aid);
                list.add(announceInfo);
            }
            if (list.size() > 0) {
                announceMapper.batchAnnounceInfo(list);
            }
        }
    }
}
