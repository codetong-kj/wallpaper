package com.ruoyi.wallpaper.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.CheckInMapper;
import com.ruoyi.wallpaper.domain.CheckIn;
import com.ruoyi.wallpaper.service.ICheckInService;

/**
 * 签到Service业务层处理
 * 
 * @author weiwei
 * @date 2022-04-17
 */
@Service
public class CheckInServiceImpl implements ICheckInService 
{
    @Autowired
    private CheckInMapper checkInMapper;

    /**
     * 查询签到
     * 
     * @param id 签到主键
     * @return 签到
     */
    @Override
    public CheckIn selectCheckInById(Integer id)
    {
        return checkInMapper.selectCheckInById(id);
    }

    /**
     * 查询签到列表
     * 
     * @param checkIn 签到
     * @return 签到
     */
    @Override
    public List<CheckIn> selectCheckInList(CheckIn checkIn)
    {
        return checkInMapper.selectCheckInList(checkIn);
    }

    /**
     * 新增签到
     * 
     * @param checkIn 签到
     * @return 结果
     */
    @Override
    public int insertCheckIn(CheckIn checkIn)
    {
        checkIn.setCreateTime(DateUtils.getNowDate());
        return checkInMapper.insertCheckIn(checkIn);
    }

    /**
     * 修改签到
     * 
     * @param checkIn 签到
     * @return 结果
     */
    @Override
    public int updateCheckIn(CheckIn checkIn)
    {
        checkIn.setUpdateTime(DateUtils.getNowDate());
        return checkInMapper.updateCheckIn(checkIn);
    }

    /**
     * 批量删除签到
     * 
     * @param ids 需要删除的签到主键
     * @return 结果
     */
    @Override
    public int deleteCheckInByIds(Integer[] ids)
    {
        return checkInMapper.deleteCheckInByIds(ids);
    }

    /**
     * 删除签到信息
     * 
     * @param id 签到主键
     * @return 结果
     */
    @Override
    public int deleteCheckInById(Integer id)
    {
        return checkInMapper.deleteCheckInById(id);
    }
}
