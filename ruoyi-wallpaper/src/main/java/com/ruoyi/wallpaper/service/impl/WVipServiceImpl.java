package com.ruoyi.wallpaper.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wallpaper.mapper.WVipMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.domain.WVip;
import com.ruoyi.wallpaper.service.IWVipService;

/**
 * 会员Service业务层处理
 *
 * @author weiwei
 * @date 2021-12-17
 */
@Service
public class WVipServiceImpl implements IWVipService {
    @Autowired
    private WVipMapper wVipMapper;

    @Override
    public WVip selectWVipByUserId(String userId) {
        return wVipMapper.selectWVipByUserId(userId);
    }

    /**
     * 查询会员
     *
     * @param vipId 会员主键
     * @return 会员
     */
    @Override
    public WVip selectWVipByVipId(Long vipId) {
        return wVipMapper.selectWVipByVipId(vipId);
    }

    /**
     * 查询会员列表
     *
     * @param wVip 会员
     * @return 会员
     */
    @Override
    public List<WVip> selectWVipList(WVip wVip) {
        return wVipMapper.selectWVipList(wVip);
    }

    /**
     * 新增会员
     *
     * @param wVip 会员
     * @return 结果
     */
    @Override
    public int insertWVip(WVip wVip) {
        wVip.setCreateTime(DateUtils.getNowDate());
        return wVipMapper.insertWVip(wVip);
    }

    /**
     * 修改会员
     *
     * @param wVip 会员
     * @return 结果
     */
    @Override
    public int updateWVip(WVip wVip) {
        wVip.setUpdateTime(DateUtils.getNowDate());
        return wVipMapper.updateWVip(wVip);
    }

    /**
     * 批量删除会员
     *
     * @param vipIds 需要删除的会员主键
     * @return 结果
     */
    @Override
    public int deleteWVipByVipIds(Long[] vipIds) {
        return wVipMapper.deleteWVipByVipIds(vipIds);
    }

    /**
     * 删除会员信息
     *
     * @param vipId 会员主键
     * @return 结果
     */
    @Override
    public int deleteWVipByVipId(Long vipId) {
        return wVipMapper.deleteWVipByVipId(vipId);
    }
}
