package com.ruoyi.wallpaper.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.WallpaperStaticMapper;
import com.ruoyi.wallpaper.domain.WallpaperStatic;
import com.ruoyi.wallpaper.service.IWallpaperStaticService;

/**
 * 静态壁纸Service业务层处理
 * 
 * @author weiwei
 * @date 2021-11-10
 */
@Service
public class WallpaperStaticServiceImpl implements IWallpaperStaticService 
{
    @Autowired
    private WallpaperStaticMapper wallpaperStaticMapper;

    /**
     * 查询静态壁纸
     * 
     * @param wId 静态壁纸主键
     * @return 静态壁纸
     */
    @Override
    public WallpaperStatic selectWallpaperStaticByWId(String wId)
    {
        return wallpaperStaticMapper.selectWallpaperStaticByWId(wId);
    }

    /**
     * 查询静态壁纸列表
     * 
     * @param wallpaperStatic 静态壁纸
     * @return 静态壁纸
     */
    @Override
    public List<WallpaperStatic> selectWallpaperStaticList(WallpaperStatic wallpaperStatic)
    {
        return wallpaperStaticMapper.selectWallpaperStaticList(wallpaperStatic);
    }

    /**
     * 新增静态壁纸
     * 
     * @param wallpaperStatic 静态壁纸
     * @return 结果
     */
    @Override
    public int insertWallpaperStatic(WallpaperStatic wallpaperStatic)
    {
        wallpaperStatic.setCreateTime(DateUtils.getNowDate());
        return wallpaperStaticMapper.insertWallpaperStatic(wallpaperStatic);
    }

    /**
     * 修改静态壁纸
     * 
     * @param wallpaperStatic 静态壁纸
     * @return 结果
     */
    @Override
    public int updateWallpaperStatic(WallpaperStatic wallpaperStatic)
    {
        wallpaperStatic.setUpdateTime(DateUtils.getNowDate());
        return wallpaperStaticMapper.updateWallpaperStatic(wallpaperStatic);
    }

    /**
     * 批量删除静态壁纸
     * 
     * @param wIds 需要删除的静态壁纸主键
     * @return 结果
     */
    @Override
    public int deleteWallpaperStaticByWIds(String[] wIds)
    {
        return wallpaperStaticMapper.deleteWallpaperStaticByWIds(wIds);
    }

    /**
     * 删除静态壁纸信息
     * 
     * @param wId 静态壁纸主键
     * @return 结果
     */
    @Override
    public int deleteWallpaperStaticByWId(String wId)
    {
        return wallpaperStaticMapper.deleteWallpaperStaticByWId(wId);
    }
}
