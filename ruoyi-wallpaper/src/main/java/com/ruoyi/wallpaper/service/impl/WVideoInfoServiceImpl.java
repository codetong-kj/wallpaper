package com.ruoyi.wallpaper.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.WVideoInfoMapper;
import com.ruoyi.wallpaper.domain.WVideoInfo;
import com.ruoyi.wallpaper.service.IWVideoInfoService;

/**
 * 视频详情Service业务层处理
 *
 * @author weiwei
 * @date 2022-02-28
 */
@Service
public class WVideoInfoServiceImpl implements IWVideoInfoService {
    @Autowired
    private WVideoInfoMapper wVideoInfoMapper;

    /**
     * 新增视频详情
     *
     * @param videoInfos 视频详情列表
     * @return 结果
     */
    @Override
    public int insertVideoInfoList(List<WVideoInfo> videoInfos) {
        return wVideoInfoMapper.insertVideoInfoList(videoInfos);
    }

    /**
     * 查询视频详情
     *
     * @param vId 视频详情主键
     * @return 视频详情
     */
    @Override
    public WVideoInfo selectWVideoInfoByVId(Integer vId) {
        return wVideoInfoMapper.selectWVideoInfoByVId(vId);
    }

    /**
     * 查询视频详情列表
     *
     * @param wVideoInfo 视频详情
     * @return 视频详情
     */
    @Override
    public List<WVideoInfo> selectWVideoInfoList(WVideoInfo wVideoInfo) {
        return wVideoInfoMapper.selectWVideoInfoList(wVideoInfo);
    }

    /**
     * 新增视频详情
     *
     * @param wVideoInfo 视频详情
     * @return 结果
     */
    @Override
    public int insertWVideoInfo(WVideoInfo wVideoInfo) {
        wVideoInfo.setCreateTime(DateUtils.getNowDate());
        return wVideoInfoMapper.insertWVideoInfo(wVideoInfo);
    }

    /**
     * 修改视频详情
     *
     * @param wVideoInfo 视频详情
     * @return 结果
     */
    @Override
    public int updateWVideoInfo(WVideoInfo wVideoInfo) {
        wVideoInfo.setUpdateTime(DateUtils.getNowDate());
        return wVideoInfoMapper.updateWVideoInfo(wVideoInfo);
    }

    /**
     * 批量删除视频详情
     *
     * @param vIds 需要删除的视频详情主键
     * @return 结果
     */
    @Override
    public int deleteWVideoInfoByVIds(Integer[] vIds) {
        return wVideoInfoMapper.deleteWVideoInfoByVIds(vIds);
    }

    /**
     * 删除视频详情信息
     *
     * @param vId 视频详情主键
     * @return 结果
     */
    @Override
    public int deleteWVideoInfoByVId(Integer vId) {
        return wVideoInfoMapper.deleteWVideoInfoByVId(vId);
    }
}
