package com.ruoyi.wallpaper.service;

import java.util.List;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wallpaper.domain.WUser;

/**
 * 用户信息Service接口
 * 
 * @author weiwei
 * @date 2021-12-17
 */
public interface IWUserService 
{

    /**
     *
     * @return 条数
     */
    int selectUserCount();
    /**
    * @Description用户登录
    * @Author  weiwei
    * @Date   2022/3/26 21:28
    * @Param
    * @Return
    * @Exception
    *
    */
    String userLogin(WUser user);
    /**
    * @Description 查询用户id
    * @Author  weiwei
    * @Date   2022/3/26 21:12
    * @Param
    * @Return
    * @Exception
    *
    */
    String getUserId();

    /**
     *
     * @param wUser 用户信息
     * @return 用户信息
     */
    WUser selectOneUser(WUser wUser);
    /**
     * 获取用户
     * @return 用户信息用户名
     */
    WUser selectUserNickName(String userId);
    /**
     *
     * @param wUser 用户信息
     * @return 用户信息
     */
    WUser selectWUserByNamePwd(WUser wUser);
    /**
     *  @author: weiwei
     *  @Date: 2022/1/12 18:40
     *  @Description: 根据社会化id查询用户
     */
    WUser selectUserBySUid(String socialUserId);
    /**
     *
     * @param userName 用户名
     * @return 条数
     */
    int selectCountByUserName(String userName);

    /**
     * 查询用户信息
     * 
     * @param userId 用户信息主键
     * @return 用户信息
     */
    public WUser selectWUserByUserId(String userId);
    /**
     * @Description
     * @Author  weiwei
     * @Date   2022/2/26 21:43
     * @Param  用户id
     * @Return      查询简单的用信息
     * @Exception
     *
     */
    WUser selectUserSimpleInfo(String userId);
    /**
     * 查询用户信息列表
     * 
     * @param wUser 用户信息
     * @return 用户信息集合
     */
    public List<WUser> selectWUserList(WUser wUser);

    /**
     * 新增用户信息
     * 
     * @param wUser 用户信息
     * @return 结果
     */
    public int insertWUser(WUser wUser);

    /**
     * 修改用户信息
     * 
     * @param wUser 用户信息
     * @return 结果
     */
    public int updateWUser(WUser wUser);

    /**
     * 批量删除用户信息
     * 
     * @param userIds 需要删除的用户信息主键集合
     * @return 结果
     */
    public int deleteWUserByUserIds(String[] userIds);

    /**
     * 删除用户信息信息
     * 
     * @param userId 用户信息主键
     * @return 结果
     */
    public int deleteWUserByUserId(String userId);
}
