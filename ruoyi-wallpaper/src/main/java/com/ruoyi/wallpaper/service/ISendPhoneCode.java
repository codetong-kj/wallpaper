package com.ruoyi.wallpaper.service;

/**
 *  @author: weiwei
 *  @Date: 2022/3/26 17:03
 *  @Description: 发送手机验证码
 */
public interface ISendPhoneCode {
    public boolean sendCode(String phoneNumber,String code);
}
