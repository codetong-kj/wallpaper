package com.ruoyi.wallpaper.service;

import java.util.List;
import com.ruoyi.wallpaper.domain.CheckIn;

/**
 * 签到Service接口
 * 
 * @author weiwei
 * @date 2022-04-17
 */
public interface ICheckInService 
{
    /**
     * 查询签到
     * 
     * @param id 签到主键
     * @return 签到
     */
    public CheckIn selectCheckInById(Integer id);

    /**
     * 查询签到列表
     * 
     * @param checkIn 签到
     * @return 签到集合
     */
    public List<CheckIn> selectCheckInList(CheckIn checkIn);

    /**
     * 新增签到
     * 
     * @param checkIn 签到
     * @return 结果
     */
    public int insertCheckIn(CheckIn checkIn);

    /**
     * 修改签到
     * 
     * @param checkIn 签到
     * @return 结果
     */
    public int updateCheckIn(CheckIn checkIn);

    /**
     * 批量删除签到
     * 
     * @param ids 需要删除的签到主键集合
     * @return 结果
     */
    public int deleteCheckInByIds(Integer[] ids);

    /**
     * 删除签到信息
     * 
     * @param id 签到主键
     * @return 结果
     */
    public int deleteCheckInById(Integer id);
}
