package com.ruoyi.wallpaper.service.impl;

import com.ruoyi.common.enums.LikedStatusEnum;
import com.ruoyi.common.utils.RedisKeyUtils;
import com.ruoyi.wallpaper.domain.LikedCountDTO;
import com.ruoyi.wallpaper.domain.UserLike;
import com.ruoyi.wallpaper.service.IUserLikeService;
import com.ruoyi.wallpaper.service.LikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class LikeServiceImpl implements LikeService {

    @Autowired
    RedisTemplate redisTemplate;

    @Autowired
    IUserLikeService likeService;

    /**
     * @Description
     * @Author weiwei
     * @Date 2022/3/12 19:01
     * @Param
     * @Return
     * @Exception
     */
    @Override
    public void saveLiked2Redis(String likedWorkId, String likedPostId) {
        String key = RedisKeyUtils.getLikedKey(likedWorkId, likedPostId);
        redisTemplate.opsForHash().put("like:"+RedisKeyUtils.MAP_KEY_WORK_LIKED, key, LikedStatusEnum.LIKE.getCode());
    }

    @Override
    public void unlikeFromRedis(String likedWorkId, String likedPostId) {
        String key = RedisKeyUtils.getLikedKey(likedWorkId, likedPostId);
        redisTemplate.opsForHash().put("like:"+RedisKeyUtils.MAP_KEY_WORK_LIKED, key, LikedStatusEnum.UNLIKE.getCode());
    }

    @Override
    public Integer selectLiked(String likedWorkId, String likedPostId) {
        //查询是否点赞
        String key = RedisKeyUtils.getLikedKey(likedWorkId, likedPostId);
        Integer integer = (Integer) redisTemplate.opsForHash().get("like:" + RedisKeyUtils.MAP_KEY_WORK_LIKED, key);
        return integer;
    }

    @Override
    public void deleteLikedFromRedis(String likedWorkId, String likedPostId) {
        String key = RedisKeyUtils.getLikedKey(likedWorkId, likedPostId);
        redisTemplate.opsForHash().delete("like:"+RedisKeyUtils.MAP_KEY_WORK_LIKED, key);
    }

    @Override
    public void incrementLikedCount(String likedWorkId) {
        redisTemplate.opsForHash().increment(RedisKeyUtils.MAP_KEY_WORK_LIKED_COUNT, likedWorkId, 1);
    }

    @Override
    public void decrementLikedCount(String likedWorkId) {
        redisTemplate.opsForHash().increment(RedisKeyUtils.MAP_KEY_WORK_LIKED_COUNT, likedWorkId, -1);
    }

    @Override
    public List<UserLike> getLikedDataFromRedis() {
        Cursor<Map.Entry<Object, Object>> cursor = redisTemplate.opsForHash().scan("like:"+RedisKeyUtils.MAP_KEY_WORK_LIKED, ScanOptions.NONE);
        List<UserLike> list = new ArrayList<>();
        while (cursor.hasNext()) {
            Map.Entry<Object, Object> entry = cursor.next();
            String key = (String) entry.getKey();
            //分离出 likedUserId，likedPostId
            String[] split = key.split("::");
            String likedWorkId = split[0];
            String likedPostId = split[1];
            Integer value = (Integer) entry.getValue();
            //组装成 UserLike 对象
            UserLike userLike = new UserLike(likedWorkId, likedPostId, value);
            list.add(userLike);
            //存到 list 后从 Redis 中删除
            redisTemplate.opsForHash().delete("like:"+RedisKeyUtils.MAP_KEY_WORK_LIKED, key);
        }
        return list;
    }

    @Override
    public List<LikedCountDTO> getLikedCountFromRedis() {
        Cursor<Map.Entry<Object, Object>> cursor = redisTemplate.opsForHash().scan(RedisKeyUtils.MAP_KEY_WORK_LIKED_COUNT, ScanOptions.NONE);
        List<LikedCountDTO> list = new ArrayList<>();
        while (cursor.hasNext()){
            Map.Entry<Object, Object> map = cursor.next();
            //将点赞数量存储在 LikedCountDT
            String key = (String)map.getKey();
            LikedCountDTO dto = new LikedCountDTO(key, (Integer) map.getValue());
            list.add(dto);
            //从Redis中删除这条记录
            redisTemplate.opsForHash().delete(RedisKeyUtils.MAP_KEY_WORK_LIKED_COUNT, key);
        }
        return list;
    }
}