package com.ruoyi.wallpaper.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.WBuriedLoginMapper;
import com.ruoyi.wallpaper.domain.WBuriedLogin;
import com.ruoyi.wallpaper.service.IWBuriedLoginService;

/**
 * 埋点用户登录Service业务层处理
 *
 * @author weiwei
 * @date 2021-12-23
 */
@Service
public class WBuriedLoginServiceImpl implements IWBuriedLoginService {
    @Autowired
    private WBuriedLoginMapper wBuriedLoginMapper;

    /**
     * 查询埋点用户登录
     *
     * @param lId 埋点用户登录主键
     * @return 埋点用户登录
     */
    @Override
    public WBuriedLogin selectWBuriedLoginByLId(Long lId) {
        return wBuriedLoginMapper.selectWBuriedLoginByLId(lId);
    }

    @Override
    public int updateExitTime(String userId) {
        return wBuriedLoginMapper.updateExitTime(userId);
    }

    /**
     * 查询埋点用户登录列表
     *
     * @param wBuriedLogin 埋点用户登录
     * @return 埋点用户登录
     */
    @Override
    public List<WBuriedLogin> selectWBuriedLoginList(WBuriedLogin wBuriedLogin) {
        return wBuriedLoginMapper.selectWBuriedLoginList(wBuriedLogin);
    }

    /**
     * 新增埋点用户登录
     *
     * @param wBuriedLogin 埋点用户登录
     * @return 结果
     */
    @Override
    public int insertWBuriedLogin(WBuriedLogin wBuriedLogin) {
        wBuriedLogin.setCreateTime(DateUtils.getNowDate());
        return wBuriedLoginMapper.insertWBuriedLogin(wBuriedLogin);
    }

    /**
     * 修改埋点用户登录
     *
     * @param wBuriedLogin 埋点用户登录
     * @return 结果
     */
    @Override
    public int updateWBuriedLogin(WBuriedLogin wBuriedLogin) {
        wBuriedLogin.setUpdateTime(DateUtils.getNowDate());
        return wBuriedLoginMapper.updateWBuriedLogin(wBuriedLogin);
    }

    /**
     * 批量删除埋点用户登录
     *
     * @param lIds 需要删除的埋点用户登录主键
     * @return 结果
     */
    @Override
    public int deleteWBuriedLoginByLIds(Long[] lIds) {
        return wBuriedLoginMapper.deleteWBuriedLoginByLIds(lIds);
    }

    /**
     * 删除埋点用户登录信息
     *
     * @param lId 埋点用户登录主键
     * @return 结果
     */
    @Override
    public int deleteWBuriedLoginByLId(Long lId) {
        return wBuriedLoginMapper.deleteWBuriedLoginByLId(lId);
    }
}
