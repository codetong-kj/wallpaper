package com.ruoyi.wallpaper.service.impl;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wallpaper.util.JwtUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.wallpaper.domain.WUser;
import com.ruoyi.wallpaper.mapper.WUserMapper;
import com.ruoyi.wallpaper.service.IWUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * 用户信息Service业务层处理
 *
 * @author weiwei
 * @date 2021-12-17
 */
@Service
public class WUserServiceImpl implements IWUserService {
    @Autowired
    private WUserMapper wUserMapper;
    @Autowired
    RedisCache redisCache;

    /**
     * @return 条数
     */
    @Override
    public int selectUserCount() {
        return wUserMapper.selectUserCount();
    }

    @Override
    public String userLogin(WUser user) {
        //查询到了用户信息，进行设置最后一次登录时间并且登录
        user.setLoginDate(DateUtils.getNowDate());
        updateWUser(user);
        String jwtTokenByUserId = JwtUtils.getJwtTokenByUserId(user.getUserId());
        redisCache.setCacheObject(Constants.LOGIN_USER_TOKEN_KEY + jwtTokenByUserId, user, 2, TimeUnit.DAYS);
        return jwtTokenByUserId;
    }

    @Override
    public String getUserId() {
        HttpServletRequest request = ServletUtils.getRequest();
        String token = request.getHeader("token");
        //先从redis里查
        WUser wUser = redisCache.getCacheObject(Constants.LOGIN_USER_TOKEN_KEY + token);
        if (wUser != null) {
            return wUser.getUserId();
        }
        return null;
    }

    /**
     * @param wUser 用户信息
     * @return 用户信息
     */
    @Override
    public WUser selectOneUser(WUser wUser) {
        return wUserMapper.selectOneUser(wUser);
    }

    @Override
    public WUser selectUserNickName(String userId) {
        return wUserMapper.selectUserNickName(userId);
    }

    @Override
    public WUser selectWUserByNamePwd(WUser wUser) {
        return wUserMapper.selectWUserByNamePwd(wUser);
    }

    /**
     * @param socialUserId
     * @author: weiwei
     * @Date: 2022/1/12 18:40
     * @Description: 根据社会化id查询用户
     */
    @Override
    public WUser selectUserBySUid(String socialUserId) {
        return wUserMapper.selectUserBySUid(socialUserId);
    }

    @Override
    public int selectCountByUserName(String userName) {
        return wUserMapper.selectCountByUserName(userName);
    }

    /**
     * 查询用户信息
     *
     * @param userId 用户信息主键
     * @return 用户信息
     */
    @Override
    public WUser selectWUserByUserId(String userId) {
        return wUserMapper.selectWUserByUserId(userId);
    }

    /**
     * @param userId
     * @Description
     * @Author weiwei
     * @Date 2022/2/26 21:43
     * @Param 用户id
     * @Return 查询简单的用信息
     * @Exception
     */
    @Override
    public WUser selectUserSimpleInfo(String userId) {
        return wUserMapper.selectUserSimpleInfo(userId);
    }

    /**
     * 查询用户信息列表
     *
     * @param wUser 用户信息
     * @return 用户信息
     */
    @Override
    public List<WUser> selectWUserList(WUser wUser) {
        return wUserMapper.selectWUserList(wUser);
    }

    /**
     * 新增用户信息
     *
     * @param wUser 用户信息
     * @return 结果
     */
    @Override
    public int insertWUser(WUser wUser) {
        wUser.setCreateTime(DateUtils.getNowDate());
        return wUserMapper.insertWUser(wUser);
    }

    /**
     * 修改用户信息
     *
     * @param wUser 用户信息
     * @return 结果
     */
    @Override
    public int updateWUser(WUser wUser) {
        wUser.setUpdateTime(DateUtils.getNowDate());
        return wUserMapper.updateWUser(wUser);
    }

    /**
     * 批量删除用户信息
     *
     * @param userIds 需要删除的用户信息主键
     * @return 结果
     */
    @Override
    public int deleteWUserByUserIds(String[] userIds) {
        return wUserMapper.deleteWUserByUserIds(userIds);
    }

    /**
     * 删除用户信息信息
     *
     * @param userId 用户信息主键
     * @return 结果
     */
    @Override
    public int deleteWUserByUserId(String userId) {
        return wUserMapper.deleteWUserByUserId(userId);
    }
}
