package com.ruoyi.wallpaper.service;

import java.util.List;
import com.ruoyi.wallpaper.domain.WUpdate;

/**
 * 软件更新Service接口
 * 
 * @author weiwei
 * @date 2021-11-18
 */
public interface IWUpdateService 
{
    /**
     * 查询软件更新列表
     *
     * @return 软件更新集合
     */
    public List<WUpdate> selectWUpdateAllList();
    /**
     * 查询软件更新
     * 
     * @param updateId 软件更新主键
     * @return 软件更新
     */
    public WUpdate selectWUpdateByUpdateId(Long updateId);
    /**
     *
     * @return 获取最后一个版本
     */
    public WUpdate getLastUpdate();
    /**
     * 查询软件更新列表
     * 
     * @param wUpdate 软件更新
     * @return 软件更新集合
     */
    public List<WUpdate> selectWUpdateList(WUpdate wUpdate);

    /**
     * 新增软件更新
     * 
     * @param wUpdate 软件更新
     * @return 结果
     */
    public int insertWUpdate(WUpdate wUpdate);

    /**
     * 修改软件更新
     * 
     * @param wUpdate 软件更新
     * @return 结果
     */
    public int updateWUpdate(WUpdate wUpdate);

    /**
     * 批量删除软件更新
     * 
     * @param updateIds 需要删除的软件更新主键集合
     * @return 结果
     */
    public int deleteWUpdateByUpdateIds(Long[] updateIds);

    /**
     * 删除软件更新信息
     * 
     * @param updateId 软件更新主键
     * @return 结果
     */
    public int deleteWUpdateByUpdateId(Long updateId);
}
