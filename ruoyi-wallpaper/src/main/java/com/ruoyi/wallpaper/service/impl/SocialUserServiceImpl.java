package com.ruoyi.wallpaper.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.SocialUserMapper;
import com.ruoyi.wallpaper.domain.SocialUser;
import com.ruoyi.wallpaper.service.ISocialUserService;

/**
 * 社会化用户Service业务层处理
 * 
 * @author weiwei
 * @date 2022-01-12
 */
@Service
public class SocialUserServiceImpl implements ISocialUserService 
{
    @Autowired
    private SocialUserMapper socialUserMapper;

    /**
     * 查询社会化用户
     * 
     * @param id 社会化用户主键
     * @return 社会化用户
     */
    @Override
    public SocialUser selectSocialUserById(Long id)
    {
        return socialUserMapper.selectSocialUserById(id);
    }

    /**
     * 查询社会化用户列表
     * 
     * @param socialUser 社会化用户
     * @return 社会化用户
     */
    @Override
    public List<SocialUser> selectSocialUserList(SocialUser socialUser)
    {
        return socialUserMapper.selectSocialUserList(socialUser);
    }

    /**
     * 新增社会化用户
     * 
     * @param socialUser 社会化用户
     * @return 结果
     */
    @Override
    public int insertSocialUser(SocialUser socialUser)
    {
        return socialUserMapper.insertSocialUser(socialUser);
    }

    /**
     * 修改社会化用户
     * 
     * @param socialUser 社会化用户
     * @return 结果
     */
    @Override
    public int updateSocialUser(SocialUser socialUser)
    {
        return socialUserMapper.updateSocialUser(socialUser);
    }

    /**
     * 批量删除社会化用户
     * 
     * @param ids 需要删除的社会化用户主键
     * @return 结果
     */
    @Override
    public int deleteSocialUserByIds(Long[] ids)
    {
        return socialUserMapper.deleteSocialUserByIds(ids);
    }

    /**
     * 删除社会化用户信息
     * 
     * @param id 社会化用户主键
     * @return 结果
     */
    @Override
    public int deleteSocialUserById(Long id)
    {
        return socialUserMapper.deleteSocialUserById(id);
    }
}
