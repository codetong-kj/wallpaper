package com.ruoyi.wallpaper.service;

import java.util.List;
import com.ruoyi.wallpaper.domain.WBuried;

/**
 * 埋点Service接口
 * 
 * @author weiwei
 * @date 2021-12-23
 */
public interface IWBuriedService 
{
    /**
     * 查询埋点
     * 
     * @param id 埋点主键
     * @return 埋点
     */
    public WBuried selectWBuriedById(Long id);
    /**
     * 修改埋点根据ip
     *
     * @param wBuried 埋点
     * @return 结果
     */
    public int updateWBuriedByIp(WBuried wBuried);
    /**
     * 查询埋点
     *
     * @param userId 用户id
     * @return 埋点
     */
    public int selectWBuriedIdByUserId(String userId);
    /**
     * 查询埋点列表
     * 
     * @param wBuried 埋点
     * @return 埋点集合
     */
    public List<WBuried> selectWBuriedList(WBuried wBuried);
    /**
     *  @author: weiwei
     *  @Date: 2022/1/4 17:49
     *  @Description:根据IP查询埋点主数据
     */
    public Integer getWBuriedByIp(String ip);
    /**
     * 新增埋点
     * 
     * @param wBuried 埋点
     * @return 结果
     */
    public int insertWBuried(WBuried wBuried);

    /**
     * 修改埋点
     * 
     * @param wBuried 埋点
     * @return 结果
     */
    public int updateWBuried(WBuried wBuried);

    /**
     * 批量删除埋点
     * 
     * @param ids 需要删除的埋点主键集合
     * @return 结果
     */
    public int deleteWBuriedByIds(Long[] ids);

    /**
     * 删除埋点信息
     * 
     * @param id 埋点主键
     * @return 结果
     */
    public int deleteWBuriedById(Long id);
}
