package com.ruoyi.wallpaper.service;

import java.util.List;
import com.ruoyi.wallpaper.domain.IpLocation;
import com.ruoyi.wallpaper.domain.IpResult;

/**
 * ip地址地域Service接口
 * 
 * @author weiwei
 * @date 2022-01-08
 */
public interface IIpLocationService 
{
    /**
     * @Description 获取地理位置信息
     * @Author  weiwei
     * @Date   2022/1/8 22:03
     * @Param
     * @Return
     * @Exception
     *
     */
    public List<IpResult> selectLocation();
    /**
     * 查询ip地址地域
     * 
     * @param id ip地址地域主键
     * @return ip地址地域
     */
    public IpLocation selectIpLocationById(Long id);

    /**
     * 查询ip地址地域列表
     * 
     * @param ipLocation ip地址地域
     * @return ip地址地域集合
     */
    public List<IpLocation> selectIpLocationList(IpLocation ipLocation);

    /**
     * 新增ip地址地域
     * 
     * @param ipLocation ip地址地域
     * @return 结果
     */
    public int insertIpLocation(IpLocation ipLocation);

    /**
     * 修改ip地址地域
     * 
     * @param ipLocation ip地址地域
     * @return 结果
     */
    public int updateIpLocation(IpLocation ipLocation);

    /**
     * 批量删除ip地址地域
     * 
     * @param ids 需要删除的ip地址地域主键集合
     * @return 结果
     */
    public int deleteIpLocationByIds(Long[] ids);

    /**
     * 删除ip地址地域信息
     * 
     * @param id ip地址地域主键
     * @return 结果
     */
    public int deleteIpLocationById(Long id);
}
