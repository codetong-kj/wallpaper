package com.ruoyi.wallpaper.service;

import java.util.List;
import com.ruoyi.wallpaper.domain.Favorites;
import com.ruoyi.wallpaper.domain.FavoritesChoose;
import org.apache.ibatis.annotations.Param;

/**
 * 个人收藏Service接口
 * 
 * @author weiwei
 * @date 2022-03-16
 */
public interface IFavoritesService 
{
    /**
     * 查询个人收藏
     * 
     * @param fId 个人收藏主键
     * @return 个人收藏
     */
    public Favorites selectFavoritesByFId(Integer fId);

    /**
     * @Description 查询操作用户某个人作品下的收藏列表以及选中状态
     * @Author  weiwei
     * @Date   2022/3/20 14:18
     * @Param  userId 用户id,workId 作品id
     * @Return
     * @Exception
     *
     */
    public List<FavoritesChoose> selectCheckList(String userId,String workId,Integer type);
    /**
     * 查询个人收藏列表
     * 
     * @param favorites 个人收藏
     * @return 个人收藏集合
     */
    public List<Favorites> selectFavoritesList(Favorites favorites);

    /**
     * 新增个人收藏
     * 
     * @param favorites 个人收藏
     * @return 结果
     */
    public int insertFavorites(Favorites favorites);

    /**
     * 修改个人收藏
     * 
     * @param favorites 个人收藏
     * @return 结果
     */
    public int updateFavorites(Favorites favorites);

    /**
     * 批量删除个人收藏
     * 
     * @param fIds 需要删除的个人收藏主键集合
     * @return 结果
     */
    public int deleteFavoritesByFIds(Integer[] fIds);

    /**
     * 删除个人收藏信息
     * 
     * @param fId 个人收藏主键
     * @return 结果
     */
    public int deleteFavoritesByFId(Integer fId);
}
