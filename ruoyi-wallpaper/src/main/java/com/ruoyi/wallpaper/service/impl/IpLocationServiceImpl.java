package com.ruoyi.wallpaper.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wallpaper.domain.IpResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.IpLocationMapper;
import com.ruoyi.wallpaper.domain.IpLocation;
import com.ruoyi.wallpaper.service.IIpLocationService;

/**
 * ip地址地域Service业务层处理
 *
 * @author weiwei
 * @date 2022-01-08
 */
@Service
public class IpLocationServiceImpl implements IIpLocationService {
    @Autowired
    private IpLocationMapper ipLocationMapper;

    /**
     * @Description 获取地理位置信息
     * @Author weiwei
     * @Date 2022/1/8 22:03
     * @Param
     * @Return
     * @Exception
     */
    @Override
    public List<IpResult> selectLocation() {
        return ipLocationMapper.selectLocation();
    }

    /**
     * 查询ip地址地域
     *
     * @param id ip地址地域主键
     * @return ip地址地域
     */
    @Override
    public IpLocation selectIpLocationById(Long id) {
        return ipLocationMapper.selectIpLocationById(id);
    }

    /**
     * 查询ip地址地域列表
     *
     * @param ipLocation ip地址地域
     * @return ip地址地域
     */
    @Override
    public List<IpLocation> selectIpLocationList(IpLocation ipLocation) {
        return ipLocationMapper.selectIpLocationList(ipLocation);
    }

    /**
     * 新增ip地址地域
     *
     * @param ipLocation ip地址地域
     * @return 结果
     */
    @Override
    public int insertIpLocation(IpLocation ipLocation) {
        ipLocation.setCreateTime(DateUtils.getNowDate());
        return ipLocationMapper.insertIpLocation(ipLocation);
    }

    /**
     * 修改ip地址地域
     *
     * @param ipLocation ip地址地域
     * @return 结果
     */
    @Override
    public int updateIpLocation(IpLocation ipLocation) {
        ipLocation.setUpdateTime(DateUtils.getNowDate());
        return ipLocationMapper.updateIpLocation(ipLocation);
    }

    /**
     * 批量删除ip地址地域
     *
     * @param ids 需要删除的ip地址地域主键
     * @return 结果
     */
    @Override
    public int deleteIpLocationByIds(Long[] ids) {
        return ipLocationMapper.deleteIpLocationByIds(ids);
    }

    /**
     * 删除ip地址地域信息
     *
     * @param id ip地址地域主键
     * @return 结果
     */
    @Override
    public int deleteIpLocationById(Long id) {
        return ipLocationMapper.deleteIpLocationById(id);
    }
}
