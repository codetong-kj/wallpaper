package com.ruoyi.wallpaper.service;

import java.util.List;
import com.ruoyi.wallpaper.domain.WallpaperStatic;

/**
 * 静态壁纸Service接口
 * 
 * @author weiwei
 * @date 2021-11-10
 */
public interface IWallpaperStaticService 
{
    /**
     * 查询静态壁纸
     * 
     * @param wId 静态壁纸主键
     * @return 静态壁纸
     */
    public WallpaperStatic selectWallpaperStaticByWId(String wId);

    /**
     * 查询静态壁纸列表
     * 
     * @param wallpaperStatic 静态壁纸
     * @return 静态壁纸集合
     */
    public List<WallpaperStatic> selectWallpaperStaticList(WallpaperStatic wallpaperStatic);

    /**
     * 新增静态壁纸
     * 
     * @param wallpaperStatic 静态壁纸
     * @return 结果
     */
    public int insertWallpaperStatic(WallpaperStatic wallpaperStatic);

    /**
     * 修改静态壁纸
     * 
     * @param wallpaperStatic 静态壁纸
     * @return 结果
     */
    public int updateWallpaperStatic(WallpaperStatic wallpaperStatic);

    /**
     * 批量删除静态壁纸
     * 
     * @param wIds 需要删除的静态壁纸主键集合
     * @return 结果
     */
    public int deleteWallpaperStaticByWIds(String[] wIds);

    /**
     * 删除静态壁纸信息
     * 
     * @param wId 静态壁纸主键
     * @return 结果
     */
    public int deleteWallpaperStaticByWId(String wId);
}
