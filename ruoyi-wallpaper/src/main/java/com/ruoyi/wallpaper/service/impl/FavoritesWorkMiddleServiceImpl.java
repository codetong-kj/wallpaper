package com.ruoyi.wallpaper.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.FavoritesWorkMiddleMapper;
import com.ruoyi.wallpaper.domain.FavoritesWorkMiddle;
import com.ruoyi.wallpaper.service.IFavoritesWorkMiddleService;
import org.springframework.transaction.annotation.Transactional;

/**
 * 收藏夹与作品之间的关联Service业务层处理
 *
 * @author weiwei
 * @date 2022-03-16
 */
@Service
public class FavoritesWorkMiddleServiceImpl implements IFavoritesWorkMiddleService {
    @Autowired
    private FavoritesWorkMiddleMapper favoritesWorkMiddleMapper;

    /**
     * 查询收藏夹与作品之间的关联
     *
     * @param fId 收藏夹与作品之间的关联主键
     * @return 收藏夹与作品之间的关联
     */
    @Override
    public FavoritesWorkMiddle selectFavoritesWorkMiddleByFId(Integer fId) {
        return favoritesWorkMiddleMapper.selectFavoritesWorkMiddleByFId(fId);
    }

    /**
     * 查询收藏夹与作品之间的关联列表
     *
     * @param userId 用户id
     * @return 收藏夹与作品之间的关联集合
     */
    @Override
    public List<FavoritesWorkMiddle> selectFavoritesWorkMiddleListByUserId(String userId) {
        return favoritesWorkMiddleMapper.selectFavoritesWorkMiddleListByUserId(userId);
    }

    /**
     * 查询收藏夹与作品之间的关联列表
     *
     * @param favoritesWorkMiddle 收藏夹与作品之间的关联
     * @return 收藏夹与作品之间的关联
     */
    @Override
    public List<FavoritesWorkMiddle> selectFavoritesWorkMiddleList(FavoritesWorkMiddle favoritesWorkMiddle) {
        return favoritesWorkMiddleMapper.selectFavoritesWorkMiddleList(favoritesWorkMiddle);
    }

    /**
     * 新增收藏夹与作品之间的关联
     *
     * @param favoritesWorkMiddle 收藏夹与作品之间的关联
     * @return 结果
     */
    @Override
    public int insertFavoritesWorkMiddle(FavoritesWorkMiddle favoritesWorkMiddle) {
        favoritesWorkMiddle.setCreateTime(DateUtils.getNowDate());
        return favoritesWorkMiddleMapper.insertFavoritesWorkMiddle(favoritesWorkMiddle);
    }

    /**
     * 修改收藏夹与作品之间的关联
     *
     * @param favoritesWorkMiddle 收藏夹与作品之间的关联
     * @return 结果
     */
    @Override
    public int updateFavoritesWorkMiddle(FavoritesWorkMiddle favoritesWorkMiddle) {
        favoritesWorkMiddle.setUpdateTime(DateUtils.getNowDate());
        return favoritesWorkMiddleMapper.updateFavoritesWorkMiddle(favoritesWorkMiddle);
    }

    /**
     * 批量删除收藏夹与作品之间的关联
     *
     * @param mIds 需要删除的收藏夹与作品之间的关联主键
     * @return 结果
     */
    @Override
    public int deleteFavoritesWorkMiddleByMIds(Integer[] mIds) {
        return favoritesWorkMiddleMapper.deleteFavoritesWorkMiddleByMIds(mIds);
    }

    /**
     * 删除收藏夹与作品之间的关联信息
     *
     * @param mId 收藏夹与作品之间的关联主键
     * @return 结果
     */
    @Override
    public int deleteFavoritesWorkMiddleByMId(Integer mId) {
        return favoritesWorkMiddleMapper.deleteFavoritesWorkMiddleByMId(mId);
    }

    /**
     * @param favoritesWorkMiddle
     * @Description 根据fId和workId删除
     * @Author weiwei
     * @Date 2022/3/20 21:50
     * @Param
     * @Return
     * @Exception
     */
    @Override
    public int deleteFavoritesWorkMiddleByMFidWorkId(FavoritesWorkMiddle favoritesWorkMiddle) {
        return favoritesWorkMiddleMapper.deleteFavoritesWorkMiddleByMFidWorkId(favoritesWorkMiddle);
    }

    /**
     * @param userId 用户id
     * @Description 查询用户收藏夹里的内容
     * @Author weiwei
     * @Date 2022/3/21 15:18
     * @Param
     * @Return
     * @Exception
     */
    @Override
    public List<Map<String, Object>> selectAllFavoritesList(String userId) {
        return favoritesWorkMiddleMapper.selectAllFavoritesList(userId);
    }
}
