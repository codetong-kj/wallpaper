package com.ruoyi.wallpaper.service;

import java.util.List;
import java.util.Map;

import com.ruoyi.wallpaper.domain.Comment;
import com.ruoyi.wallpaper.domain.CommentResult;

/**
 * 用户评论Service接口
 * 
 * @author weiwei
 * @date 2022-02-22
 */
public interface ICommentService 
{
    /**
     * 查询用户评论
     *
     * @param workId 用户作品id
     * @return 用户评论
     */
    public List<Comment> selectCommentByWorkId(String workId);
    /**
     * 查询用户评论
     *
     * @param workId 用户作品id
     * @return 用户评论二层列表
     */
    public List<CommentResult> selectListSecondCommentListByWorkId(String workId);

    /**
     * 查询用户评论
     *
     * @param commentId 顶级评论的id
     * @return 第二层评论
     */
    public List<CommentResult> selectListSecondAllCommentListByWorkId(Long commentId);
    /**
     * @Description
     * @Author  weiwei
     * @Date   2022/2/25 15:05
     * @Param
     * @Return 返回顶层评论的条数
     * @Exception
     *
     */
    public Integer selectFirstTotalByWorkId(String workId);
    /**
     * 查询用户评论
     *
     * @param workId 用户作品id
     * @return 用户评论一层列表
     */
    public List<CommentResult> selectListFirstCommentListByWorkId(String workId);
    /**
     * 查询用户评论
     * 
     * @param commentId 用户评论主键
     * @return 用户评论
     */
    public Comment selectCommentByCommentId(Long commentId);

    /**
     * 查询用户评论列表
     * 
     * @param comment 用户评论
     * @return 用户评论集合
     */
    public List<Comment> selectCommentList(Comment comment);

    /**
     * 新增用户评论
     * 
     * @param comment 用户评论
     * @return 结果
     */
    public int insertComment(Comment comment);

    /**
     * 修改用户评论
     * 
     * @param comment 用户评论
     * @return 结果
     */
    public int updateComment(Comment comment);

    /**
     * 批量删除用户评论
     * 
     * @param commentIds 需要删除的用户评论主键集合
     * @return 结果
     */
    public int deleteCommentByCommentIds(Long[] commentIds);

    /**
     * 删除用户评论信息
     * 
     * @param commentId 用户评论主键
     * @return 结果
     */
    public int deleteCommentByCommentId(Long commentId);
}
