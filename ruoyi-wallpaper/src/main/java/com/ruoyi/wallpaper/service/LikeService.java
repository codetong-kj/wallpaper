package com.ruoyi.wallpaper.service;

import com.ruoyi.wallpaper.domain.LikedCountDTO;
import com.ruoyi.wallpaper.domain.UserLike;

import java.util.List;

public interface LikeService {
    /**
     * 点赞。状态为1
     * @param likedWorkId
     * @param likedPostId
     */
    void saveLiked2Redis(String likedWorkId, String likedPostId);

    /**
    * @Description 查询点赞
    * @Author  weiwei
    * @Date   2022/3/13 12:04
    * @Param
    * @Return
    * @Exception
    *
    */
    public Integer selectLiked(String likedWorkId, String likedPostId);
    /**
     * 取消点赞。将状态改变为0
     * @param likedWorkId
     * @param likedPostId
     */
    void unlikeFromRedis(String likedWorkId, String likedPostId);

    /**
     * 从Redis中删除一条点赞数据
     * @param likedWorkId
     * @param likedPostId
     */
    void deleteLikedFromRedis(String likedWorkId, String likedPostId);

    /**
     * 该用户的点赞数加1
     * @param likedWorkId
     */
    void incrementLikedCount(String likedWorkId);

    /**
     * 该用户的点赞数减1
     * @param likedWorkId
     */
    void decrementLikedCount(String likedWorkId);

    /**
     * 获取Redis中存储的所有点赞数据
     * @return
     */
    List<UserLike> getLikedDataFromRedis();

    /**
    * @Description  获取每个作品的点赞次数
    * @Author  weiwei
    * @Date 2022/3/13 22:52
    * @Param
    * @Return
    * @Exception
    *
    */
    public List<LikedCountDTO> getLikedCountFromRedis();
}
