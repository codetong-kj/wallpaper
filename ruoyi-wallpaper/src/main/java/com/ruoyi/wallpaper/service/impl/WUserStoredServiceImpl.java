package com.ruoyi.wallpaper.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.WUserStoredMapper;
import com.ruoyi.wallpaper.domain.WUserStored;
import com.ruoyi.wallpaper.service.IWUserStoredService;

/**
 * 用户壁纸库存中间Service业务层处理
 * 
 * @author weiwei
 * @date 2021-12-17
 */
@Service
public class WUserStoredServiceImpl implements IWUserStoredService 
{
    @Autowired
    private WUserStoredMapper wUserStoredMapper;

    /**
     * 查询用户壁纸库存中间
     * 
     * @param wPid 用户壁纸库存中间主键
     * @return 用户壁纸库存中间
     */
    @Override
    public WUserStored selectWUserStoredByWPid(String wPid)
    {
        return wUserStoredMapper.selectWUserStoredByWPid(wPid);
    }

    /**
     * 查询用户壁纸库存中间列表
     * 
     * @param wUserStored 用户壁纸库存中间
     * @return 用户壁纸库存中间
     */
    @Override
    public List<WUserStored> selectWUserStoredList(WUserStored wUserStored)
    {
        return wUserStoredMapper.selectWUserStoredList(wUserStored);
    }

    /**
     * 新增用户壁纸库存中间
     * 
     * @param wUserStored 用户壁纸库存中间
     * @return 结果
     */
    @Override
    public int insertWUserStored(WUserStored wUserStored)
    {
        return wUserStoredMapper.insertWUserStored(wUserStored);
    }

    /**
     * 修改用户壁纸库存中间
     * 
     * @param wUserStored 用户壁纸库存中间
     * @return 结果
     */
    @Override
    public int updateWUserStored(WUserStored wUserStored)
    {
        return wUserStoredMapper.updateWUserStored(wUserStored);
    }

    /**
     * 批量删除用户壁纸库存中间
     * 
     * @param wPids 需要删除的用户壁纸库存中间主键
     * @return 结果
     */
    @Override
    public int deleteWUserStoredByWPids(String[] wPids)
    {
        return wUserStoredMapper.deleteWUserStoredByWPids(wPids);
    }

    /**
     * 删除用户壁纸库存中间信息
     * 
     * @param wPid 用户壁纸库存中间主键
     * @return 结果
     */
    @Override
    public int deleteWUserStoredByWPid(String wPid)
    {
        return wUserStoredMapper.deleteWUserStoredByWPid(wPid);
    }
}
