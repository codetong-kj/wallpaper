package com.ruoyi.wallpaper.service.impl;

import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wallpaper.domain.CommentResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.CommentMapper;
import com.ruoyi.wallpaper.domain.Comment;
import com.ruoyi.wallpaper.service.ICommentService;

/**
 * 用户评论Service业务层处理
 *
 * @author weiwei
 * @date 2022-02-22
 */
@Service
public class CommentServiceImpl implements ICommentService {
    @Autowired
    private CommentMapper commentMapper;

    /**
     * 查询用户评论
     *
     * @param workId 用户作品id
     * @return 用户评论
     */
    @Override
    public List<Comment> selectCommentByWorkId(String workId) {
        return commentMapper.selectCommentByWorkId(workId);
    }

    /**
     * 查询用户评论
     *
     * @param workId 用户作品id
     * @return 用户评论二层列表
     */
    @Override
    public List<CommentResult> selectListSecondCommentListByWorkId(String workId) {
        return commentMapper.selectListSecondCommentListByWorkId(workId);
    }

    /**
     * 查询用户评论
     *
     * @param workId 顶级评论的id
     * @return 第二层评论
     */
    @Override
    public List<CommentResult> selectListSecondAllCommentListByWorkId(Long commentId) {
        return commentMapper.selectListSecondAllCommentListByWorkId(commentId);
    }

    /**
     * @param workId
     * @Description
     * @Author weiwei
     * @Date 2022/2/25 15:05
     * @Param
     * @Return 返回顶层评论的条数
     * @Exception
     */
    @Override
    public Integer selectFirstTotalByWorkId(String workId) {
        return commentMapper.selectFirstTotalByWorkId(workId);
    }


    /**
     * 查询用户评论
     *
     * @param workId 用户作品id
     * @return 用户评论一层列表
     */
    @Override
    public List<CommentResult> selectListFirstCommentListByWorkId(String workId) {
        return commentMapper.selectListFirstCommentListByWorkId(workId);
    }


    /**
     * 查询用户评论
     *
     * @param commentId 用户评论主键
     * @return 用户评论
     */
    @Override
    public Comment selectCommentByCommentId(Long commentId) {
        return commentMapper.selectCommentByCommentId(commentId);
    }

    /**
     * 查询用户评论列表
     *
     * @param comment 用户评论
     * @return 用户评论
     */
    @Override
    public List<Comment> selectCommentList(Comment comment) {
        return commentMapper.selectCommentList(comment);
    }

    /**
     * 新增用户评论
     *
     * @param comment 用户评论
     * @return 结果
     */
    @Override
    public int insertComment(Comment comment) {
        comment.setCreateTime(DateUtils.getNowDate());
        return commentMapper.insertComment(comment);
    }

    /**
     * 修改用户评论
     *
     * @param comment 用户评论
     * @return 结果
     */
    @Override
    public int updateComment(Comment comment) {
        comment.setUpdateTime(DateUtils.getNowDate());
        return commentMapper.updateComment(comment);
    }

    /**
     * 批量删除用户评论
     *
     * @param commentIds 需要删除的用户评论主键
     * @return 结果
     */
    @Override
    public int deleteCommentByCommentIds(Long[] commentIds) {
        return commentMapper.deleteCommentByCommentIds(commentIds);
    }

    /**
     * 删除用户评论信息
     *
     * @param commentId 用户评论主键
     * @return 结果
     */
    @Override
    public int deleteCommentByCommentId(Long commentId) {
        return commentMapper.deleteCommentByCommentId(commentId);
    }
}
