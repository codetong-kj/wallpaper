package com.ruoyi.wallpaper.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.SocialUserAuthMapper;
import com.ruoyi.wallpaper.domain.SocialUserAuth;
import com.ruoyi.wallpaper.service.ISocialUserAuthService;

/**
 * 社会化用户 & 系统用户关系Service业务层处理
 * 
 * @author weiwei
 * @date 2022-01-12
 */
@Service
public class SocialUserAuthServiceImpl implements ISocialUserAuthService 
{
    @Autowired
    private SocialUserAuthMapper socialUserAuthMapper;

    /**
     * 查询社会化用户 & 系统用户关系
     * 
     * @param id 社会化用户 & 系统用户关系主键
     * @return 社会化用户 & 系统用户关系
     */
    @Override
    public SocialUserAuth selectSocialUserAuthById(Integer id)
    {
        return socialUserAuthMapper.selectSocialUserAuthById(id);
    }

    /**
     * 查询社会化用户 & 系统用户关系列表
     * 
     * @param socialUserAuth 社会化用户 & 系统用户关系
     * @return 社会化用户 & 系统用户关系
     */
    @Override
    public List<SocialUserAuth> selectSocialUserAuthList(SocialUserAuth socialUserAuth)
    {
        return socialUserAuthMapper.selectSocialUserAuthList(socialUserAuth);
    }

    /**
     * 新增社会化用户 & 系统用户关系
     * 
     * @param socialUserAuth 社会化用户 & 系统用户关系
     * @return 结果
     */
    @Override
    public int insertSocialUserAuth(SocialUserAuth socialUserAuth)
    {
        return socialUserAuthMapper.insertSocialUserAuth(socialUserAuth);
    }

    /**
     * 修改社会化用户 & 系统用户关系
     * 
     * @param socialUserAuth 社会化用户 & 系统用户关系
     * @return 结果
     */
    @Override
    public int updateSocialUserAuth(SocialUserAuth socialUserAuth)
    {
        return socialUserAuthMapper.updateSocialUserAuth(socialUserAuth);
    }

    /**
     * 批量删除社会化用户 & 系统用户关系
     * 
     * @param ids 需要删除的社会化用户 & 系统用户关系主键
     * @return 结果
     */
    @Override
    public int deleteSocialUserAuthByIds(Integer[] ids)
    {
        return socialUserAuthMapper.deleteSocialUserAuthByIds(ids);
    }

    /**
     * 删除社会化用户 & 系统用户关系信息
     * 
     * @param id 社会化用户 & 系统用户关系主键
     * @return 结果
     */
    @Override
    public int deleteSocialUserAuthById(Integer id)
    {
        return socialUserAuthMapper.deleteSocialUserAuthById(id);
    }
}
