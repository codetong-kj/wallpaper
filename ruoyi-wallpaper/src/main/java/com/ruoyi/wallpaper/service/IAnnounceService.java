package com.ruoyi.wallpaper.service;

import java.util.List;
import com.ruoyi.wallpaper.domain.Announce;
import com.ruoyi.wallpaper.domain.AnnounceInfo;

/**
 * 公告管理Service接口
 * 
 * @author weiwei
 * @date 2022-03-28
 */
public interface IAnnounceService 
{
    /**
     * 查询公告管理
     * 
     * @param aid 公告管理主键
     * @return 公告管理
     */
    public Announce selectAnnounceByAid(Integer aid);

    /**
     * 查询公告信息
     *
     * @param aid 公告管理主键
     * @return 公告管理
     */
    public AnnounceInfo selectAnnounceInfo(Integer aid);
    /**
     * 查询公告管理列表
     * 
     * @param announce 公告管理
     * @return 公告管理集合
     */
    public List<Announce> selectAnnounceList(Announce announce);

    /**
     * 新增公告管理
     * 
     * @param announce 公告管理
     * @return 结果
     */
    public int insertAnnounce(Announce announce);

    /**
     * 修改公告管理
     * 
     * @param announce 公告管理
     * @return 结果
     */
    public int updateAnnounce(Announce announce);

    /**
     * 批量删除公告管理
     * 
     * @param aids 需要删除的公告管理主键集合
     * @return 结果
     */
    public int deleteAnnounceByAids(Integer[] aids);

    /**
     * 删除公告管理信息
     * 
     * @param aid 公告管理主键
     * @return 结果
     */
    public int deleteAnnounceByAid(Integer aid);
}
