package com.ruoyi.wallpaper.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.WBuriedPayMapper;
import com.ruoyi.wallpaper.domain.WBuriedPay;
import com.ruoyi.wallpaper.service.IWBuriedPayService;

/**
 * 埋点支付Service业务层处理
 * 
 * @author weiwei
 * @date 2021-12-23
 */
@Service
public class WBuriedPayServiceImpl implements IWBuriedPayService 
{
    @Autowired
    private WBuriedPayMapper wBuriedPayMapper;

    /**
     * 查询埋点支付
     * 
     * @param vId 埋点支付主键
     * @return 埋点支付
     */
    @Override
    public WBuriedPay selectWBuriedPayByVId(Long vId)
    {
        return wBuriedPayMapper.selectWBuriedPayByVId(vId);
    }

    /**
     * 查询埋点支付列表
     * 
     * @param wBuriedPay 埋点支付
     * @return 埋点支付
     */
    @Override
    public List<WBuriedPay> selectWBuriedPayList(WBuriedPay wBuriedPay)
    {
        return wBuriedPayMapper.selectWBuriedPayList(wBuriedPay);
    }

    /**
     * 新增埋点支付
     * 
     * @param wBuriedPay 埋点支付
     * @return 结果
     */
    @Override
    public int insertWBuriedPay(WBuriedPay wBuriedPay)
    {
        wBuriedPay.setCreateTime(DateUtils.getNowDate());
        return wBuriedPayMapper.insertWBuriedPay(wBuriedPay);
    }

    /**
     * 修改埋点支付
     * 
     * @param wBuriedPay 埋点支付
     * @return 结果
     */
    @Override
    public int updateWBuriedPay(WBuriedPay wBuriedPay)
    {
        wBuriedPay.setUpdateTime(DateUtils.getNowDate());
        return wBuriedPayMapper.updateWBuriedPay(wBuriedPay);
    }

    /**
     * 批量删除埋点支付
     * 
     * @param vIds 需要删除的埋点支付主键
     * @return 结果
     */
    @Override
    public int deleteWBuriedPayByVIds(Long[] vIds)
    {
        return wBuriedPayMapper.deleteWBuriedPayByVIds(vIds);
    }

    /**
     * 删除埋点支付信息
     * 
     * @param vId 埋点支付主键
     * @return 结果
     */
    @Override
    public int deleteWBuriedPayByVId(Long vId)
    {
        return wBuriedPayMapper.deleteWBuriedPayByVId(vId);
    }
}
