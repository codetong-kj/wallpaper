package com.ruoyi.wallpaper.service.pay;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeQueryRequest;
import com.alipay.api.response.AlipayTradePagePayResponse;
import com.alipay.api.response.AlipayTradeQueryResponse;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;

@Service
public class Alipay {

    //支付宝网关
    private static String GATEWAY = "https://openapi.alipay.com/gateway.do";
    //应用私钥
    private static String APP_PRIVATE_KEY = "MIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCD9+EpT+q+gmDsHfEWoNTX4PhSnx6x46b/2EvBS3reikGVqQOXlnRoiAh6Os/nSs6JahtNe+hi9KZvTiTPhS8tYshnWL/Zrd+0ToxF4EZ+vi5aPjTg4QIQLS/6TjqG3pKxMMLLCjAAWkeVx2aF4AKxxqEVBusX6jaLNJS8QIVMWaYIDcVFyQY/z5hPQ0jPHuf+MMqAu/blO2a5l8/f8nNMl327AU2OsUm8qMNRXDHKfj3d7YRBUDF2LG60YqSf1NdheX0E08hc4JCSURdKtZGZ6vqDNDnauYIouAzyj2c6aObiWbkc8n1KoPsCa8ZtBrVh4lXcSGjtwJ3hZMyS2lAlAgMBAAECggEAexHqO5q58pyoQsrRkSbAF+tH6dVWzNLfZO2UCFmZ5nvNGgMaxH2dZfam4UaDqNGMt33qpGIPoGlD5b1ceuNb0dmzxx1vHVQXsZEnI+s6EuzNUX991NJCGE/r1GSsFCbxaqlsn+mKRmnrJHVm2SbFnID+rBtdl1nnL/8DTmRla4MtrKuFkBpz2OUMkcoBz7kOwI6zdW38NSqUQ5crgknXGKEJnLNI+5ie4MimYqp2sPC6W/O46vNw7iRubS/DB3GPbnvJ2oj/n+0iAV8hFvlfGe0N4Gu2gu3fuxpv+61JDj3CLAl46XyrgpqjQLEU5Jgf+U+BxrbLfu2gqRrkMwRNAQKBgQD/wwKb7YRcut9/kkAzzuwpCsWibCBm1rfkdw8+yZul34gJEnJ3jL0nD6qgjq3FZ4jMifv8KPEUaohfU3TN9s8v/NB3b6drr4PepPfD6CrNIiUECMrlHLir4eCk52qqmID0Ap5+9hDVLxQCPNze/Gv0Lt1R/e2h0rHEcrQjlZOCRQKBgQCEF1lg+KIz30HrWvZiQbzzmnirQlU8wcqq3z2NZ07L38+rn2GRE3a3xJfnSPK5xylM/A67JK0bJUT+KBjaz3AIYnof5xLvujODR9OjzC96+jCBXdjHI5nNd9j6Duy5O7eTquF/r/Oa251vAscVY+8Rtx0yFbnKoBNQ7jxC5XdkYQKBgG/d4vFCtSc+EW2iEXTXp9HXxVPiiVWRywMh1Eg5NFwb5ktZhzmlFELWhmINphH21A3quI21dqTgZ9+SCD/iGQM7SA9FhewC6+HhNAG7NaVdruUR13TY2ClN9c0K70vIwnZ4As0zYW9tVQoZht39eFZofSNUtrCWEG0FYqdB0DAlAoGAegN7ayif49xxbq6qcl6Y5Eim+EMgqyuzQp1NpSVtUsNdeAGURUc+XUe6rUjj9Rl0HAmkrdeWE0Ic9cwiEVSV/xjACGIRSuGrnbOmWmJM5YLfdVAz4tcOxg3TnkBVCFGSJFQvUhw8oKoGRbnCHg8ekTWdQVtyMConTZWSkpSV8WECgYBOsIGdnneo1Th6Z4jeoEzfecKSu2HG1tiRFGqMA6Yk8NYPw1L7UAiF1jQy3RFqD+DkXhSV4UR9+Etyht9aAx2FCcwJhXiLxL7H/792zoV0ekGs06VozKXlWXJ4US39c9Ad3nf3QBuKWXup78AOxeIWA1RST9Aa6bZD9k1A30RrAw==";
    private static String APP_ID = "2021002192698368";
    //应用公钥
    private static String APP_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAg/fhKU/qvoJg7B3xFqDU1+D4Up8eseOm/9hLwUt63opBlakDl5Z0aIgIejrP50rOiWobTXvoYvSmb04kz4UvLWLIZ1i/2a3ftE6MReBGfr4uWj404OECEC0v+k46ht6SsTDCywowAFpHlcdmheACscahFQbrF+o2izSUvECFTFmmCA3FRckGP8+YT0NIzx7n/jDKgLv25TtmuZfP3/JzTJd9uwFNjrFJvKjDUVwxyn493e2EQVAxdixutGKkn9TXYXl9BNPIXOCQklEXSrWRmer6gzQ52rmCKLgM8o9nOmjm4lm5HPJ9SqD7AmvGbQa1YeJV3Eho7cCd4WTMktpQJQIDAQAB";
    //支付宝公钥
    private static String ALIPAY_PUBLIC_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAtvqGNvy84HQhwYcNt0IRNFj6IahdK9aL0A15DDpp+cr6c7TGyOsbPNKBUMVAIImjQnbaikZq2eyMMeYG5eKjRaXTcIZ8yFdtQOePsBGsgoW/JhvIbzOKiWwMtpmUxbUKixCZH64LYi+f32ZkhsYjymgdCZJdgEwyAihg6lxpnLY3toxHESXVKu2VDC6mb3q7pKs53s6EaP2uM4f2GTTN9g6yG8eI0RlUE+kf+QOmxEVv2SOPZp84NjfIjRLlPdTToPL+zZ9jBCZliQgcTpQgNrO5R9YOgOR8UmLj/WNh0ASOC8HtR6cQL+F8soFv87kcClFTzW5XnWQi57O1L+Dg9QIDAQAB";
    //支付宝公钥证书
//    private static String  ALIPAY_PUBLIC_CERTPATH ="E:\\MyFold\\wendang\\支付宝开放平台开发助手\\支付宝公钥证书alipayCertPublicKey_RSA2.crt";
    //支付宝根证书
//    private static String  ALIPAY_ROOT_CERTPATH ="E:\\MyFold\\wendang\\支付宝开放平台开发助手\\支付宝根证书alipayRootCert.crt";
    //应用证书
//    private static String  APP_CERTPATH ="E:\\MyFold\\wendang\\支付宝开放平台开发助手\\应用公钥证书appCertPublicKey_2021002192698368.crt";
    //异步回调地址  支付成功后,支付宝后台会向此地址发送通知
    private static String NOTIFY = "https://www.bizhilou.com/dev-api/alipay/notify_url";
    //同步回调地址, 支付成功后跳转回的页面
    private static String CALLBACK = "https://www.bizhilou.com/dev-api/alipay/return_url";

    private AlipayClient alipayClient = new DefaultAlipayClient(GATEWAY, APP_ID, APP_PRIVATE_KEY, "JSON", "UTF-8", APP_PUBLIC_KEY, "RSA2");


    /*public String getOrderDetailByalipayid(String orderid, String outorderid) {
        AlipayTradeQueryResponse response = null;

        try {

            AlipayTradeQueryRequest request = new AlipayTradeQueryRequest();
            JSONObject bizContent = new JSONObject();
            // 支付宝订单号与本地订单号 ,不能同时为空
            bizContent.put("out_trade_no", "20150320010101001");//内部订单号
            bizContent.put("trade_no", orderid); //支付宝订单号
            request.setBizContent(bizContent.toString());
            response = alipayClient.pageExecute(request);
            if (response.isSuccess()) {

                System.out.println(response.getBody());

                return "success";
            }

        } catch (Exception e) {
            System.out.println("调用遭遇异常，原因：" + e.getMessage());


        }
        return "fail";
    }*/


    public String getPayPage(String userId, String orderNumber, BigDecimal total_amount, String subject, String ptype) {
        try {
            //实例化客户端
            //     AlipayClient alipayClient = new DefaultAlipayClient(GATEWAY, APP_ID, APP_PRIVATE_KEY, "JSON", "UTF-8", APP_PUBLIC_KEY, "RSA2");

            AlipayTradePagePayRequest request = new AlipayTradePagePayRequest();
            request.setNotifyUrl(NOTIFY);
            request.setReturnUrl(CALLBACK);
            JSONObject bizContent = new JSONObject();
           /* bizContent.put("out_trade_no", body.getParameter("out_trade_no")+"");
            bizContent.put("total_amount", Float.parseFloat(body.getParameter("total_amount")));
            bizContent.put("subject", body.getParameter("subject")+"");
            bizContent.put("userId", body.getParameter("userId")+"");
            bizContent.put("out_trade_no", body.getParameter("out_trade_no")+"");*/
            bizContent.put("out_trade_no", orderNumber);
            bizContent.put("total_amount", total_amount);
            bizContent.put("subject", subject);
            bizContent.put("product_code", "FAST_INSTANT_TRADE_PAY");
            //bizContent.put("time_expire", "2022-08-01 22:00:00");

            // 商品明细信息，按需传入
          /*  JSONArray goodsDetail = new JSONArray();
            JSONObject goods1 = new JSONObject();
            goods1.put("goods_name", subject);
            goods1.put("userId", userId);
            goods1.put("ptype", ptype);
            //   goods1.put("buyer", body.getParameter("buyer"));
            goodsDetail.add(goods1);
            bizContent.put("goods_detail", goodsDetail);*/

            //// 扩展信息，按需传入
            //JSONObject extendParams = new JSONObject();
            //extendParams.put("sys_service_provider_id", "2088511833207846");
            //bizContent.put("extend_params", extendParams);

            request.setBizContent(bizContent.toString());

            AlipayTradePagePayResponse response = alipayClient.pageExecute(request);

            //调用成功，则处理业务逻辑
            if (response.isSuccess()) {
                System.out.println("拉取支付页面成功");
                return response.getBody();//此内容返回给前端
            }
            return response.getMsg();
        } catch (Exception e) {
            System.out.println("调用遭遇异常，原因：" + e.getMessage());

        }
        return "";
    }

}
