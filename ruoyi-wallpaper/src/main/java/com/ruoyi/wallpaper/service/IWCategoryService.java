package com.ruoyi.wallpaper.service;

import java.util.List;

import com.ruoyi.wallpaper.domain.WCategory;

/**
 * 文件分类Service接口
 *
 * @author weiwei
 * @date 2022-03-29
 */
public interface IWCategoryService {
    /**
     * 查询文件分类
     *
     * @param cId 文件分类主键
     * @return 文件分类
     */
    public WCategory selectWCategoryByCId(Integer cId);

    /**
     * 查询文件分类列表
     *
     * @return 文件分类集合
     * @Param 所属的一级分类1视频0图片
     */
    public List<WCategory> selectNameList(Integer type);

    /**
     * 查询文件分类列表
     *
     * @param wCategory 文件分类
     * @return 文件分类集合
     */
    public List<WCategory> selectWCategoryList(WCategory wCategory);

    /**
     * 新增文件分类
     *
     * @param wCategory 文件分类
     * @return 结果
     */
    public int insertWCategory(WCategory wCategory);

    /**
     * 修改文件分类
     *
     * @param wCategory 文件分类
     * @return 结果
     */
    public int updateWCategory(WCategory wCategory);

    /**
     * 批量删除文件分类
     *
     * @param cIds 需要删除的文件分类主键集合
     * @return 结果
     */
    public int deleteWCategoryByCIds(Integer[] cIds);

    /**
     * 删除文件分类信息
     *
     * @param cId 文件分类主键
     * @return 结果
     */
    public int deleteWCategoryByCId(Integer cId);
}
