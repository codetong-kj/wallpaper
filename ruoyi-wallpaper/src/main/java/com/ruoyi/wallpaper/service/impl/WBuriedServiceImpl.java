package com.ruoyi.wallpaper.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.WBuriedMapper;
import com.ruoyi.wallpaper.domain.WBuried;
import com.ruoyi.wallpaper.service.IWBuriedService;

/**
 * 埋点Service业务层处理
 *
 * @author weiwei
 * @date 2021-12-23
 */
@Service
public class WBuriedServiceImpl implements IWBuriedService {
    @Autowired
    private WBuriedMapper wBuriedMapper;

    /**
     * 查询埋点
     *
     * @param id 埋点主键
     * @return 埋点
     */
    @Override
    public WBuried selectWBuriedById(Long id) {
        return wBuriedMapper.selectWBuriedById(id);
    }

    @Override
    public int updateWBuriedByIp(WBuried wBuried) {
        return wBuriedMapper.updateWBuriedByIp(wBuried);
    }

    @Override
    public int selectWBuriedIdByUserId(String userId) {
        return wBuriedMapper.selectWBuriedIdByUserId(userId);
    }

    /**
     * 查询埋点列表
     *
     * @param wBuried 埋点
     * @return 埋点
     */
    @Override
    public List<WBuried> selectWBuriedList(WBuried wBuried) {
        return wBuriedMapper.selectWBuriedList(wBuried);
    }

    @Override
    public Integer getWBuriedByIp(String ip) {
        return wBuriedMapper.getWBuriedByIp(ip);
    }

    /**
     * 新增埋点
     *
     * @param wBuried 埋点
     * @return 结果
     */
    @Override
    public int insertWBuried(WBuried wBuried) {
        wBuried.setCreateTime(DateUtils.getNowDate());
        return wBuriedMapper.insertWBuried(wBuried);
    }

    /**
     * 修改埋点
     *
     * @param wBuried 埋点
     * @return 结果
     */
    @Override
    public int updateWBuried(WBuried wBuried) {
        wBuried.setUpdateTime(DateUtils.getNowDate());
        return wBuriedMapper.updateWBuried(wBuried);
    }

    /**
     * 批量删除埋点
     *
     * @param ids 需要删除的埋点主键
     * @return 结果
     */
    @Override
    public int deleteWBuriedByIds(Long[] ids) {
        return wBuriedMapper.deleteWBuriedByIds(ids);
    }

    /**
     * 删除埋点信息
     *
     * @param id 埋点主键
     * @return 结果
     */
    @Override
    public int deleteWBuriedById(Long id) {
        return wBuriedMapper.deleteWBuriedById(id);
    }
}
