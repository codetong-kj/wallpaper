package com.ruoyi.wallpaper.service.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.WBuriedDayLiveMapper;
import com.ruoyi.wallpaper.domain.WBuriedDayLive;
import com.ruoyi.wallpaper.service.IWBuriedDayLiveService;

/**
 * 埋点日活Service业务层处理
 *
 * @author weiwei
 * @date 2021-12-23
 */
@Service
public class WBuriedDayLiveServiceImpl implements IWBuriedDayLiveService {
    @Autowired
    private WBuriedDayLiveMapper wBuriedDayLiveMapper;

    /**
     * 查询埋点日活
     *
     * @param dId 埋点日活主键
     * @return 埋点日活
     */
    @Override
    public WBuriedDayLive selectWBuriedDayLiveByDId(Long dId) {
        return wBuriedDayLiveMapper.selectWBuriedDayLiveByDId(dId);
    }

    /**
     * 查询埋点日活列表
     *
     * @param wBuriedDayLive 埋点日活
     * @return 埋点日活
     */
    @Override
    public List<WBuriedDayLive> selectWBuriedDayLiveList(WBuriedDayLive wBuriedDayLive) {
        return wBuriedDayLiveMapper.selectWBuriedDayLiveList(wBuriedDayLive);
    }

    /**
     * @Description 近一个月的
     * @Author weiwei
     * @Date 2022/1/6 22:19
     * @Param
     * @Return
     * @Exception
     */
    @Override
    public List<Map<String,Object>> selectStartMoonCount() {
        return wBuriedDayLiveMapper.selectStartMoonCount();
    }

    /**
     * @Description 返回每日软件启动的次数
     * @Author weiwei
     * @Date 2022/1/6 17:20
     * @Param
     * @Return 次数
     * @Exception
     */
    @Override
    public Integer selectStartCount() {
        return wBuriedDayLiveMapper.selectStartCount();
    }

    /**
     * 新增埋点日活
     *
     * @param wBuriedDayLive 埋点日活
     * @return 结果
     */
    @Override
    public int insertWBuriedDayLive(WBuriedDayLive wBuriedDayLive) {
        wBuriedDayLive.setCreateTime(DateUtils.getNowDate());
        return wBuriedDayLiveMapper.insertWBuriedDayLive(wBuriedDayLive);
    }

    @Override
    public int updateBuriedEndTime(String userId, String ip) {
        return wBuriedDayLiveMapper.updateBuriedEndTime(userId, ip);
    }

    /**
     * 修改埋点日活
     *
     * @param wBuriedDayLive 埋点日活
     * @return 结果
     */
    @Override
    public int updateWBuriedDayLive(WBuriedDayLive wBuriedDayLive) {
        wBuriedDayLive.setUpdateTime(DateUtils.getNowDate());
        return wBuriedDayLiveMapper.updateWBuriedDayLive(wBuriedDayLive);
    }

    /**
     * 批量删除埋点日活
     *
     * @param dIds 需要删除的埋点日活主键
     * @return 结果
     */
    @Override
    public int deleteWBuriedDayLiveByDIds(Long[] dIds) {
        return wBuriedDayLiveMapper.deleteWBuriedDayLiveByDIds(dIds);
    }

    /**
     * 删除埋点日活信息
     *
     * @param dId 埋点日活主键
     * @return 结果
     */
    @Override
    public int deleteWBuriedDayLiveByDId(Long dId) {
        return wBuriedDayLiveMapper.deleteWBuriedDayLiveByDId(dId);
    }
}
