package com.ruoyi.wallpaper.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.WCategoryMapper;
import com.ruoyi.wallpaper.domain.WCategory;
import com.ruoyi.wallpaper.service.IWCategoryService;

/**
 * 文件分类Service业务层处理
 *
 * @author weiwei
 * @date 2022-03-29
 */
@Service
public class WCategoryServiceImpl implements IWCategoryService {
    @Autowired
    private WCategoryMapper wCategoryMapper;

    /**
     * 查询文件分类
     *
     * @param cId 文件分类主键
     * @return 文件分类
     */
    @Override
    public WCategory selectWCategoryByCId(Integer cId) {
        return wCategoryMapper.selectWCategoryByCId(cId);
    }

    /**
     * 查询文件分类列表
     * @Param 所属的一级分类1视频0图片
     * @return 文件分类集合
     */
    @Override
    public List<WCategory> selectNameList(Integer type) {
        return wCategoryMapper.selectNameList(type);
    }

    /**
     * 查询文件分类列表
     *
     * @param wCategory 文件分类
     * @return 文件分类
     */
    @Override
    public List<WCategory> selectWCategoryList(WCategory wCategory) {
        return wCategoryMapper.selectWCategoryList(wCategory);
    }

    /**
     * 新增文件分类
     *
     * @param wCategory 文件分类
     * @return 结果
     */
    @Override
    public int insertWCategory(WCategory wCategory) {
        wCategory.setCreateTime(DateUtils.getNowDate());
        return wCategoryMapper.insertWCategory(wCategory);
    }

    /**
     * 修改文件分类
     *
     * @param wCategory 文件分类
     * @return 结果
     */
    @Override
    public int updateWCategory(WCategory wCategory) {
        wCategory.setUpdateTime(DateUtils.getNowDate());
        return wCategoryMapper.updateWCategory(wCategory);
    }

    /**
     * 批量删除文件分类
     *
     * @param cIds 需要删除的文件分类主键
     * @return 结果
     */
    @Override
    public int deleteWCategoryByCIds(Integer[] cIds) {
        return wCategoryMapper.deleteWCategoryByCIds(cIds);
    }

    /**
     * 删除文件分类信息
     *
     * @param cId 文件分类主键
     * @return 结果
     */
    @Override
    public int deleteWCategoryByCId(Integer cId) {
        return wCategoryMapper.deleteWCategoryByCId(cId);
    }
}
