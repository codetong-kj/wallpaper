package com.ruoyi.wallpaper.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.WallpaperCommentMapper;
import com.ruoyi.wallpaper.domain.WallpaperComment;
import com.ruoyi.wallpaper.service.IWallpaperCommentService;

/**
 * 用户评论Service业务层处理
 * 
 * @author weiwei
 * @date 2021-11-10
 */
@Service
public class WallpaperCommentServiceImpl implements IWallpaperCommentService 
{
    @Autowired
    private WallpaperCommentMapper wallpaperCommentMapper;

    /**
     * 查询用户评论
     * 
     * @param commentId 用户评论主键
     * @return 用户评论
     */
    @Override
    public WallpaperComment selectWallpaperCommentByCommentId(Long commentId)
    {
        return wallpaperCommentMapper.selectWallpaperCommentByCommentId(commentId);
    }

    /**
     * 查询用户评论列表
     * 
     * @param wallpaperComment 用户评论
     * @return 用户评论
     */
    @Override
    public List<WallpaperComment> selectWallpaperCommentList(WallpaperComment wallpaperComment)
    {
        return wallpaperCommentMapper.selectWallpaperCommentList(wallpaperComment);
    }

    /**
     * 新增用户评论
     * 
     * @param wallpaperComment 用户评论
     * @return 结果
     */
    @Override
    public int insertWallpaperComment(WallpaperComment wallpaperComment)
    {
        wallpaperComment.setCreateTime(DateUtils.getNowDate());
        return wallpaperCommentMapper.insertWallpaperComment(wallpaperComment);
    }

    /**
     * 修改用户评论
     * 
     * @param wallpaperComment 用户评论
     * @return 结果
     */
    @Override
    public int updateWallpaperComment(WallpaperComment wallpaperComment)
    {
        wallpaperComment.setUpdateTime(DateUtils.getNowDate());
        return wallpaperCommentMapper.updateWallpaperComment(wallpaperComment);
    }

    /**
     * 批量删除用户评论
     * 
     * @param commentIds 需要删除的用户评论主键
     * @return 结果
     */
    @Override
    public int deleteWallpaperCommentByCommentIds(Long[] commentIds)
    {
        return wallpaperCommentMapper.deleteWallpaperCommentByCommentIds(commentIds);
    }

    /**
     * 删除用户评论信息
     * 
     * @param commentId 用户评论主键
     * @return 结果
     */
    @Override
    public int deleteWallpaperCommentByCommentId(Long commentId)
    {
        return wallpaperCommentMapper.deleteWallpaperCommentByCommentId(commentId);
    }
}
