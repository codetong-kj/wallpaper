package com.ruoyi.wallpaper.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.WallpaperOrderDetailMapper;
import com.ruoyi.wallpaper.domain.WallpaperOrderDetail;
import com.ruoyi.wallpaper.service.IWallpaperOrderDetailService;

/**
 * 订单明细Service业务层处理
 *
 * @author weiwei
 * @date 2021-12-17
 */
@Service
public class WallpaperOrderDetailServiceImpl implements IWallpaperOrderDetailService {
    @Autowired
    private WallpaperOrderDetailMapper wallpaperOrderDetailMapper;

    @Override
    public int updateWallpaperOrderDetailByOrderNumber(WallpaperOrderDetail wallpaperOrderDetail) {
        return wallpaperOrderDetailMapper.updateWallpaperOrderDetailByOrderNumber(wallpaperOrderDetail);
    }

    @Override
    public WallpaperOrderDetail selectWallpaperOrderDetailByOrderNum(String orderNumber) {
        return wallpaperOrderDetailMapper.selectWallpaperOrderDetailByOrderNum(orderNumber);
    }

    /**
     * 查询订单明细
     *
     * @param id 订单明细主键
     * @return 订单明细
     */
    @Override
    public WallpaperOrderDetail selectWallpaperOrderDetailById(Long id) {
        return wallpaperOrderDetailMapper.selectWallpaperOrderDetailById(id);
    }

    /**
     * 查询订单明细列表
     *
     * @param wallpaperOrderDetail 订单明细
     * @return 订单明细
     */
    @Override
    public List<WallpaperOrderDetail> selectWallpaperOrderDetailList(WallpaperOrderDetail wallpaperOrderDetail) {
        return wallpaperOrderDetailMapper.selectWallpaperOrderDetailList(wallpaperOrderDetail);
    }

    /**
     * 新增订单明细
     *
     * @param wallpaperOrderDetail 订单明细
     * @return 结果
     */
    @Override
    public int insertWallpaperOrderDetail(WallpaperOrderDetail wallpaperOrderDetail) {
        wallpaperOrderDetail.setCreateTime(DateUtils.getNowDate());
        return wallpaperOrderDetailMapper.insertWallpaperOrderDetail(wallpaperOrderDetail);
    }

    /**
     * 修改订单明细
     *
     * @param wallpaperOrderDetail 订单明细
     * @return 结果
     */
    @Override
    public int updateWallpaperOrderDetail(WallpaperOrderDetail wallpaperOrderDetail) {
        wallpaperOrderDetail.setUpdateTime(DateUtils.getNowDate());
        return wallpaperOrderDetailMapper.updateWallpaperOrderDetail(wallpaperOrderDetail);
    }

    /**
     * 批量删除订单明细
     *
     * @param ids 需要删除的订单明细主键
     * @return 结果
     */
    @Override
    public int deleteWallpaperOrderDetailByIds(Long[] ids) {
        return wallpaperOrderDetailMapper.deleteWallpaperOrderDetailByIds(ids);
    }

    /**
     * 删除订单明细信息
     *
     * @param id 订单明细主键
     * @return 结果
     */
    @Override
    public int deleteWallpaperOrderDetailById(Long id) {
        return wallpaperOrderDetailMapper.deleteWallpaperOrderDetailById(id);
    }
}
