package com.ruoyi.wallpaper.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.wallpaper.domain.WUpdateItem;
import com.ruoyi.wallpaper.mapper.WUpdateMapper;
import com.ruoyi.wallpaper.domain.WUpdate;
import com.ruoyi.wallpaper.service.IWUpdateService;

/**
 * 软件更新Service业务层处理
 * 
 * @author weiwei
 * @date 2021-11-18
 */
@Service
public class WUpdateServiceImpl implements IWUpdateService 
{
    @Autowired
    private WUpdateMapper wUpdateMapper;

    @Override
    public List<WUpdate> selectWUpdateAllList() {
        return wUpdateMapper.selectWUpdateAllList();
    }

    /**
     * 查询软件更新
     * 
     * @param updateId 软件更新主键
     * @return 软件更新
     */
    @Override
    public WUpdate selectWUpdateByUpdateId(Long updateId)
    {
        return wUpdateMapper.selectWUpdateByUpdateId(updateId);
    }

    @Override
    public WUpdate getLastUpdate() {
        return wUpdateMapper.getLastUpdate();
    }

    /**
     * 查询软件更新列表
     * 
     * @param wUpdate 软件更新
     * @return 软件更新
     */
    @Override
    public List<WUpdate> selectWUpdateList(WUpdate wUpdate)
    {
        return wUpdateMapper.selectWUpdateList(wUpdate);
    }

    /**
     * 新增软件更新
     * 
     * @param wUpdate 软件更新
     * @return 结果
     */
    @Transactional
    @Override
    public int insertWUpdate(WUpdate wUpdate)
    {
        wUpdate.setCreateTime(DateUtils.getNowDate());
        int rows = wUpdateMapper.insertWUpdate(wUpdate);
        insertWUpdateItem(wUpdate);
        return rows;
    }

    /**
     * 修改软件更新
     * 
     * @param wUpdate 软件更新
     * @return 结果
     */
    @Transactional
    @Override
    public int updateWUpdate(WUpdate wUpdate)
    {
        wUpdate.setUpdateTime(DateUtils.getNowDate());
        wUpdateMapper.deleteWUpdateItemByUpdateId(wUpdate.getUpdateId());
        insertWUpdateItem(wUpdate);
        return wUpdateMapper.updateWUpdate(wUpdate);
    }

    /**
     * 批量删除软件更新
     * 
     * @param updateIds 需要删除的软件更新主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteWUpdateByUpdateIds(Long[] updateIds)
    {
        wUpdateMapper.deleteWUpdateItemByUpdateIds(updateIds);
        return wUpdateMapper.deleteWUpdateByUpdateIds(updateIds);
    }

    /**
     * 删除软件更新信息
     * 
     * @param updateId 软件更新主键
     * @return 结果
     */
    @Override
    public int deleteWUpdateByUpdateId(Long updateId)
    {
        wUpdateMapper.deleteWUpdateItemByUpdateId(updateId);
        return wUpdateMapper.deleteWUpdateByUpdateId(updateId);
    }

    /**
     * 新增更新明细信息
     * 
     * @param wUpdate 软件更新对象
     */
    public void insertWUpdateItem(WUpdate wUpdate)
    {
        List<WUpdateItem> wUpdateItemList = wUpdate.getWUpdateItemList();
        Long updateId = wUpdate.getUpdateId();
        if (StringUtils.isNotNull(wUpdateItemList))
        {
            List<WUpdateItem> list = new ArrayList<WUpdateItem>();
            for (WUpdateItem wUpdateItem : wUpdateItemList)
            {
                wUpdateItem.setUpdateId(updateId);
                list.add(wUpdateItem);
            }
            if (list.size() > 0)
            {
                wUpdateMapper.batchWUpdateItem(list);
            }
        }
    }
}
