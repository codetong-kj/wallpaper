package com.ruoyi.wallpaper.service.impl;

import java.util.List;

import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wallpaper.domain.WorkResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

import com.ruoyi.common.utils.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import com.ruoyi.wallpaper.domain.WFile;
import com.ruoyi.wallpaper.mapper.WUserWorksMapper;
import com.ruoyi.wallpaper.domain.WUserWorks;
import com.ruoyi.wallpaper.service.IWUserWorksService;

/**
 * 用户作品Service业务层处理
 *
 * @author weiwei
 * @date 2022-02-11
 */
@Service
public class WUserWorksServiceImpl implements IWUserWorksService {
    @Autowired
    private WUserWorksMapper wUserWorksMapper;

    /**
     * @Description
     * @Author weiwei
     * @Date 2022/2/15 22:57
     * @Param
     * @Return 返回用户作品列表
     * @Exception
     */
    @Override
    public List<WorkResult> selectWorksList(WUserWorks wUserWorks) {
        return wUserWorksMapper.selectWorksList(wUserWorks);
    }



    /**
     * 查询用户作品
     *
     * @param workId 用户作品主键
     * @return 用户作品
     */
    @Override
    public WUserWorks selectWUserWorksByWorkId(String workId) {
        return wUserWorksMapper.selectWUserWorksByWorkId(workId);
    }

    /**
     * @param userId
     * @Description
     * @Author weiwei
     * @Date 2022/2/26 20:29
     * @Param userId 用户id
     * @Return 作品列表
     * @Exception 没有
     */
    @Override
    public List<WUserWorks> selectWUserWorksImagesByUserId(String userId) {
        return wUserWorksMapper.selectWUserWorksImagesByUserId(userId);
    }

    /**
     * @param workId
     * @param like
     * @Description 修改用户作品点赞数量
     * @Author weiwei
     * @Date 2022/3/13 23:02
     * @Param
     * @Return
     * @Exception
     */
    @Override
    public int updateLikeByWorkId(String workId, Integer like) {
        return wUserWorksMapper.updateLikeByWorkId(workId, like);
    }

    /**
     * @param userId
     * @param fId
     * @Description 查询用户收藏夹下的某搜藏id下的所有图片
     * @Author weiwei
     * @Date 2022/3/21 19:42
     * @Param
     * @Return
     * @Exception
     */
    @Override
    public List<WorkResult> selectWorkImgByFavorites(String userId, Integer fId) {
        /* if (workResults.size()==0||StringUtils.isNull(workResults.get(0))){
            return null;
        }*/
        return wUserWorksMapper.selectWorkImgByFavorites(userId, fId);
    }

    /**
     * @param userId
     * @param fId
     * @Description 查询用户收藏夹下的某搜藏id下的所有图片
     * @Author weiwei
     * @Date 2022/3/21 19:42
     * @Param
     * @Return
     * @Exception
     */
    @Override
    public List<WorkResult> selectWorkVideoByFavorites(String userId, Integer fId) {
        List<WorkResult> workResults = wUserWorksMapper.selectWorkVideoByFavorites(userId, fId);
        if (workResults.size()==0){
            return null;
        }
        return workResults;
    }

    /**
     * @param userId
     * @Description
     * @Author weiwei
     * @Date 2022/2/26 20:29
     * @Param userId 用户id
     * @Return 作品列表
     * @Exception 没有
     */
    @Override
    public List<WUserWorks> selectWUserWorksVideoByUserId(String userId) {
        return wUserWorksMapper.selectWUserWorksVideoByUserId(userId);
    }

    /**
     * 查询用户作品列表
     *
     * @param wUserWorks 用户作品
     * @return 用户作品
     */
    @Override
    public List<WUserWorks> selectWUserWorksList(WUserWorks wUserWorks) {
        return wUserWorksMapper.selectWUserWorksList(wUserWorks);
    }

    /**
     * 新增用户作品
     *
     * @param wUserWorks 用户作品
     * @return 结果
     */
    @Transactional
    @Override
    public int insertWUserWorks(WUserWorks wUserWorks) {
        wUserWorks.setCreateTime(DateUtils.getNowDate());
        int rows = wUserWorksMapper.insertWUserWorks(wUserWorks);
        insertWFile(wUserWorks);
        return rows;
    }

    /**
     * 修改用户作品
     *
     * @param wUserWorks 用户作品
     * @return 结果
     */
    @Transactional
    @Override
    public int updateWUserWorks(WUserWorks wUserWorks) {
        wUserWorks.setUpdateTime(DateUtils.getNowDate());
        wUserWorksMapper.deleteWFileByWorkId(wUserWorks.getWorkId());
        insertWFile(wUserWorks);
        return wUserWorksMapper.updateWUserWorks(wUserWorks);
    }

    /**
     * 批量删除用户作品
     *
     * @param workIds 需要删除的用户作品主键
     * @return 结果
     */
    @Transactional
    @Override
    public int deleteWUserWorksByWorkIds(String[] workIds) {
        wUserWorksMapper.deleteWFileByWorkIds(workIds);
        return wUserWorksMapper.deleteWUserWorksByWorkIds(workIds);
    }

    /**
     * 删除用户作品信息
     *
     * @param workId 用户作品主键
     * @return 结果
     */
    @Override
    public int deleteWUserWorksByWorkId(String workId) {
        wUserWorksMapper.deleteWFileByWorkId(workId);
        return wUserWorksMapper.deleteWUserWorksByWorkId(workId);
    }

    /**
     * 新增文件信息
     *
     * @param wUserWorks 用户作品对象
     */
    public void insertWFile(WUserWorks wUserWorks) {
        List<WFile> wFileList = wUserWorks.getWFileList();
        String workId = wUserWorks.getWorkId();
        if (StringUtils.isNotNull(wFileList)) {
            List<WFile> list = new ArrayList<WFile>();
            for (WFile wFile : wFileList) {
                wFile.setWorkId(workId);
                list.add(wFile);
            }
            if (list.size() > 0) {
                wUserWorksMapper.batchWFile(list);
            }
        }
    }
}
