package com.ruoyi.wallpaper.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.wallpaper.mapper.WallpaperDynamicMapper;
import com.ruoyi.wallpaper.domain.WallpaperDynamic;
import com.ruoyi.wallpaper.service.IWallpaperDynamicService;

/**
 * 动态壁纸Service业务层处理
 * 
 * @author weiwei
 * @date 2021-11-10
 */
@Service
public class WallpaperDynamicServiceImpl implements IWallpaperDynamicService 
{
    @Autowired
    private WallpaperDynamicMapper wallpaperDynamicMapper;

    /**
     * 查询动态壁纸
     * 
     * @param wId 动态壁纸主键
     * @return 动态壁纸
     */
    @Override
    public WallpaperDynamic selectWallpaperDynamicByWId(String wId)
    {
        return wallpaperDynamicMapper.selectWallpaperDynamicByWId(wId);
    }

    /**
     * 查询动态壁纸列表
     * 
     * @param wallpaperDynamic 动态壁纸
     * @return 动态壁纸
     */
    @Override
    public List<WallpaperDynamic> selectWallpaperDynamicList(WallpaperDynamic wallpaperDynamic)
    {
        return wallpaperDynamicMapper.selectWallpaperDynamicList(wallpaperDynamic);
    }

    /**
     * 新增动态壁纸
     * 
     * @param wallpaperDynamic 动态壁纸
     * @return 结果
     */
    @Override
    public int insertWallpaperDynamic(WallpaperDynamic wallpaperDynamic)
    {
        wallpaperDynamic.setCreateTime(DateUtils.getNowDate());
        return wallpaperDynamicMapper.insertWallpaperDynamic(wallpaperDynamic);
    }

    /**
     * 修改动态壁纸
     * 
     * @param wallpaperDynamic 动态壁纸
     * @return 结果
     */
    @Override
    public int updateWallpaperDynamic(WallpaperDynamic wallpaperDynamic)
    {
        wallpaperDynamic.setUpdateTime(DateUtils.getNowDate());
        return wallpaperDynamicMapper.updateWallpaperDynamic(wallpaperDynamic);
    }

    /**
     * 批量删除动态壁纸
     * 
     * @param wIds 需要删除的动态壁纸主键
     * @return 结果
     */
    @Override
    public int deleteWallpaperDynamicByWIds(String[] wIds)
    {
        return wallpaperDynamicMapper.deleteWallpaperDynamicByWIds(wIds);
    }

    /**
     * 删除动态壁纸信息
     * 
     * @param wId 动态壁纸主键
     * @return 结果
     */
    @Override
    public int deleteWallpaperDynamicByWId(String wId)
    {
        return wallpaperDynamicMapper.deleteWallpaperDynamicByWId(wId);
    }
}
