package com.ruoyi.wallpaper.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wallpaper.domain.Announce;
import com.ruoyi.wallpaper.service.IAnnounceService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 公告管理Controller
 * 
 * @author weiwei
 * @date 2022-03-28
 */
@RestController
@RequestMapping("/wallpaper/announce")
public class AnnounceController extends BaseController
{
    @Autowired
    private IAnnounceService announceService;

    /**
     * 查询公告管理列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:announce:list')")
    @GetMapping("/list")
    public TableDataInfo list(Announce announce)
    {
        startPage();
        List<Announce> list = announceService.selectAnnounceList(announce);
        return getDataTable(list);
    }

    /**
     * 导出公告管理列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:announce:export')")
    @Log(title = "公告管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Announce announce)
    {
        List<Announce> list = announceService.selectAnnounceList(announce);
        ExcelUtil<Announce> util = new ExcelUtil<Announce>(Announce.class);
        return util.exportExcel(list, "公告管理数据");
    }

    /**
     * 获取公告管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:announce:query')")
    @GetMapping(value = "/{aid}")
    public AjaxResult getInfo(@PathVariable("aid") Integer aid)
    {
        return AjaxResult.success(announceService.selectAnnounceByAid(aid));
    }

    /**
     * 新增公告管理
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:announce:add')")
    @Log(title = "公告管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Announce announce)
    {
        return toAjax(announceService.insertAnnounce(announce));
    }

    /**
     * 修改公告管理
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:announce:edit')")
    @Log(title = "公告管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Announce announce)
    {
        return toAjax(announceService.updateAnnounce(announce));
    }

    /**
     * 删除公告管理
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:announce:remove')")
    @Log(title = "公告管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{aids}")
    public AjaxResult remove(@PathVariable Integer[] aids)
    {
        return toAjax(announceService.deleteAnnounceByAids(aids));
    }
}
