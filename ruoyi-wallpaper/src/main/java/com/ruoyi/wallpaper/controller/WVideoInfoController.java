package com.ruoyi.wallpaper.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wallpaper.domain.WVideoInfo;
import com.ruoyi.wallpaper.service.IWVideoInfoService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 视频详情Controller
 * 
 * @author weiwei
 * @date 2022-02-28
 */
@RestController
@RequestMapping("/wallpaper/info")
public class WVideoInfoController extends BaseController
{
    @Autowired
    private IWVideoInfoService wVideoInfoService;

    /**
     * 查询视频详情列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:info:list')")
    @GetMapping("/list")
    public TableDataInfo list(WVideoInfo wVideoInfo)
    {
        startPage();
        List<WVideoInfo> list = wVideoInfoService.selectWVideoInfoList(wVideoInfo);
        return getDataTable(list);
    }

    /**
     * 导出视频详情列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:info:export')")
    @Log(title = "视频详情", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WVideoInfo wVideoInfo)
    {
        List<WVideoInfo> list = wVideoInfoService.selectWVideoInfoList(wVideoInfo);
        ExcelUtil<WVideoInfo> util = new ExcelUtil<WVideoInfo>(WVideoInfo.class);
        return util.exportExcel(list, "视频详情数据");
    }

    /**
     * 获取视频详情详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:info:query')")
    @GetMapping(value = "/{vId}")
    public AjaxResult getInfo(@PathVariable("vId") Integer vId)
    {
        return AjaxResult.success(wVideoInfoService.selectWVideoInfoByVId(vId));
    }

    /**
     * 新增视频详情
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:info:add')")
    @Log(title = "视频详情", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WVideoInfo wVideoInfo)
    {
        return toAjax(wVideoInfoService.insertWVideoInfo(wVideoInfo));
    }

    /**
     * 修改视频详情
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:info:edit')")
    @Log(title = "视频详情", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WVideoInfo wVideoInfo)
    {
        return toAjax(wVideoInfoService.updateWVideoInfo(wVideoInfo));
    }

    /**
     * 删除视频详情
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:info:remove')")
    @Log(title = "视频详情", businessType = BusinessType.DELETE)
	@DeleteMapping("/{vIds}")
    public AjaxResult remove(@PathVariable Integer[] vIds)
    {
        return toAjax(wVideoInfoService.deleteWVideoInfoByVIds(vIds));
    }
}
