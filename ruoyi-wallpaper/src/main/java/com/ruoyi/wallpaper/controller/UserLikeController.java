package com.ruoyi.wallpaper.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wallpaper.domain.UserLike;
import com.ruoyi.wallpaper.service.IUserLikeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户点赞Controller
 * 
 * @author weiwei
 * @date 2022-03-13
 */
@RestController
@RequestMapping("/wallpaper/like")
public class UserLikeController extends BaseController
{
    @Autowired
    private IUserLikeService userLikeService;

    /**
     * 查询用户点赞列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:like:list')")
    @GetMapping("/list")
    public TableDataInfo list(UserLike userLike)
    {
        startPage();
        List<UserLike> list = userLikeService.selectUserLikeList(userLike);
        return getDataTable(list);
    }

    /**
     * 导出用户点赞列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:like:export')")
    @Log(title = "用户点赞", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(UserLike userLike)
    {
        List<UserLike> list = userLikeService.selectUserLikeList(userLike);
        ExcelUtil<UserLike> util = new ExcelUtil<UserLike>(UserLike.class);
        return util.exportExcel(list, "用户点赞数据");
    }

    /**
     * 获取用户点赞详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:like:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id)
    {
        return AjaxResult.success(userLikeService.selectUserLikeById(id));
    }

    /**
     * 新增用户点赞
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:like:add')")
    @Log(title = "用户点赞", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody UserLike userLike)
    {
        return toAjax(userLikeService.insertUserLike(userLike));
    }

    /**
     * 修改用户点赞
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:like:edit')")
    @Log(title = "用户点赞", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody UserLike userLike)
    {
        return toAjax(userLikeService.updateUserLike(userLike));
    }

    /**
     * 删除用户点赞
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:like:remove')")
    @Log(title = "用户点赞", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Integer[] ids)
    {
        return toAjax(userLikeService.deleteUserLikeByIds(ids));
    }
}
