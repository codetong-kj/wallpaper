package com.ruoyi.wallpaper.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wallpaper.domain.WBuriedPay;
import com.ruoyi.wallpaper.service.IWBuriedPayService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 埋点支付Controller
 * 
 * @author weiwei
 * @date 2021-12-23
 */
@RestController
@RequestMapping("/wallpaper/pay")
public class WBuriedPayController extends BaseController
{
    @Autowired
    private IWBuriedPayService wBuriedPayService;

    /**
     * 查询埋点支付列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:pay:list')")
    @GetMapping("/list")
    public TableDataInfo list(WBuriedPay wBuriedPay)
    {
        startPage();
        List<WBuriedPay> list = wBuriedPayService.selectWBuriedPayList(wBuriedPay);
        return getDataTable(list);
    }

    /**
     * 导出埋点支付列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:pay:export')")
    @Log(title = "埋点支付", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WBuriedPay wBuriedPay)
    {
        List<WBuriedPay> list = wBuriedPayService.selectWBuriedPayList(wBuriedPay);
        ExcelUtil<WBuriedPay> util = new ExcelUtil<WBuriedPay>(WBuriedPay.class);
        return util.exportExcel(list, "埋点支付数据");
    }

    /**
     * 获取埋点支付详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:pay:query')")
    @GetMapping(value = "/{vId}")
    public AjaxResult getInfo(@PathVariable("vId") Long vId)
    {
        return AjaxResult.success(wBuriedPayService.selectWBuriedPayByVId(vId));
    }

    /**
     * 新增埋点支付
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:pay:add')")
    @Log(title = "埋点支付", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WBuriedPay wBuriedPay)
    {
        return toAjax(wBuriedPayService.insertWBuriedPay(wBuriedPay));
    }

    /**
     * 修改埋点支付
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:pay:edit')")
    @Log(title = "埋点支付", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WBuriedPay wBuriedPay)
    {
        return toAjax(wBuriedPayService.updateWBuriedPay(wBuriedPay));
    }

    /**
     * 删除埋点支付
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:pay:remove')")
    @Log(title = "埋点支付", businessType = BusinessType.DELETE)
	@DeleteMapping("/{vIds}")
    public AjaxResult remove(@PathVariable Long[] vIds)
    {
        return toAjax(wBuriedPayService.deleteWBuriedPayByVIds(vIds));
    }
}
