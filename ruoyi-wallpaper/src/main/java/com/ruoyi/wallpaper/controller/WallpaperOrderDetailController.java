package com.ruoyi.wallpaper.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wallpaper.domain.WallpaperOrderDetail;
import com.ruoyi.wallpaper.service.IWallpaperOrderDetailService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 订单明细Controller
 * 
 * @author weiwei
 * @date 2022-03-28
 */
@RestController
@RequestMapping("/wallpaper/detail")
public class WallpaperOrderDetailController extends BaseController
{
    @Autowired
    private IWallpaperOrderDetailService wallpaperOrderDetailService;

    /**
     * 查询订单明细列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:detail:list')")
    @GetMapping("/list")
    public TableDataInfo list(WallpaperOrderDetail wallpaperOrderDetail)
    {
        startPage();
        List<WallpaperOrderDetail> list = wallpaperOrderDetailService.selectWallpaperOrderDetailList(wallpaperOrderDetail);
        return getDataTable(list);
    }

    /**
     * 导出订单明细列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:detail:export')")
    @Log(title = "订单明细", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WallpaperOrderDetail wallpaperOrderDetail)
    {
        List<WallpaperOrderDetail> list = wallpaperOrderDetailService.selectWallpaperOrderDetailList(wallpaperOrderDetail);
        ExcelUtil<WallpaperOrderDetail> util = new ExcelUtil<WallpaperOrderDetail>(WallpaperOrderDetail.class);
        return util.exportExcel(list, "订单明细数据");
    }

    /**
     * 获取订单明细详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:detail:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(wallpaperOrderDetailService.selectWallpaperOrderDetailById(id));
    }

    /**
     * 新增订单明细
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:detail:add')")
    @Log(title = "订单明细", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WallpaperOrderDetail wallpaperOrderDetail)
    {
        return toAjax(wallpaperOrderDetailService.insertWallpaperOrderDetail(wallpaperOrderDetail));
    }

    /**
     * 修改订单明细
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:detail:edit')")
    @Log(title = "订单明细", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WallpaperOrderDetail wallpaperOrderDetail)
    {
        return toAjax(wallpaperOrderDetailService.updateWallpaperOrderDetail(wallpaperOrderDetail));
    }

    /**
     * 删除订单明细
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:detail:remove')")
    @Log(title = "订单明细", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wallpaperOrderDetailService.deleteWallpaperOrderDetailByIds(ids));
    }
}
