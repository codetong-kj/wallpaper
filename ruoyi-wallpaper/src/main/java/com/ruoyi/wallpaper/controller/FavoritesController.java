package com.ruoyi.wallpaper.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wallpaper.domain.Favorites;
import com.ruoyi.wallpaper.service.IFavoritesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 个人收藏Controller
 * 
 * @author weiwei
 * @date 2022-03-16
 */
@RestController
@RequestMapping("/wallpaper/favorites")
public class FavoritesController extends BaseController
{
    @Autowired
    private IFavoritesService favoritesService;

    /**
     * 查询个人收藏列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:favorites:list')")
    @GetMapping("/list")
    public TableDataInfo list(Favorites favorites)
    {
        startPage();
        List<Favorites> list = favoritesService.selectFavoritesList(favorites);
        return getDataTable(list);
    }

    /**
     * 导出个人收藏列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:favorites:export')")
    @Log(title = "个人收藏", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Favorites favorites)
    {
        List<Favorites> list = favoritesService.selectFavoritesList(favorites);
        ExcelUtil<Favorites> util = new ExcelUtil<Favorites>(Favorites.class);
        return util.exportExcel(list, "个人收藏数据");
    }

    /**
     * 获取个人收藏详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:favorites:query')")
    @GetMapping(value = "/{fId}")
    public AjaxResult getInfo(@PathVariable("fId") Integer fId)
    {
        return AjaxResult.success(favoritesService.selectFavoritesByFId(fId));
    }

    /**
     * 新增个人收藏
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:favorites:add')")
    @Log(title = "个人收藏", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Favorites favorites)
    {
        return toAjax(favoritesService.insertFavorites(favorites));
    }

    /**
     * 修改个人收藏
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:favorites:edit')")
    @Log(title = "个人收藏", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Favorites favorites)
    {
        return toAjax(favoritesService.updateFavorites(favorites));
    }

    /**
     * 删除个人收藏
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:favorites:remove')")
    @Log(title = "个人收藏", businessType = BusinessType.DELETE)
	@DeleteMapping("/{fIds}")
    public AjaxResult remove(@PathVariable Integer[] fIds)
    {
        return toAjax(favoritesService.deleteFavoritesByFIds(fIds));
    }
}
