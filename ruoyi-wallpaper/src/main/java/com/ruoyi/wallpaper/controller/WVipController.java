package com.ruoyi.wallpaper.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wallpaper.domain.WVip;
import com.ruoyi.wallpaper.service.IWVipService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 会员管理Controller
 * 
 * @author weiwei
 * @date 2022-03-28
 */
@RestController
@RequestMapping("/wallpaper/vip")
public class WVipController extends BaseController
{
    @Autowired
    private IWVipService wVipService;

    /**
     * 查询会员管理列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:vip:list')")
    @GetMapping("/list")
    public TableDataInfo list(WVip wVip)
    {
        startPage();
        List<WVip> list = wVipService.selectWVipList(wVip);
        return getDataTable(list);
    }

    /**
     * 导出会员管理列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:vip:export')")
    @Log(title = "会员管理", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WVip wVip)
    {
        List<WVip> list = wVipService.selectWVipList(wVip);
        ExcelUtil<WVip> util = new ExcelUtil<WVip>(WVip.class);
        return util.exportExcel(list, "会员管理数据");
    }

    /**
     * 获取会员管理详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:vip:query')")
    @GetMapping(value = "/{vipId}")
    public AjaxResult getInfo(@PathVariable("vipId") Long vipId)
    {
        return AjaxResult.success(wVipService.selectWVipByVipId(vipId));
    }

    /**
     * 新增会员管理
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:vip:add')")
    @Log(title = "会员管理", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WVip wVip)
    {
        return toAjax(wVipService.insertWVip(wVip));
    }

    /**
     * 修改会员管理
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:vip:edit')")
    @Log(title = "会员管理", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WVip wVip)
    {
        return toAjax(wVipService.updateWVip(wVip));
    }

    /**
     * 删除会员管理
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:vip:remove')")
    @Log(title = "会员管理", businessType = BusinessType.DELETE)
	@DeleteMapping("/{vipIds}")
    public AjaxResult remove(@PathVariable Long[] vipIds)
    {
        return toAjax(wVipService.deleteWVipByVipIds(vipIds));
    }
}
