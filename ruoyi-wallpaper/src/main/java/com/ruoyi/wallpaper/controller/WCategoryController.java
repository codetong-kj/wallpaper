package com.ruoyi.wallpaper.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wallpaper.domain.WCategory;
import com.ruoyi.wallpaper.service.IWCategoryService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 文件分类Controller
 * 
 * @author weiwei
 * @date 2022-03-29
 */
@RestController
@RequestMapping("/wallpaper/category")
public class WCategoryController extends BaseController
{
    @Autowired
    private IWCategoryService wCategoryService;

    /**
     * 查询文件分类列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:category:list')")
    @GetMapping("/list")
    public TableDataInfo list(WCategory wCategory)
    {
        startPage();
        List<WCategory> list = wCategoryService.selectWCategoryList(wCategory);
        return getDataTable(list);
    }

    /**
     * 导出文件分类列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:category:export')")
    @Log(title = "文件分类", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WCategory wCategory)
    {
        List<WCategory> list = wCategoryService.selectWCategoryList(wCategory);
        ExcelUtil<WCategory> util = new ExcelUtil<WCategory>(WCategory.class);
        return util.exportExcel(list, "文件分类数据");
    }

    /**
     * 获取文件分类详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:category:query')")
    @GetMapping(value = "/{cId}")
    public AjaxResult getInfo(@PathVariable("cId") Integer cId)
    {
        return AjaxResult.success(wCategoryService.selectWCategoryByCId(cId));
    }

    /**
     * 新增文件分类
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:category:add')")
    @Log(title = "文件分类", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WCategory wCategory)
    {
        return toAjax(wCategoryService.insertWCategory(wCategory));
    }

    /**
     * 修改文件分类
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:category:edit')")
    @Log(title = "文件分类", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WCategory wCategory)
    {
        return toAjax(wCategoryService.updateWCategory(wCategory));
    }

    /**
     * 删除文件分类
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:category:remove')")
    @Log(title = "文件分类", businessType = BusinessType.DELETE)
	@DeleteMapping("/{cIds}")
    public AjaxResult remove(@PathVariable Integer[] cIds)
    {
        return toAjax(wCategoryService.deleteWCategoryByCIds(cIds));
    }
}
