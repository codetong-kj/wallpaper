package com.ruoyi.wallpaper.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wallpaper.domain.WBuried;
import com.ruoyi.wallpaper.service.IWBuriedService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 埋点Controller
 * 
 * @author weiwei
 * @date 2021-12-23
 */
@RestController
@RequestMapping("/wallpaper/buried")
public class WBuriedController extends BaseController
{
    @Autowired
    private IWBuriedService wBuriedService;

    /**
     * 查询埋点列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:buried:list')")
    @GetMapping("/list")
    public TableDataInfo list(WBuried wBuried)
    {
        startPage();
        List<WBuried> list = wBuriedService.selectWBuriedList(wBuried);
        return getDataTable(list);
    }

    /**
     * 导出埋点列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:buried:export')")
    @Log(title = "埋点", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WBuried wBuried)
    {
        List<WBuried> list = wBuriedService.selectWBuriedList(wBuried);
        ExcelUtil<WBuried> util = new ExcelUtil<WBuried>(WBuried.class);
        return util.exportExcel(list, "埋点数据");
    }

    /**
     * 获取埋点详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:buried:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(wBuriedService.selectWBuriedById(id));
    }

    /**
     * 新增埋点
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:buried:add')")
    @Log(title = "埋点", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WBuried wBuried)
    {
        return toAjax(wBuriedService.insertWBuried(wBuried));
    }

    /**
     * 修改埋点
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:buried:edit')")
    @Log(title = "埋点", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WBuried wBuried)
    {
        return toAjax(wBuriedService.updateWBuried(wBuried));
    }

    /**
     * 删除埋点
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:buried:remove')")
    @Log(title = "埋点", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(wBuriedService.deleteWBuriedByIds(ids));
    }
}
