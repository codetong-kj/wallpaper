package com.ruoyi.wallpaper.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wallpaper.domain.WUserWorks;
import com.ruoyi.wallpaper.service.IWUserWorksService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户作品Controller
 * 
 * @author weiwei
 * @date 2022-03-28
 */
@RestController
@RequestMapping("/wallpaper/works")
public class WUserWorksController extends BaseController
{
    @Autowired
    private IWUserWorksService wUserWorksService;

    /**
     * 查询用户作品列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:works:list')")
    @GetMapping("/list")
    public TableDataInfo list(WUserWorks wUserWorks)
    {
        startPage();
        List<WUserWorks> list = wUserWorksService.selectWUserWorksList(wUserWorks);
        return getDataTable(list);
    }

    /**
     * 导出用户作品列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:works:export')")
    @Log(title = "用户作品", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WUserWorks wUserWorks)
    {
        List<WUserWorks> list = wUserWorksService.selectWUserWorksList(wUserWorks);
        ExcelUtil<WUserWorks> util = new ExcelUtil<WUserWorks>(WUserWorks.class);
        return util.exportExcel(list, "用户作品数据");
    }

    /**
     * 获取用户作品详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:works:query')")
    @GetMapping(value = "/{workId}")
    public AjaxResult getInfo(@PathVariable("workId") String workId)
    {

        return AjaxResult.success(wUserWorksService.selectWUserWorksByWorkId(workId));
    }

    /**
     * 新增用户作品
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:works:add')")
    @Log(title = "用户作品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WUserWorks wUserWorks)
    {
        return toAjax(wUserWorksService.insertWUserWorks(wUserWorks));
    }

    /**
     * 修改用户作品
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:works:edit')")
    @Log(title = "用户作品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WUserWorks wUserWorks)
    {
        return toAjax(wUserWorksService.updateWUserWorks(wUserWorks));
    }

    /**
     * 删除用户作品
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:works:remove')")
    @Log(title = "用户作品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{workIds}")
    public AjaxResult remove(@PathVariable String[] workIds)
    {
        return toAjax(wUserWorksService.deleteWUserWorksByWorkIds(workIds));
    }
}
