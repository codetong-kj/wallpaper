package com.ruoyi.wallpaper.controller;

import java.util.List;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wallpaper.domain.WUser;
import com.ruoyi.wallpaper.service.IWUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 用户信息Controller
 * 
 * @author weiwei
 * @date 2022-03-28
 */
@RestController
@RequestMapping("/wallpaper/user")
public class WUserController extends BaseController
{
    @Autowired
    private IWUserService wUserService;

    /**
     * 查询用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:user:list')")
    @GetMapping("/list")
    public TableDataInfo list(WUser wUser)
    {
        startPage();
        List<WUser> list = wUserService.selectWUserList(wUser);
        return getDataTable(list);
    }

    /**
     * 导出用户信息列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:user:export')")
    @Log(title = "用户信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WUser wUser)
    {
        List<WUser> list = wUserService.selectWUserList(wUser);
        ExcelUtil<WUser> util = new ExcelUtil<WUser>(WUser.class);
        return util.exportExcel(list, "用户信息数据");
    }

    /**
     * 获取用户信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:user:query')")
    @GetMapping(value = "/{userId}")
    public AjaxResult getInfo(@PathVariable("userId") String userId)
    {
        return AjaxResult.success(wUserService.selectWUserByUserId(userId));
    }

    /**
     * 新增用户信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:user:add')")
    @Log(title = "用户信息", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WUser wUser)
    {
        return toAjax(wUserService.insertWUser(wUser));
    }

    /**
     * 修改用户信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:user:edit')")
    @Log(title = "用户信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WUser wUser)
    {
        return toAjax(wUserService.updateWUser(wUser));
    }

    /**
     * 删除用户信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:user:remove')")
    @Log(title = "用户信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{userIds}")
    public AjaxResult remove(@PathVariable String[] userIds)
    {
        return toAjax(wUserService.deleteWUserByUserIds(userIds));
    }
}
