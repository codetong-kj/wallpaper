package com.ruoyi.wallpaper.mapper;

import java.util.List;
import com.ruoyi.wallpaper.domain.UserLike;
import org.apache.ibatis.annotations.Param;
import org.springframework.security.core.parameters.P;

/**
 * 用户点赞Mapper接口
 * 
 * @author weiwei
 * @date 2022-03-13
 */
public interface UserLikeMapper 
{
    /**
     * 查询用户点赞
     * 
     * @param id 用户点赞主键
     * @return 用户点赞
     */
    public UserLike selectUserLikeById(Integer id);

    /**
    * @Description 查询用户是否对此作品点过赞
    * @Author  weiwei
    * @Date   2022/3/13 11:15
    * @Param
    * @Return
    * @Exception
    *
    */
    public Integer isLikedByUserIdAndWorkId(@Param("workId")String workId, @Param("userId")String userId);
    /**
     * 查询用户点赞列表
     * 
     * @param userLike 用户点赞
     * @return 用户点赞集合
     */
    public List<UserLike> selectUserLikeList(UserLike userLike);

    /**
     * 新增用户点赞
     * 
     * @param userLike 用户点赞
     * @return 结果
     */
    public int insertUserLike(UserLike userLike);

    /**
     * 修改用户点赞
     * 
     * @param userLike 用户点赞
     * @return 结果
     */
    public int updateUserLike(UserLike userLike);

    /**
     * 删除用户点赞
     * 
     * @param id 用户点赞主键
     * @return 结果
     */
    public int deleteUserLikeById(Integer id);
    /**
     * 删除用户点赞
     *
     * @param workId 作品id；userId 用户id
     * @return 结果
     */
    public int deleteUserLikeByUserIdAndWorkId(@Param("workId")String workId, @Param("userId")String userId);

    /**
     * 批量删除用户点赞
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteUserLikeByIds(Integer[] ids);
}
