package com.ruoyi.wallpaper.mapper;

import java.util.List;
import com.ruoyi.wallpaper.domain.WUpdate;
import com.ruoyi.wallpaper.domain.WUpdateItem;

/**
 * 软件更新Mapper接口
 * 
 * @author weiwei
 * @date 2021-11-18
 */
public interface WUpdateMapper 
{
    /**
     * 查询软件更新
     * 
     * @param updateId 软件更新主键
     * @return 软件更新
     */
    public WUpdate selectWUpdateByUpdateId(Long updateId);

    /**
     *
     * @return 获取最后一个版本
     */
    public WUpdate getLastUpdate();

    /**
     * 查询软件更新列表
     * 
     * @param wUpdate 软件更新
     * @return 软件更新集合
     */
    public List<WUpdate> selectWUpdateList(WUpdate wUpdate);
    /**
     * 查询软件更新列表
     *
     * @return 软件更新集合
     */
    public List<WUpdate> selectWUpdateAllList();
    /**
     * 新增软件更新
     * 
     * @param wUpdate 软件更新
     * @return 结果
     */
    public int insertWUpdate(WUpdate wUpdate);

    /**
     * 修改软件更新
     * 
     * @param wUpdate 软件更新
     * @return 结果
     */
    public int updateWUpdate(WUpdate wUpdate);

    /**
     * 删除软件更新
     * 
     * @param updateId 软件更新主键
     * @return 结果
     */
    public int deleteWUpdateByUpdateId(Long updateId);

    /**
     * 批量删除软件更新
     * 
     * @param updateIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWUpdateByUpdateIds(Long[] updateIds);

    /**
     * 批量删除更新明细
     * 
     * @param updateIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWUpdateItemByUpdateIds(Long[] updateIds);
    
    /**
     * 批量新增更新明细
     * 
     * @param wUpdateItemList 更新明细列表
     * @return 结果
     */
    public int batchWUpdateItem(List<WUpdateItem> wUpdateItemList);
    

    /**
     * 通过软件更新主键删除更新明细信息
     * 
     * @param updateId 软件更新ID
     * @return 结果
     */
    public int deleteWUpdateItemByUpdateId(Long updateId);
}
