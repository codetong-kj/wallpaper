package com.ruoyi.wallpaper.mapper;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.wallpaper.domain.WBuriedDayLive;
import org.springframework.data.repository.query.Param;

/**
 * 埋点日活Mapper接口
 * 
 * @author weiwei
 * @date 2021-12-23
 */
public interface WBuriedDayLiveMapper 
{
    /**
     * 查询埋点日活
     * 
     * @param dId 埋点日活主键
     * @return 埋点日活
     */
    public WBuriedDayLive selectWBuriedDayLiveByDId(Long dId);

    /**
    * @Description 返回每日软件启动的次数
    * @Author  weiwei
    * @Date   2022/1/6 17:20
    * @Param
    * @Return      次数
    * @Exception
    *
    */
    public Integer selectStartCount();

    /**
    * @Description 近一个月的
    * @Author  weiwei
    * @Date   2022/1/6 22:19
    * @Param
    * @Return
    * @Exception
    *
    */
    public List<Map<String,Object>> selectStartMoonCount();

    /**
     * 查询埋点日活列表
     * 
     * @param wBuriedDayLive 埋点日活
     * @return 埋点日活集合
     */
    public List<WBuriedDayLive> selectWBuriedDayLiveList(WBuriedDayLive wBuriedDayLive);

    /**
     * 新增埋点日活
     * 
     * @param wBuriedDayLive 埋点日活
     * @return 结果
     */
    public int insertWBuriedDayLive(WBuriedDayLive wBuriedDayLive);

    /**
     * 修改埋点日活
     * 
     * @param wBuriedDayLive 埋点日活
     * @return 结果
     */
    public int updateWBuriedDayLive(WBuriedDayLive wBuriedDayLive);
    /**
    * @Description 退出软件
    * @Author  weiwei
    * @Date   2021/12/23 14:51
    * @Param  ip userId
    * @Return
    * @Exception
    *
    */
    public int updateBuriedEndTime(@Param("userId") String userId,@Param("ip") String ip);
    /**
     * 删除埋点日活
     * 
     * @param dId 埋点日活主键
     * @return 结果
     */
    public int deleteWBuriedDayLiveByDId(Long dId);

    /**
     * 批量删除埋点日活
     * 
     * @param dIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWBuriedDayLiveByDIds(Long[] dIds);
}
