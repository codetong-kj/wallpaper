package com.ruoyi.wallpaper.mapper;

import java.util.List;

import com.ruoyi.wallpaper.domain.WallpaperOrderDetail;

/**
 * 订单明细Mapper接口
 *
 * @author weiwei
 * @date 2021-12-17
 */
public interface WallpaperOrderDetailMapper {

    /**
     * @param wallpaperOrderDetail 订单明细
     * @return 修改条数
     */
    int updateWallpaperOrderDetailByOrderNumber(WallpaperOrderDetail wallpaperOrderDetail);

    /**
     * @param orderNumber 订单号
     * @return 订单明细
     */
    WallpaperOrderDetail selectWallpaperOrderDetailByOrderNum(String orderNumber);

    /**
     * 查询订单明细
     *
     * @param id 订单明细主键
     * @return 订单明细
     */
    public WallpaperOrderDetail selectWallpaperOrderDetailById(Long id);

    /**
     * 查询订单明细列表
     *
     * @param wallpaperOrderDetail 订单明细
     * @return 订单明细集合
     */
    public List<WallpaperOrderDetail> selectWallpaperOrderDetailList(WallpaperOrderDetail wallpaperOrderDetail);

    /**
     * 新增订单明细
     *
     * @param wallpaperOrderDetail 订单明细
     * @return 结果
     */
    public int insertWallpaperOrderDetail(WallpaperOrderDetail wallpaperOrderDetail);

    /**
     * 修改订单明细
     *
     * @param wallpaperOrderDetail 订单明细
     * @return 结果
     */
    public int updateWallpaperOrderDetail(WallpaperOrderDetail wallpaperOrderDetail);

    /**
     * 删除订单明细
     *
     * @param id 订单明细主键
     * @return 结果
     */
    public int deleteWallpaperOrderDetailById(Long id);

    /**
     * 批量删除订单明细
     *
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWallpaperOrderDetailByIds(Long[] ids);
}
