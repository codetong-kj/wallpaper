package com.ruoyi.wallpaper.mapper;

import java.util.List;
import com.ruoyi.wallpaper.domain.WBuriedLogin;

/**
 * 埋点用户登录Mapper接口
 * 
 * @author weiwei
 * @date 2021-12-23
 */
public interface WBuriedLoginMapper 
{
    /**
     * 查询埋点用户登录
     * 
     * @param lId 埋点用户登录主键
     * @return 埋点用户登录
     */
    public WBuriedLogin selectWBuriedLoginByLId(Long lId);

    /**
     * 查询埋点用户登录列表
     * 
     * @param wBuriedLogin 埋点用户登录
     * @return 埋点用户登录集合
     */
    public List<WBuriedLogin> selectWBuriedLoginList(WBuriedLogin wBuriedLogin);

    /**
     * 新增埋点用户登录
     * 
     * @param wBuriedLogin 埋点用户登录
     * @return 结果
     */
    public int insertWBuriedLogin(WBuriedLogin wBuriedLogin);

    /**
     * 修改埋点用户登录
     * 
     * @param wBuriedLogin 埋点用户登录
     * @return 结果
     */
    public int updateWBuriedLogin(WBuriedLogin wBuriedLogin);
    /**
     * 修改埋点用户退出
     *
     * @param userId 埋点用户id
     * @return 结果
     */
    public int updateExitTime(String userId);
    /**
     * 删除埋点用户登录
     * 
     * @param lId 埋点用户登录主键
     * @return 结果
     */
    public int deleteWBuriedLoginByLId(Long lId);

    /**
     * 批量删除埋点用户登录
     * 
     * @param lIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWBuriedLoginByLIds(Long[] lIds);
}
