package com.ruoyi.wallpaper.mapper;

import java.util.List;
import com.ruoyi.wallpaper.domain.WallpaperComment;

/**
 * 用户评论Mapper接口
 * 
 * @author weiwei
 * @date 2021-11-10
 */
public interface WallpaperCommentMapper 
{
    /**
     * 查询用户评论
     * 
     * @param commentId 用户评论主键
     * @return 用户评论
     */
    public WallpaperComment selectWallpaperCommentByCommentId(Long commentId);

    /**
     * 查询用户评论列表
     * 
     * @param wallpaperComment 用户评论
     * @return 用户评论集合
     */
    public List<WallpaperComment> selectWallpaperCommentList(WallpaperComment wallpaperComment);

    /**
     * 新增用户评论
     * 
     * @param wallpaperComment 用户评论
     * @return 结果
     */
    public int insertWallpaperComment(WallpaperComment wallpaperComment);

    /**
     * 修改用户评论
     * 
     * @param wallpaperComment 用户评论
     * @return 结果
     */
    public int updateWallpaperComment(WallpaperComment wallpaperComment);

    /**
     * 删除用户评论
     * 
     * @param commentId 用户评论主键
     * @return 结果
     */
    public int deleteWallpaperCommentByCommentId(Long commentId);

    /**
     * 批量删除用户评论
     * 
     * @param commentIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWallpaperCommentByCommentIds(Long[] commentIds);
}
