package com.ruoyi.wallpaper.mapper;

import java.util.List;
import com.ruoyi.wallpaper.domain.SocialUserAuth;

/**
 * 社会化用户 & 系统用户关系Mapper接口
 * 
 * @author weiwei
 * @date 2022-01-12
 */
public interface SocialUserAuthMapper 
{
    /**
     * 查询社会化用户 & 系统用户关系
     * 
     * @param id 社会化用户 & 系统用户关系主键
     * @return 社会化用户 & 系统用户关系
     */
    public SocialUserAuth selectSocialUserAuthById(Integer id);

    /**
     * 查询社会化用户 & 系统用户关系列表
     * 
     * @param socialUserAuth 社会化用户 & 系统用户关系
     * @return 社会化用户 & 系统用户关系集合
     */
    public List<SocialUserAuth> selectSocialUserAuthList(SocialUserAuth socialUserAuth);

    /**
     * 新增社会化用户 & 系统用户关系
     * 
     * @param socialUserAuth 社会化用户 & 系统用户关系
     * @return 结果
     */
    public int insertSocialUserAuth(SocialUserAuth socialUserAuth);

    /**
     * 修改社会化用户 & 系统用户关系
     * 
     * @param socialUserAuth 社会化用户 & 系统用户关系
     * @return 结果
     */
    public int updateSocialUserAuth(SocialUserAuth socialUserAuth);

    /**
     * 删除社会化用户 & 系统用户关系
     * 
     * @param id 社会化用户 & 系统用户关系主键
     * @return 结果
     */
    public int deleteSocialUserAuthById(Integer id);

    /**
     * 批量删除社会化用户 & 系统用户关系
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSocialUserAuthByIds(Integer[] ids);
}
