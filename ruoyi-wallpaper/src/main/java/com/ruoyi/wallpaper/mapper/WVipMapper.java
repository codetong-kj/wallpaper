package com.ruoyi.wallpaper.mapper;

import java.util.List;
import com.ruoyi.wallpaper.domain.WVip;

/**
 * 会员Mapper接口
 *
 * @author weiwei
 * @date 2021-12-17
 */
public interface WVipMapper
{

    /**
     *
     * @param userId 用户id
     * @return 用户vip信息
     */
    WVip selectWVipByUserId(String userId);
    /**
     * 查询会员
     *
     * @param vipId 会员主键
     * @return 会员
     */
    public WVip selectWVipByVipId(Long vipId);

    /**
     * 查询会员列表
     *
     * @param wVip 会员
     * @return 会员集合
     */
    public List<WVip> selectWVipList(WVip wVip);

    /**
     * 新增会员
     *
     * @param wVip 会员
     * @return 结果
     */
    public int insertWVip(WVip wVip);

    /**
     * 修改会员
     *
     * @param wVip 会员
     * @return 结果
     */
    public int updateWVip(WVip wVip);

    /**
     * 删除会员
     *
     * @param vipId 会员主键
     * @return 结果
     */
    public int deleteWVipByVipId(Long vipId);

    /**
     * 批量删除会员
     *
     * @param vipIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWVipByVipIds(Long[] vipIds);
}
