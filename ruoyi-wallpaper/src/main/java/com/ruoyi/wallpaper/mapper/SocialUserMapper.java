package com.ruoyi.wallpaper.mapper;

import java.util.List;
import com.ruoyi.wallpaper.domain.SocialUser;

/**
 * 社会化用户Mapper接口
 * 
 * @author weiwei
 * @date 2022-01-12
 */
public interface SocialUserMapper 
{
    /**
     * 查询社会化用户
     * 
     * @param id 社会化用户主键
     * @return 社会化用户
     */
    public SocialUser selectSocialUserById(Long id);

    /**
     * 查询社会化用户列表
     * 
     * @param socialUser 社会化用户
     * @return 社会化用户集合
     */
    public List<SocialUser> selectSocialUserList(SocialUser socialUser);

    /**
     * 新增社会化用户
     * 
     * @param socialUser 社会化用户
     * @return 结果
     */
    public int insertSocialUser(SocialUser socialUser);

    /**
     * 修改社会化用户
     * 
     * @param socialUser 社会化用户
     * @return 结果
     */
    public int updateSocialUser(SocialUser socialUser);

    /**
     * 删除社会化用户
     * 
     * @param id 社会化用户主键
     * @return 结果
     */
    public int deleteSocialUserById(Long id);

    /**
     * 批量删除社会化用户
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteSocialUserByIds(Long[] ids);
}
