package com.ruoyi.wallpaper.mapper;

import java.util.List;
import com.ruoyi.wallpaper.domain.WallpaperDynamic;

/**
 * 动态壁纸Mapper接口
 * 
 * @author weiwei
 * @date 2021-11-10
 */
public interface WallpaperDynamicMapper 
{
    /**
     * 查询动态壁纸
     * 
     * @param wId 动态壁纸主键
     * @return 动态壁纸
     */
    public WallpaperDynamic selectWallpaperDynamicByWId(String wId);

    /**
     * 查询动态壁纸列表
     * 
     * @param wallpaperDynamic 动态壁纸
     * @return 动态壁纸集合
     */
    public List<WallpaperDynamic> selectWallpaperDynamicList(WallpaperDynamic wallpaperDynamic);

    /**
     * 新增动态壁纸
     * 
     * @param wallpaperDynamic 动态壁纸
     * @return 结果
     */
    public int insertWallpaperDynamic(WallpaperDynamic wallpaperDynamic);

    /**
     * 修改动态壁纸
     * 
     * @param wallpaperDynamic 动态壁纸
     * @return 结果
     */
    public int updateWallpaperDynamic(WallpaperDynamic wallpaperDynamic);

    /**
     * 删除动态壁纸
     * 
     * @param wId 动态壁纸主键
     * @return 结果
     */
    public int deleteWallpaperDynamicByWId(String wId);

    /**
     * 批量删除动态壁纸
     * 
     * @param wIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWallpaperDynamicByWIds(String[] wIds);
}
