package com.ruoyi.wallpaper.mapper;

import java.util.List;
import com.ruoyi.wallpaper.domain.WUserStored;

/**
 * 用户壁纸库存中间Mapper接口
 * 
 * @author weiwei
 * @date 2021-12-17
 */
public interface WUserStoredMapper 
{
    /**
     * 查询用户壁纸库存中间
     * 
     * @param wPid 用户壁纸库存中间主键
     * @return 用户壁纸库存中间
     */
    public WUserStored selectWUserStoredByWPid(String wPid);

    /**
     * 查询用户壁纸库存中间列表
     * 
     * @param wUserStored 用户壁纸库存中间
     * @return 用户壁纸库存中间集合
     */
    public List<WUserStored> selectWUserStoredList(WUserStored wUserStored);

    /**
     * 新增用户壁纸库存中间
     * 
     * @param wUserStored 用户壁纸库存中间
     * @return 结果
     */
    public int insertWUserStored(WUserStored wUserStored);

    /**
     * 修改用户壁纸库存中间
     * 
     * @param wUserStored 用户壁纸库存中间
     * @return 结果
     */
    public int updateWUserStored(WUserStored wUserStored);

    /**
     * 删除用户壁纸库存中间
     * 
     * @param wPid 用户壁纸库存中间主键
     * @return 结果
     */
    public int deleteWUserStoredByWPid(String wPid);

    /**
     * 批量删除用户壁纸库存中间
     * 
     * @param wPids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWUserStoredByWPids(String[] wPids);
}
