package com.ruoyi.wallpaper.mapper;

import java.util.List;
import com.ruoyi.wallpaper.domain.WVideoInfo;
import org.apache.ibatis.annotations.Param;

/**
 * 视频详情Mapper接口
 * 
 * @author weiwei
 * @date 2022-02-28
 */
public interface WVideoInfoMapper 
{
    /**
     * 查询视频详情
     * 
     * @param vId 视频详情主键
     * @return 视频详情
     */
    public WVideoInfo selectWVideoInfoByVId(Integer vId);

    /**
     * 查询视频详情列表
     * 
     * @param wVideoInfo 视频详情
     * @return 视频详情集合
     */
    public List<WVideoInfo> selectWVideoInfoList(WVideoInfo wVideoInfo);

    /**
     * 新增视频详情
     * 
     * @param wVideoInfo 视频详情
     * @return 结果
     */
    public int insertWVideoInfo(WVideoInfo wVideoInfo);
    /**
     * 新增视频详情
     *
     * @param videoInfos 视频详情列表
     * @return 结果
     */
    public int insertVideoInfoList(@Param("videoInfos")List<WVideoInfo> videoInfos);

    /**
     * 修改视频详情
     * 
     * @param wVideoInfo 视频详情
     * @return 结果
     */
    public int updateWVideoInfo(WVideoInfo wVideoInfo);

    /**
     * 删除视频详情
     * 
     * @param vId 视频详情主键
     * @return 结果
     */
    public int deleteWVideoInfoByVId(Integer vId);

    /**
     * 批量删除视频详情
     * 
     * @param vIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWVideoInfoByVIds(Integer[] vIds);
}
