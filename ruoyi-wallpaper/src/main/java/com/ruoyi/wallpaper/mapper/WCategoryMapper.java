package com.ruoyi.wallpaper.mapper;

import java.util.List;

import com.ruoyi.wallpaper.domain.WCategory;

/**
 * 文件分类Mapper接口
 *
 * @author weiwei
 * @date 2022-03-29
 */
public interface WCategoryMapper {
    /**
     * 查询文件分类
     *
     * @param cId 文件分类主键
     * @return 文件分类
     */
    public WCategory selectWCategoryByCId(Integer cId);

    /**
     * 查询文件分类列表
     *
     * @param wCategory 文件分类
     * @return 文件分类集合
     */
    public List<WCategory> selectWCategoryList(WCategory wCategory);

    /**
     * 查询文件分类列表
     * @Param 所属的一级分类1视频0图片
     * @return 文件分类集合
     */
    public List<WCategory> selectNameList(Integer type);

    /**
     * 新增文件分类
     *
     * @param wCategory 文件分类
     * @return 结果
     */
    public int insertWCategory(WCategory wCategory);

    /**
     * 修改文件分类
     *
     * @param wCategory 文件分类
     * @return 结果
     */
    public int updateWCategory(WCategory wCategory);

    /**
     * 删除文件分类
     *
     * @param cId 文件分类主键
     * @return 结果
     */
    public int deleteWCategoryByCId(Integer cId);

    /**
     * 批量删除文件分类
     *
     * @param cIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWCategoryByCIds(Integer[] cIds);
}
