package com.ruoyi.wallpaper.mapper;

import java.util.List;
import com.ruoyi.wallpaper.domain.WBuriedPay;

/**
 * 埋点支付Mapper接口
 * 
 * @author weiwei
 * @date 2021-12-23
 */
public interface WBuriedPayMapper 
{
    /**
     * 查询埋点支付
     * 
     * @param vId 埋点支付主键
     * @return 埋点支付
     */
    public WBuriedPay selectWBuriedPayByVId(Long vId);

    /**
     * 查询埋点支付列表
     * 
     * @param wBuriedPay 埋点支付
     * @return 埋点支付集合
     */
    public List<WBuriedPay> selectWBuriedPayList(WBuriedPay wBuriedPay);

    /**
     * 新增埋点支付
     * 
     * @param wBuriedPay 埋点支付
     * @return 结果
     */
    public int insertWBuriedPay(WBuriedPay wBuriedPay);

    /**
     * 修改埋点支付
     * 
     * @param wBuriedPay 埋点支付
     * @return 结果
     */
    public int updateWBuriedPay(WBuriedPay wBuriedPay);

    /**
     * 删除埋点支付
     * 
     * @param vId 埋点支付主键
     * @return 结果
     */
    public int deleteWBuriedPayByVId(Long vId);

    /**
     * 批量删除埋点支付
     * 
     * @param vIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteWBuriedPayByVIds(Long[] vIds);
}
