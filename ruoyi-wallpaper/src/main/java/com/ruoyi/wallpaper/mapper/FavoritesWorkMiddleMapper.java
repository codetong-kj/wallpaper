package com.ruoyi.wallpaper.mapper;

import java.util.List;
import java.util.Map;

import com.ruoyi.wallpaper.domain.FavoritesWorkMiddle;

/**
 * 收藏夹与作品之间的关联Mapper接口
 * 
 * @author weiwei
 * @date 2022-03-16
 */
public interface FavoritesWorkMiddleMapper 
{
    /**
     * 查询收藏夹与作品之间的关联
     * 
     * @param fId 收藏夹与作品之间的关联主键
     * @return 收藏夹与作品之间的关联
     */
    public FavoritesWorkMiddle selectFavoritesWorkMiddleByFId(Integer fId);

    /**
     * 查询收藏夹与作品之间的关联列表
     * 
     * @param favoritesWorkMiddle 收藏夹与作品之间的关联
     * @return 收藏夹与作品之间的关联集合
     */
    public List<FavoritesWorkMiddle> selectFavoritesWorkMiddleList(FavoritesWorkMiddle favoritesWorkMiddle);
    /**
     * 查询收藏夹与作品之间的关联列表
     *
     * @param userId 用户id
     * @return 收藏夹与作品之间的关联集合
     */
    public List<FavoritesWorkMiddle> selectFavoritesWorkMiddleListByUserId(String userId);

    /**
     * 新增收藏夹与作品之间的关联
     * 
     * @param favoritesWorkMiddle 收藏夹与作品之间的关联
     * @return 结果
     */
    public int insertFavoritesWorkMiddle(FavoritesWorkMiddle favoritesWorkMiddle);

    /**
     * 修改收藏夹与作品之间的关联
     * 
     * @param favoritesWorkMiddle 收藏夹与作品之间的关联
     * @return 结果
     */
    public int updateFavoritesWorkMiddle(FavoritesWorkMiddle favoritesWorkMiddle);

    /**
     * 删除收藏夹与作品之间的关联
     * 
     * @param mId 收藏夹与作品之间的关联主键
     * @return 结果
     */
    public int deleteFavoritesWorkMiddleByMId(Integer mId);
    /**
    * @Description 根据fId和workId删除
    * @Author  weiwei
    * @Date   2022/3/20 21:50
    * @Param
    * @Return
    * @Exception
    *
    */
    public int deleteFavoritesWorkMiddleByMFidWorkId(FavoritesWorkMiddle favoritesWorkMiddle);

    /**
    * @Description 查询用户收藏夹里的内容
    * @Author  weiwei
    * @Date   2022/3/21 15:18
    * @Param
    * @Return
    * @Exception
    *
    */
    public List<Map<String,Object>> selectAllFavoritesList(String userId);

    /**
     * 批量删除收藏夹与作品之间的关联
     * 
     * @param mIds 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteFavoritesWorkMiddleByMIds(Integer[] mIds);
}
