package com.ruoyi.task;

import com.ruoyi.wallpaper.service.IUserLikeService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.QuartzJobBean;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 点赞的定时任务
 */

@Component("likeTask")
@Slf4j
public class LikeTask{
    @Autowired
    IUserLikeService likedService;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    public void runJob(){
        log.info("LikeTask--run------ {}", sdf.format(new Date()));
        //将 Redis 里的点赞信息同步到数据库里
        likedService.transLikeFromRedis();
        likedService.transLikeCountFromRedis();
    }
}