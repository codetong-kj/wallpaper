package com.ruoyi.task;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.utils.http.HttpUtils;
import com.ruoyi.wallpaper.domain.CheckIn;
import com.ruoyi.wallpaper.service.ICheckInService;
import lombok.extern.slf4j.Slf4j;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author ke
 * @Date 2022/4/16 17:47
 * @Version 1.0
 */
@Slf4j
@Component("checkTask")
public class ClassBoxTask {
    String getIdUrl = "http://k8n.cn/student/course/35340/punchs";
    String cookie = "remember_student_59ba36addc2b2f9401580f014c7f58ea4e30989d=1697401%7CCs277liPnsl6I0l2fgf29Nu9FvPHvNZ0jjWkUqEUnSYH4BAew8n7AJdXG4Oy%7C%242y%2410%24jSTgW.5o6YunX6T9BinHienTVl%2F4vZ61oIDqoZsPaOmovPD5%2FwW5.; yxktmf=6dgMGerBXM2h8qJ2arPEFMjzOU6eGBvsmTznsPhP";
    @Resource
    ICheckInService checkInService;

    public void run() {
       /* String s = HttpUtils.sendGetCarryCookie(getIdUrl, "", Constants.UTF8, cookie);
        System.out.println(s);
        Document parse = Jsoup.parse(s);
        String attr = parse.getElementsByTag("form").attr("id");
        if (!"".equals(attr)) {
            String id = attr.substring(attr.length() - 6);
            String params = "id="+id+"&lat=34.61824417114258&lng=113.71863555908203&acc=35&res=&gps_addr=";
            System.out.println(HttpUtils.sendPostCarryCookie(getIdUrl, params, cookie));
        }*/
        CheckIn checkIn = new CheckIn();
        checkIn.setStatus(0);
        List<CheckIn> checkIns = checkInService.selectCheckInList(checkIn);
        for (CheckIn item : checkIns
        ) {
            String s = HttpUtils.sendGetCarryCookie(getIdUrl, "", Constants.UTF8, item.getCookie());
            Document parse = Jsoup.parse(s);
            String attr = parse.getElementsByTag("form").attr("id");
            if (!"".equals(attr)) {
                String id = attr.substring(attr.length() - 6);
                String params = "id=" + id + "&lat="+item.getLat()+"&lng="+item.getLng()+"&acc="+item.getAcc()+"&res=&gps_addr=";
                log.info(HttpUtils.sendPostCarryCookie(getIdUrl, params, item.getCookie()));
            }
        }

    }
}
