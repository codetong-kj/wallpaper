package com.ruoyi.web.core.config;

import com.ruoyi.interceptors.JwtInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author: weiwei
 * @Date: 2022/1/27 18:17
 * @Description: 配置拦截器
 */
@Configuration
public class InterceptorConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new JwtInterceptor())
                //拦截
                //拦截用户下的所有接口
                .addPathPatterns("/dev-api/private/**")
//                .addPathPatterns("/dev-api/wexinpay/**")
                //放行
                .excludePathPatterns("/dev-api/buried/**")
                .excludePathPatterns("/dev-api/oauth/**")
                .excludePathPatterns("/dev-api/public/**")
                .excludePathPatterns("/oauth/**");
    }
}
