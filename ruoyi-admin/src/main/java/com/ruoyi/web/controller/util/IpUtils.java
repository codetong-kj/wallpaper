package com.ruoyi.web.controller.util;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.http.HttpUtils;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;

public class IpUtils {
    private static final String[] ADDR_HEADER = {"X-Forwarded-For", "Proxy-Client-IP", "WL-Proxy-Client-IP",
            "X-Real-IP"};
    private static final String NUKNOWN = "unknown";


    /**
     * 获得真实IP地址。
     * 在使用了反向代理时，直接用HttpServletRequest.getRemoteAddr()无法获取客户真实的IP地址。
     *
     * @param request
     * @return
     */
    public static String getRemoteAddr(ServletRequest request) {
        String addr = null;
        if (request instanceof HttpServletRequest) {
            HttpServletRequest hsr = (HttpServletRequest) request;
            for (String header : ADDR_HEADER) {
                if (StringUtils.isBlank(addr) || NUKNOWN.equalsIgnoreCase(addr)) {
                    addr = hsr.getHeader(header);
                } else {
                    break;
                }
            }
        }
        if (StringUtils.isBlank(addr) || NUKNOWN.equalsIgnoreCase(addr)) {
            addr = request.getRemoteAddr();
        } else {
            // 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按','分割
            if (addr != null) {
                int i = addr.indexOf(",");
                if (i > 0) {
                    addr = addr.substring(0, i);
                }
            }

        }
        return addr;
    }

    public static String getIpLocation(String ip) {
        if (StringUtils.isNotEmpty(ip)) {
            String url = "http://ip-api.com/json/" + ip;
            String param = "lang=zh-CN";
            String res = HttpUtils.sendGet(url, param);
            JSONObject jsonObject = JSONObject.parseObject(res);
            if ("success".equals(jsonObject.getString("status"))) {
                return res;
            }
        }
        return null;
    }
}
