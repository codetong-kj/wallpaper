package com.ruoyi.web.controller.mmsprivate;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.wallpaper.domain.Favorites;
import com.ruoyi.wallpaper.domain.FavoritesWorkMiddle;
import com.ruoyi.wallpaper.service.IFavoritesService;
import com.ruoyi.wallpaper.service.IFavoritesWorkMiddleService;
import com.ruoyi.wallpaper.util.JwtUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api
@RestController
@RequestMapping("/dev-api/private/favorites")
public class FavoritesApi extends BaseController {
    @Autowired
    public IFavoritesService favoritesService;

    @Autowired
    public IFavoritesWorkMiddleService favoritesWorkMiddleService;

    /**
     * @Description 查询收藏夹
     * @Author weiwei
     * @Date 2022/3/15 14:53
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("查询用户收藏夹列表")
    @GetMapping("/getFavoritesList")
    public AjaxResult getFavoritesList() {
        String userId = JwtUtils.getUserId();
        Favorites favorites = new Favorites();
        favorites.setUserId(userId);
        return AjaxResult.success(favoritesService.selectFavoritesList(favorites));
    }


    /**
     * @Description 查询收藏夹
     * @Author weiwei
     * @Date 2022/3/15 14:53
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("查询用户收藏夹列表")
    @GetMapping("/listFavorites")
    public TableDataInfo listFavorites(Favorites favorites) {
        String userId = JwtUtils.getUserId();
        favorites.setUserId(userId);
        List<Favorites> list = favoritesService.selectFavoritesList(favorites);
        return getDataTable(list);
    }

    /**
     * @Description 查询收藏夹
     * @Author weiwei
     * @Date 2022/3/15 14:53
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("查询用户收藏夹列表")
    @GetMapping("/getFavoritesByFID/{fId}")
    public AjaxResult getFavoritesByFiD(@PathVariable("fId") Integer fId) {
        return AjaxResult.success(favoritesService.selectFavoritesByFId(fId));
    }

    /**
     * @Description 新增收藏夹
     * @Author weiwei
     * @Date 2022/3/15 14:53
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("用户新增收藏夹")
    @PostMapping("/addFavorites")
    public AjaxResult addFavorites(@RequestBody Favorites favorites) {
        String userId = JwtUtils.getUserId();
        favorites.setUserId(userId);
        return AjaxResult.success(favoritesService.insertFavorites(favorites));
    }

    /**
     * @Description 修改收藏夹
     * @Author weiwei
     * @Date 2022/3/15 14:53
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("修改收藏夹")
    @PutMapping("/updateFavorites")
    public AjaxResult updateFavorites(@RequestBody Favorites favorites) {
        return toAjax(favoritesService.updateFavorites(favorites));
    }

    /**
     * @Description 删除收藏夹
     * @Author weiwei
     * @Date 2022/3/15 14:53
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("删除收藏夹")
    @DeleteMapping("/deleteFavorites/{fId}")
    public AjaxResult deleteFavorites(@PathVariable("fId") Integer fId) {
        return toAjax(favoritesService.deleteFavoritesByFId(fId));
    }

    /**
     * @Description 排序收藏夹
     * @Author weiwei
     * @Date 2022/3/19 15:28
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("排序收藏夹")
    @PostMapping("/sortFavorites")
    @Transactional(rollbackFor = Exception.class)
    public AjaxResult sortFavorites(@RequestBody Integer[] sort) {
        //[5, 4, 6, 1, 2, 3] id
        String userId = JwtUtils.getUserId();
        Favorites favorites = new Favorites();
        favorites.setUserId(userId);

        for (int i = 0; i < sort.length; i++) {
            Favorites favorites1 = new Favorites();
            favorites1.setfId(sort[i]);
            favorites1.setSort(i);
            favoritesService.updateFavorites(favorites1);
        }
        return success();
    }

    /**
     * @Description 添加收藏
     * @Author weiwei
     * @Date 2022/3/19 15:28
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("排序收藏夹")
    @Transactional
    @PostMapping("/addFavoritesMiddle")
    public AjaxResult addFavoritesMiddle(@RequestBody FavoritesWorkMiddle[] favoritesWorkMiddles) {
        String userId = JwtUtils.getUserId();
        int sum = 0;
        for (FavoritesWorkMiddle item : favoritesWorkMiddles) {
            Favorites favorites = favoritesService.selectFavoritesByFId(item.getfId());
            if (favorites.getUserId().equals(userId)) {
                sum += favoritesWorkMiddleService.insertFavoritesWorkMiddle(item);
            }
        }
        return toAjax(sum);
    }

    /**
     * @Description 查询已经收藏的收藏夹项
     * @Author weiwei
     * @Date 2022/3/19 15:28
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("查询已经收藏的收藏夹项")
    @GetMapping("/selectFavoritesMiddles")
    public AjaxResult selectFavoritesMiddles() {
        String userId = JwtUtils.getUserId();
        return AjaxResult.success(favoritesWorkMiddleService.selectFavoritesWorkMiddleListByUserId(userId));
    }

    /**
     * @Description 获取作品下用户收藏列表
     * @Author weiwei
     * @Date 2022/3/20 14:22
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("获取作品下用户收藏列表")
    @GetMapping("/selectCheckList/{workId}/{type}")
    public AjaxResult getFavoritesData(@PathVariable("workId") String workId,@PathVariable("type")Integer type) {
        String userId = JwtUtils.getUserId();
        return AjaxResult.success(favoritesService.selectCheckList(userId, workId,type));
    }

    @ApiOperation("收藏夹中间表处理")
    @PostMapping("/dealFavorites")
    @Transactional
    public AjaxResult dealFavorites(@RequestBody String deal) {
        int i = 0;
        JSONObject jsonObject = JSONObject.parseObject(deal);
        String addMediaIds = jsonObject.getString("add_media_ids");
        String delMediaIds = jsonObject.getString("del_media_ids");
        String workId = jsonObject.getString("workId");
        JSONArray addMediaIdsArray = JSONObject.parseArray(addMediaIds);
        JSONArray delMediaIdsArray = JSONObject.parseArray(delMediaIds);
//        Integer[] delId= new Integer[delMediaIdsArray.size()];
        System.out.println(addMediaIdsArray);
        System.out.println(delMediaIdsArray);
       /* if (!delMediaIdsArray.isEmpty()) {
            Integer[] integers = delMediaIdsArray.toArray(new Integer[0]);
            i += favoritesWorkMiddleService.deleteFavoritesWorkMiddleByMIds(integers);
        }*/

        for (Object id : delMediaIdsArray
        ) {
            FavoritesWorkMiddle favoritesWorkMiddle = new FavoritesWorkMiddle();
            favoritesWorkMiddle.setWorkId(workId);
            favoritesWorkMiddle.setfId((Integer) id);
            i+=favoritesWorkMiddleService.deleteFavoritesWorkMiddleByMFidWorkId(favoritesWorkMiddle);
        }
        for (Object id : addMediaIdsArray
        ) {
            FavoritesWorkMiddle favoritesWorkMiddle = new FavoritesWorkMiddle();
            favoritesWorkMiddle.setWorkId(workId);
            favoritesWorkMiddle.setfId((Integer) id);
            i+=favoritesWorkMiddleService.insertFavoritesWorkMiddle(favoritesWorkMiddle);
        }
        return toAjax(i);
    }

    /**
     * @Description 获取用户下所有收藏列表以及数据
     * @Author weiwei
     * @Date 2022/3/20 14:22
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("获取用户下所有收藏列表以及数据")
    @GetMapping("/selectAllFavoritesList")
    public AjaxResult selectAllFavoritesList() {
        String userId = JwtUtils.getUserId();
        return AjaxResult.success(favoritesWorkMiddleService.selectAllFavoritesList(userId));
    }

}
