package com.ruoyi.web.controller.clazz;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.http.HttpUtils;
import io.swagger.models.auth.In;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clazzManage")
public class ClazzManage extends BaseController {
    /**
     * 获取分类列表
     *
     * @return 返沪类别列表
     */
    @GetMapping("/list")
    public AjaxResult list() {
        String url = "http://bzl.xook.top:1998/wp/v2/category";
        String s = HttpUtils.sendGet(url, "");
        JSONObject result = JSONObject.parseObject(s);
        return AjaxResult.success(result);
    }

    /**
     * 修改壁纸分类接口
     *
     * @param data json数据
     * @return 返回结果
     */
    @PutMapping("/put")
    public AjaxResult put(@RequestBody String data) {
        String url = "http://bzl.xook.top:1998/wp/v2/regcategory";
        JSONObject jsonObject = JSONObject.parseObject(data);
        jsonObject.remove("_id");
        String string = jsonObject.toString();
        String post = HttpUtils.post(url, string);

        return success();
    }


}
