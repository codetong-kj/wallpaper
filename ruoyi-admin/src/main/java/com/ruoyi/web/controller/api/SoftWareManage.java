package com.ruoyi.web.controller.api;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.common.utils.sign.Md5Utils;
import com.ruoyi.framework.config.ServerConfig;
import com.ruoyi.wallpaper.domain.WUpdate;
import com.ruoyi.wallpaper.domain.WUpdateItem;
import com.ruoyi.wallpaper.service.IWUpdateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Api("软件管理")
@RestController
@RequestMapping("/dev-api/soft-ware-manage")
public class SoftWareManage extends BaseController {
    @Autowired
    private IWUpdateService wUpdateService;
    @Autowired
    private ServerConfig serverConfig;
    /**
     * @Description
     * @Author  weiwei
     * @Date   2022/1/10 13:20
     * @Param
     * @Return
     * @Exception
     *
     */
    @ApiOperation("文件上传接口")
    @PostMapping("/upload")
    public AjaxResult upload(MultipartFile file) throws Exception{
        try {
            System.out.println("进入上传接口");
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);
            //对fileName 进行加密
            String hash = Md5Utils.hash(fileName);
            String url = serverConfig.getUrl() + fileName;
            System.out.println("url------------->"+url);
            WUpdateItem updateItem = new WUpdateItem();
            updateItem.setDownloadUrl(url);
            updateItem.setMd(hash);
            updateItem.setSymbel(file.getOriginalFilename());
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            ajax.put("updateItem", updateItem);
            return ajax;
        }catch (Exception e){
            System.out.println("upload-------------->"+e.getMessage());
            return AjaxResult.error(e.getMessage());
        }

    }
    /**
     * 查询软件更新列表
     */
    @ApiOperation("获取所有更新列表")
    @GetMapping("/getAllList")
    public TableDataInfo getAllList() {
        startPage();
        List<WUpdate> list = wUpdateService.selectWUpdateAllList();
        return getDataTable(list);
    }


    @ApiOperation("根据版本查询是否需要更新")
    @GetMapping("/is-update/{version}")
    public AjaxResult isUpdate(@PathVariable Integer version) {
        WUpdate lastUpdate = wUpdateService.getLastUpdate();
        if (lastUpdate.getVersion() > version) {
            return AjaxResult.success(wUpdateService.selectWUpdateByUpdateId(lastUpdate.getUpdateId()));
        } else {
            return success("不需要更新");
        }

    }


}
