package com.ruoyi.web.controller.mmsprivate;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.wallpaper.service.IUserLikeService;
import com.ruoyi.wallpaper.service.LikeService;
import com.ruoyi.wallpaper.util.JwtUtils;
import com.ruoyi.wallpaper.util.LikeUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author: weiwei
 * @Date: 2022/3/12 19:15
 * @Description:点赞接口
 */
@Api
@RestController
@RequestMapping("/dev-api/private/workLike")
public class PutLike extends BaseController {
    @Autowired
    LikeService likeService;

    @Autowired
    IUserLikeService userLikeService;


    /**
     * @Description 点赞
     * @Author weiwei
     * @Date 2022/3/12 22:31
     * @Param
     * @Return
     * @Exception
     */
    @GetMapping("/like/{workId}")
    private AjaxResult putLike(@PathVariable("workId") String workId) {
        String userId = JwtUtils.getUserId();
        if (StringUtils.isNotEmpty(workId)) {
            LikeUtil likeUtil = new LikeUtil(likeService, userLikeService);
            if (likeUtil.isLiked(workId, userId)) {
                return success("不能重复点赞。");
            } else {
                //此用户对此作品没有点过赞(数据库与redis中都没有查询到)
                likeService.saveLiked2Redis(workId, userId);
                likeService.incrementLikedCount(workId);
                return success("感谢支持~");
            }
        }
        return success("点赞失败。");
    }

    @GetMapping("/unlike/{workId}")
    private AjaxResult putUnLike(@PathVariable("workId") String workId) {
        String userId = JwtUtils.getUserId();
        if (StringUtils.isNotEmpty(workId)) {
            LikeUtil likeUtil = new LikeUtil(likeService, userLikeService);
            if (likeUtil.isLiked(workId, userId)) {
                //两者只要有一条存在则进行减赞
                likeService.unlikeFromRedis(workId, userId);
                likeService.decrementLikedCount(workId);
                return success("取消点赞成功~");
            }
        }
        return success("取消点赞失败。");
    }

    @GetMapping("/commonLike/{workId}")
    private AjaxResult commonLike(@PathVariable("workId") String workId) {
        String userId = JwtUtils.getUserId();
        if (StringUtils.isNotEmpty(workId)) {
            LikeUtil likeUtil = new LikeUtil(likeService, userLikeService);
            if (likeUtil.isLiked(workId, userId)) {
                //两者只要有一条存在则进行减赞
                likeService.unlikeFromRedis(workId, userId);
                likeService.decrementLikedCount(workId);
                return success("取消点赞成功~");
            } else {
                //此用户对此作品没有点过赞(数据库与redis中都没有查询到)
                likeService.saveLiked2Redis(workId, userId);
                likeService.incrementLikedCount(workId);
                return success("感谢支持~");
            }
        }
        return success("取消点赞失败。");
    }

}
