package com.ruoyi.web.controller.api;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wallpaper.util.JwtUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.wallpaper.domain.SocialUser;
import com.ruoyi.wallpaper.domain.SocialUserAuth;
import com.ruoyi.wallpaper.domain.WUser;
import com.ruoyi.wallpaper.service.ISocialUserAuthService;
import com.ruoyi.wallpaper.service.ISocialUserService;
import com.ruoyi.wallpaper.service.IWUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhyd.oauth.config.AuthConfig;
import me.zhyd.oauth.exception.AuthException;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.model.AuthResponse;
import me.zhyd.oauth.model.AuthToken;
import me.zhyd.oauth.model.AuthUser;
import me.zhyd.oauth.request.AuthRequest;
import me.zhyd.oauth.request.AuthWeChatOpenRequest;
import me.zhyd.oauth.utils.AuthStateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;

@Api("第三方登录接口")
@Controller
@RequestMapping("/dev-api/oauth")
public class SocialUserApi extends BaseController {

    @Autowired
    IWUserService userService;
    @Autowired
    private ISocialUserAuthService socialUserAuthService;
    @Autowired
    private ISocialUserService socialUserService;

    @GetMapping("/hello")
    public String hello(){

        return "test";
    }

    @ApiOperation("扫码登录")
    @GetMapping("/render")
    @ResponseBody
    public void renderAuth(HttpServletResponse response) throws IOException {
        String source = "wechat_open";
        System.out.println("进入render：" + source);
        AuthRequest authRequest = getAuthRequest(source);
        String authorizeUrl = authRequest.authorize(AuthStateUtils.createState());
        System.out.println(authorizeUrl);
        response.sendRedirect(authorizeUrl);
    }

    private AuthRequest getAuthRequest(String source) {
        AuthRequest authRequest = null;
        if ("wechat_open".equalsIgnoreCase(source)) {
            authRequest = new AuthWeChatOpenRequest(AuthConfig.builder()
                    .clientId("wxbc63522dab0ad86b")
                    .clientSecret("9beb93d4f6b6cb47986fabf2ae30bb6c")
                    .redirectUri("https://www.bizhilou.com/dev-api/oauth/callback/wechat_open")
                    .build());
            System.out.println("微信");
        }
        if (null == authRequest) {
            throw new AuthException("未获得到有效的Auth配置");
        }
        return authRequest;

    }

    @ApiOperation("回调接口")
    @GetMapping("/callback/{source}")
    public String login(@PathVariable("source") String source, AuthCallback callback, HttpServletRequest request) throws JsonProcessingException {
        System.out.println("进入callback：" + source + " callback params：" + JSONObject.toJSONString(callback));
        AuthRequest authRequest = getAuthRequest(source);
        AuthResponse<AuthUser> response = authRequest.login(callback);
        System.out.println(JSONObject.toJSONString(response));
        HashMap<String, String> map = new HashMap<>();
        AjaxResult ajaxResult = new AjaxResult();
        if (response.ok()) {
            //第一步先查询 系统中是否存在此用户，若存在，则登录成功
            WUser user = userService.selectUserBySUid(response.getData().getUuid());
            if (user != null) {
                //查询到用户
                //最后一次登录时间
                user.setLoginDate(DateUtils.getNowDate());
                userService.updateWUser(user);
            } else {
                AuthUser authUser = response.getData();
                //未查询到用户，对用户进行自动化注册操作
                user = new WUser();
                String id = UUID.getId();
                if (null == user.getUserId()) {
                    user.setUserId(id);
                }
                if (null == user.getNickName()) {
                    user.setNickName("bzl_" + UUID.getId());
                }
                user.setAvatar(authUser.getAvatar());
                user.setWechat(authUser.getNickname());
                userService.insertWUser(user);
                //插入第三方用户表、
                SocialUser socialUser = new SocialUser();
                socialUser.setUuid(authUser.getUuid());
                socialUser.setSource(authUser.getSource());
                AuthToken token = authUser.getToken();
                socialUser.setAccessToken(token.getAccessToken());
                int expireIn = token.getExpireIn();
                socialUser.setExpireIn(expireIn);
                socialUser.setRefreshToken(token.getRefreshToken());
                socialUser.setScope(token.getScope());
                try {
                    socialUserService.insertSocialUser(socialUser);
                    //中间表里插数据
                    SocialUserAuth socialUserAuth = new SocialUserAuth();
                    socialUserAuth.setSocialUserId(authUser.getUuid());
                    socialUserAuth.setUserId(id);
                    socialUserAuthService.insertSocialUserAuth(socialUserAuth);
                    System.out.println("注册成功---------");
                } catch (Exception e) {
                    System.out.println("error----------------->" + e.getMessage());
                    AjaxResult error = AjaxResult.success(e.getMessage());
                    String res = JSONObject.toJSONString(error);
                }

            }
            map.put("id",user.getUserId());
            map.put("username",user.getNickName());
            String jwtToken = JwtUtils.getJwtToken(map);
            ajaxResult.put("token",jwtToken);
            request.setAttribute("info", new ObjectMapper().writeValueAsString(ajaxResult));
        }
        return "login";
    }

}
