package com.ruoyi.web.controller.test;

import com.ruoyi.common.utils.RedisKeyUtils;
import com.ruoyi.wallpaper.service.IIpLocationService;
import com.ruoyi.wallpaper.service.LikeService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.UnsupportedEncodingException;

public class Test1 {
    @Autowired
    private IIpLocationService ipLocationService;

    @Autowired
    LikeService likeService;
    @Autowired
    RedisTemplate redisTemplate;

    @Test
    public void test() {
     /*   JSONObject ipLocation = IpUtils.getIpLocation("120.219.84.59");
        System.out.println("tt"+ipLocation.get("regionName"));*/
       /* List<IpResult> ipResults = ipLocationService.selectLocation();
        System.out.println(ipResults);*/
//        likeService.saveLiked2Redis("888888","1111111");

        String key = RedisKeyUtils.getLikedKey("52e7a869cbf7", "1bbf61394c9d");
        Object o = redisTemplate.opsForHash().get(RedisKeyUtils.MAP_KEY_WORK_LIKED, key);
    }

    /* @Test
     public void testGenerateToken() {
         // 指定token过期时间为10秒
         Calendar calendar = Calendar.getInstance();
         calendar.add(Calendar.SECOND, 1000);

         String token = JWT.create()
                 .withHeader(new HashMap<>())  // Header
                 .withClaim("userId", 21)  // Payload
                 .withClaim("userName", "baobao")
                 .withExpiresAt(calendar.getTime())  // 过期时间
                 .sign(Algorithm.HMAC256("!34ADAS"));  // 签名用的secret

         System.out.println(token);
     }
     @Test
     public void testResolveToken(){
         // 创建解析对象，使用的算法和secret要与创建token时保持一致
         JWTVerifier jwtVerifier = JWT.require(Algorithm.HMAC256("!34ADAS")).build();
         // 解析指定的token
         DecodedJWT decodedJWT = jwtVerifier.verify("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VyTmFtZSI6ImJhb2JhbyIsImV4cCI6MTY0MzI1OTk0MywidXNlcklkIjoyMX0.lHkBmbM5qRRJePIdKzUX3nwZFfP_yrgYjkmTvJKTvbw");
         // 获取解析后的token中的payload信息
         Claim userId = decodedJWT.getClaim("userId");
         Claim userName = decodedJWT.getClaim("userName");
         System.out.println(userId.asInt());
         System.out.println(userName.asString());
         // 输出超时时间
         System.out.println(decodedJWT.getExpiresAt());
     }*/
    @Test
    public void test2() throws UnsupportedEncodingException {
        /*HashMap<String, String> stringStringHashMap = new HashMap<>();
        stringStringHashMap.put("id","12");
        stringStringHashMap.put("name","keke");
        String hahah = JwtUtils.getJwtToken(stringStringHashMap);
        System.out.println(hahah);*/

     /*   Map<String, String> tokenInfo = JwtUtils.getTokenInfo("eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6IjM4Y2JhNmMzM2MxMSIsImV4cCI6MTY0MzUyNjY5OSwidXNlcm5hbWUiOiJiemxfMzhjYmE2YzMzYzExIn0.p9z3g7j4RnXdcosQeTT6BK2_ggKlnB-AB6vYdMR4s3M");
        tokenInfo.forEach((k,v)->{
            System.out.println(k+"----"+v);
        });*/
    /*    Date date = DateUtils.addDays(DateUtils.getNowDate(), 7);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        System.out.println(UUID.getId());*/
        long n = getN(6);
        System.out.println(n);
    }

    public long getN(int n) {
      /*  if (n == 2 || n == 1) {
            return 1;
        } else {
            if (n % 2 == 0) {
                return (getN(n - 1) + getN(n - 2)) * 2;
            } else {
                return getN(n - 1) + getN(n - 2);
            }
        }*/
        int temp1 = 1, temp2 = 1;
        for (int i = 3; i < n +1; i++) {
            if (i % 2 == 0) {
                //n为偶数
                temp2 = (temp1 + temp2) * 2;
            } else {
                //此时n为奇数
                temp1 = temp1 + temp2;
            }
        }
        if (n % 2 == 0) {
            return temp2;
        } else {
            return temp1;
        }
    }

}
