package com.ruoyi.web.controller.wallpaper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.ruoyi.wallpaper.domain.IpResult;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.wallpaper.domain.IpLocation;
import com.ruoyi.wallpaper.service.IIpLocationService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * ip地址地域Controller
 * 
 * @author weiwei
 * @date 2022-01-08
 */
@RestController
@RequestMapping("/wallpaper/location")
public class IpLocationController extends BaseController
{
    @Autowired
    private IIpLocationService ipLocationService;


    @GetMapping("/getLocation")
    public AjaxResult getLocation() {
        List<IpResult> ipResults = ipLocationService.selectLocation();
        //返回的结果
        List<Map<String,Object>> localValue=new ArrayList<>();

        Map<String,Double[]> localtions=new HashMap<>();


        for (IpResult item : ipResults
        ) {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("name",item.getCity());
            hashMap.put("value",item.getTotal());
            localValue.add(hashMap);
            Double[] doubles = new Double[2];
            doubles[0]= item.getLon();
            doubles[1]= item.getLat();
            localtions.put(item.getCity(), doubles);
        }
        Map<String, Object> result = new HashMap<>();
        result.put("localValue",localValue);
        result.put("locations",localtions);
        return AjaxResult.success(result);
    }

    /**
     * 查询ip地址地域列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:location:list')")
    @GetMapping("/list")
    public TableDataInfo list(IpLocation ipLocation)
    {
        startPage();
        List<IpLocation> list = ipLocationService.selectIpLocationList(ipLocation);
        return getDataTable(list);
    }

    /**
     * 导出ip地址地域列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:location:export')")
    @Log(title = "ip地址地域", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(IpLocation ipLocation)
    {
        List<IpLocation> list = ipLocationService.selectIpLocationList(ipLocation);
        ExcelUtil<IpLocation> util = new ExcelUtil<IpLocation>(IpLocation.class);
        return util.exportExcel(list, "ip地址地域数据");
    }

    /**
     * 获取ip地址地域详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:location:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(ipLocationService.selectIpLocationById(id));
    }

    /**
     * 新增ip地址地域
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:location:add')")
    @Log(title = "ip地址地域", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody IpLocation ipLocation)
    {
        return toAjax(ipLocationService.insertIpLocation(ipLocation));
    }

    /**
     * 修改ip地址地域
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:location:edit')")
    @Log(title = "ip地址地域", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody IpLocation ipLocation)
    {
        return toAjax(ipLocationService.updateIpLocation(ipLocation));
    }

    /**
     * 删除ip地址地域
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:location:remove')")
    @Log(title = "ip地址地域", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(ipLocationService.deleteIpLocationByIds(ids));
    }
}
