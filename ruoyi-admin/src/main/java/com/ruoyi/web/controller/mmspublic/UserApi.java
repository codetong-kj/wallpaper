package com.ruoyi.web.controller.mmspublic;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.RedisKeyUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.wallpaper.domain.Favorites;
import com.ruoyi.wallpaper.service.IFavoritesService;
import com.ruoyi.wallpaper.util.JwtUtils;
import com.ruoyi.wallpaper.domain.WUser;
import com.ruoyi.wallpaper.service.IWUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.concurrent.TimeUnit;

@Api
@RestController
@RequestMapping("/dev-api/public/myuser")
public class UserApi extends BaseController {
    @Autowired
    IWUserService userService;

    @Autowired
    RedisCache redisCache;

    @Autowired
    IFavoritesService favoritesService;

    @ApiOperation("用户注册")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "userName", value = "用户账号", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "password", value = "用户密码", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "nickName", value = "用户昵称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "wechat", value = "用户微信", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "phone", value = "手机号", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "idCart", value = "用户身份证", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "birthday", value = "用户生日", dataType = "Date", dataTypeClass = Date.class),
            @ApiImplicitParam(name = "alipay", value = "支付宝账号", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "email", value = "用户邮箱", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "sex", value = "用户性别（0男 1女 2未知）", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "avatar", value = "头像地址", dataType = "String", dataTypeClass = String.class)
    })
    @PostMapping("/register")
    private AjaxResult register(@RequestBody WUser wUser) {
        System.out.println(wUser);
        String regExp = "^[^0-9][\\w_]{5,9}$";
        String userName = wUser.getUserName();
        if (userName.isEmpty()) {
            return error("用户名不能为空");
        }
        if (!userName.matches(regExp)) {
            return error("用户名只能包括字母数字下划线且至少6位字符!");
        }
        int countByUserName = userService.selectCountByUserName(userName);
        if (countByUserName > 0) {
            return error("用户名不能重复");
        }

        wUser.setPassword(DigestUtils.md5DigestAsHex(wUser.getPassword().getBytes()));


        String id = UUID.getId();
        if (null == wUser.getUserId()) {
            wUser.setUserId(id);
        }
        if (null == wUser.getNickName()) {
            wUser.setNickName("bzl_" + id);
        }
        return toAjax(userService.insertWUser(wUser));
    }


    @ApiOperation("根据用户名和密码查询用户信息")
    @PostMapping("/login")
    private AjaxResult login(@RequestBody WUser user) {
        if (user == null) {
            return error("用户名或密码不能为空");
        }
        String userName = user.getUserName();
        String password = user.getPassword();

        password = DigestUtils.md5DigestAsHex(password.getBytes());
        WUser wUser1 = new WUser();
        wUser1.setUserName(userName);
        wUser1.setPassword(password);
        wUser1.setPhone(userName);
        WUser wUser = userService.selectWUserByNamePwd(wUser1);
        if (wUser != null) {
            //最后一次登录时间
            wUser.setLoginDate(DateUtils.getNowDate());
            userService.updateWUser(wUser);
            String jwtTokenByUserId = JwtUtils.getJwtTokenByUserId(wUser.getUserId());
            redisCache.setCacheObject(Constants.LOGIN_USER_TOKEN_KEY + jwtTokenByUserId, wUser, 2, TimeUnit.DAYS);

            return AjaxResult.success(jwtTokenByUserId);
        } else {
            return error("用户名或密码错误！");
        }
    }

    @GetMapping("/selectUserNickName/{userId}")
    public AjaxResult selectUserNickName(@PathVariable("userId") String userId) {
        return AjaxResult.success(userService.selectUserNickName(userId));
    }

    @ApiOperation("根据userId获取用户信息")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "String", paramType = "path", dataTypeClass = String.class)
    @GetMapping("/{userId}")
    private AjaxResult getUserInfo(@PathVariable String userId) {
        return AjaxResult.success(userService.selectUserSimpleInfo(userId));
    }

    @PostMapping("/loginByPhone")
    private AjaxResult loginByPhone(@RequestBody String phoneForm) {
        JSONObject jsonObject = JSONObject.parseObject(phoneForm);
        String phone = jsonObject.getString("phone");
        String code = jsonObject.getString("code");
        if (StringUtils.isEmpty(phone) && StringUtils.isEmpty(code)) {
            return error("手机号或验证码为空！");
        }
        System.out.println(phone + "----" + code);
        AjaxResult ajaxResult = new AjaxResult();
        String phoneCodeKey = RedisKeyUtils.getPhoneCodeKey(phone);
        String redisCode = redisCache.getCacheObject(phoneCodeKey);
        if (code.equals(redisCode)) {
            //根据手机号 查用户信息 如果查到就登录 未查到 则注册
            WUser user = new WUser();
            user.setPhone(phone);
            WUser wUser = userService.selectOneUser(user);
            if (wUser == null) {
                user = new WUser();
                String id = UUID.getId();
                if (null == user.getUserId()) {
                    user.setUserId(id);
                }
                if (null == user.getNickName()) {
                    user.setNickName("bzl_" + UUID.getId());
                }
                if (null == user.getNickName()) {
                    user.setUserName("bzl_" + UUID.getId());
                }
                user.setLoginDate(DateUtils.getNowDate());
                user.setPhone(phone);
                int i = userService.insertWUser(user);
                if (i > 0) {
                    String jwtTokenByUserId = JwtUtils.getJwtTokenByUserId(id);
                    redisCache.setCacheObject(Constants.LOGIN_USER_TOKEN_KEY + jwtTokenByUserId, user, 2, TimeUnit.DAYS);
                    ajaxResult.put("token", jwtTokenByUserId);
                    ajaxResult.put("code", 200);
                    //此处创建一个新线程，对用户收藏夹进行插入操作
                    new Thread(()->{
                        Favorites favorites = new Favorites();
                        favorites.setUserId(id);
                        favorites.setfName("默认图片收藏夹");
                        favorites.setType(0);
                        favoritesService.insertFavorites(favorites);
                    }).start();
                    new Thread(()->{
                        Favorites favorites = new Favorites();
                        favorites.setUserId(id);
                        favorites.setfName("默认视频收藏夹");
                        favorites.setType(1);
                        favoritesService.insertFavorites(favorites);
                    }).start();

                }
            }else{
                String token = userService.userLogin(wUser);
                ajaxResult.put("token", token);
                ajaxResult.put("code", 200);
            }

        }
        return ajaxResult;
    }


}
