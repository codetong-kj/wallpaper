package com.ruoyi.web.controller.mmspublic;

import com.alibaba.fastjson.JSONObject;
import com.github.pagehelper.PageHelper;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.RedisKeyUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.wallpaper.domain.CommentResult;
import com.ruoyi.wallpaper.domain.WUser;
import com.ruoyi.wallpaper.domain.WUserWorks;
import com.ruoyi.wallpaper.domain.WorkResult;
import com.ruoyi.wallpaper.service.*;
import com.ruoyi.wallpaper.util.JwtUtils;
import com.ruoyi.wallpaper.util.LikeUtil;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@Api
@RestController
@RequestMapping("/dev-api/public/work")
public class WorkApi extends BaseController {
    @Autowired
    IWUserWorksService worksService;
    @Autowired
    private ICommentService commentService;
    @Autowired
    IWUserService userService;
    @Autowired
    IUserLikeService likeService;
    @Autowired
    LikeService redisLikeService;

    @Autowired
    RedisCache redisCache;

    /**
     * @Description
     * @Author weiwei
     * @Date 2022/2/22 15:30
     * @Param
     * @Return 获取评论
     * @Exception
     */
    @PostMapping("/getComment")
    public AjaxResult getComment(@RequestBody String data) {
        JSONObject jsonObject = JSONObject.parseObject(data);
        String workId = jsonObject.getString("toWorkId");
        Integer pageNum = jsonObject.getInteger("pageNum");
        Integer pageSize = jsonObject.getInteger("pageSize");
        List<CommentResult> commentSecond = commentService.selectListSecondCommentListByWorkId(workId);
        Integer total = commentService.selectFirstTotalByWorkId(workId);
        ArrayList<Object> commentResults = new ArrayList<>();
        PageHelper.startPage(pageNum, pageSize);
        List<CommentResult> commentFirst = commentService.selectListFirstCommentListByWorkId(workId);
        for (CommentResult item : commentFirst
        ) {
            HashMap<String, Object> first = new HashMap<>();
            first.put("comment", item);
            ArrayList<CommentResult> second = new ArrayList<>();
            for (CommentResult item2 : commentSecond
            ) {
                if (item.getCommentId().equals(item2.getRootCommentId())) {
                    second.add(item2);
                }
            }
            first.put("second", second);
            commentResults.add(first);
        }
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("comments", commentResults);
        ajaxResult.put("total", total);
        return ajaxResult;
    }

    /**
     * @Description
     * @Author weiwei
     * @Date 2022/2/22 12:06
     * @Param
     * @Return 返回所有作品
     * @Exception
     */
    @GetMapping("/getWorks")
    private TableDataInfo getWorks(WUserWorks wUserWorks) {
        wUserWorks.setFlag(1);
        startPage();
        List<WorkResult> workResults = worksService.selectWorksList(wUserWorks);
        return getDataTable(workResults);
    }

    /**
     * @Description
     * @Author weiwei
     * @Date 2022/2/22 12:06
     * @Param
     * @Return 返回所有作品
     * @Exception
     */
   /* @GetMapping("/getVideos")
    private AjaxResult getVideos() {
        List<WorkResult> workResults = worksService.selectVideoList();
        return AjaxResult.success(workResults);
    }*/

    /**
     * 获取用户作品详细信息
     */
    @GetMapping(value = "/{workId}")
    public AjaxResult getInfo(@PathVariable("workId") String workId) {
        WUserWorks wUserWorks = worksService.selectWUserWorksByWorkId(workId);
        String userId = wUserWorks.getUserId();
        WUser user = userService.selectUserNickName(userId);
        //获取redis中的点赞数量
        Integer num = redisCache.getCacheMapValue(RedisKeyUtils.MAP_KEY_WORK_LIKED_COUNT, workId);
        if (num!=null&& num != 0) {
            wUserWorks.setLike(wUserWorks.getLike() + num);
        }
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("userWorks", wUserWorks);
        ajaxResult.put("userInfo", user);
        int like;
        //判断用户是否已经点赞
        LikeUtil likeUtil = new LikeUtil(redisLikeService, likeService);
        try {
            String accessUserId = JwtUtils.getUserId();
            if (accessUserId != null) {
                if (likeUtil.isLiked(workId, accessUserId)) {
                    //已点赞
                    like = 1;
                } else {
                    like = 0;
                }

            } else {
                like = 0;
            }
        } catch (Exception e) {
            like = 0;
        }
        ajaxResult.put("like", like);
        return ajaxResult;
    }

    /**
     * 获取用户作品详细信息
     */
    @PostMapping(value = "/getSecondAll")
    public AjaxResult getSecondAll(@RequestBody String data) {
        JSONObject jsonObject = JSONObject.parseObject(data);
        Long commentId = Long.valueOf(jsonObject.getString("commentId"));
        return AjaxResult.success(commentService.selectListSecondAllCommentListByWorkId(commentId));
    }


    @GetMapping("/getImageWorkByUserId/{userId}")
    public AjaxResult getImageWorkByUserId(@PathVariable("userId") String userId) {
        if (StringUtils.isNotEmpty(userId)) {
            List<WUserWorks> wUserWorks = worksService.selectWUserWorksImagesByUserId(userId);
            return AjaxResult.success(wUserWorks);
        }
        return error("用户id为空");

    }

    @GetMapping("/getVideoWorkByUserId/{userId}")
    public AjaxResult getVideoWorkByUserId(@PathVariable("userId") String userId) {
        if (StringUtils.isNotEmpty(userId)) {
            List<WUserWorks> wUserWorks = worksService.selectWUserWorksVideoByUserId(userId);
            return AjaxResult.success(wUserWorks);
        }
        return error("用户id为空");

    }


}
