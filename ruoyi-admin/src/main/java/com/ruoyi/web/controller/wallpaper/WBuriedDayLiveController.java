package com.ruoyi.web.controller.wallpaper;

import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.wallpaper.domain.WBuriedDayLive;
import com.ruoyi.wallpaper.service.IWBuriedDayLiveService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * 埋点日活Controller
 *
 * @author weiwei
 * @date 2021-12-23
 */
@RestController
@RequestMapping("/wallpaper/live")
public class WBuriedDayLiveController extends BaseController {
    @Autowired
    private IWBuriedDayLiveService wBuriedDayLiveService;

    /**
     * @Description 每日启动次数
     * @Author weiwei
     * @Date 2022/1/6 22:21
     * @Param
     * @Return
     * @Exception
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:live:getDayStartCount')")
    @GetMapping("/getDayStartCount")
    public AjaxResult getDayStartCount() {
        return AjaxResult.success(wBuriedDayLiveService.selectStartCount());
    }
    @PreAuthorize("@ss.hasPermi('wallpaper:live:getStartMoonCount')")
    @GetMapping("/getStartMoonCount")
    public AjaxResult getStartMoonCount() {
        SimpleDateFormat sdf1 =new SimpleDateFormat("yyyy-MM-dd" );
        List<String> dayTimes = new ArrayList<>();
        List<Long> datCounts = new ArrayList<>();
        List<Map<String, Object>> startMoonCount = wBuriedDayLiveService.selectStartMoonCount();
        for (Map<String, Object> item : startMoonCount
        ) {
            Date dayTime = (Date) item.get("dayTime");
            String date = sdf1.format(dayTime);
            dayTimes.add(date);
            datCounts.add((Long) item.get("dayCount"));
        }
        Map<String, Object> res = new HashMap<>();
        res.put("dayTimes", dayTimes);
        res.put("datCounts", datCounts);
        return AjaxResult.success(res);
    }


    /**
     * 查询埋点日活列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:live:list')")
    @GetMapping("/list")
    public TableDataInfo list(WBuriedDayLive wBuriedDayLive) {
        startPage();
        List<WBuriedDayLive> list = wBuriedDayLiveService.selectWBuriedDayLiveList(wBuriedDayLive);
        return getDataTable(list);
    }

    /**
     * 导出埋点日活列表
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:live:export')")
    @Log(title = "埋点日活", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(WBuriedDayLive wBuriedDayLive) {
        List<WBuriedDayLive> list = wBuriedDayLiveService.selectWBuriedDayLiveList(wBuriedDayLive);
        ExcelUtil<WBuriedDayLive> util = new ExcelUtil<WBuriedDayLive>(WBuriedDayLive.class);
        return util.exportExcel(list, "埋点日活数据");
    }

    /**
     * 获取埋点日活详细信息
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:live:query')")
    @GetMapping(value = "/{dId}")
    public AjaxResult getInfo(@PathVariable("dId") Long dId) {
        return AjaxResult.success(wBuriedDayLiveService.selectWBuriedDayLiveByDId(dId));
    }

    /**
     * 新增埋点日活
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:live:add')")
    @Log(title = "埋点日活", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody WBuriedDayLive wBuriedDayLive) {
        return toAjax(wBuriedDayLiveService.insertWBuriedDayLive(wBuriedDayLive));
    }

    /**
     * 修改埋点日活
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:live:edit')")
    @Log(title = "埋点日活", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody WBuriedDayLive wBuriedDayLive) {
        return toAjax(wBuriedDayLiveService.updateWBuriedDayLive(wBuriedDayLive));
    }

    /**
     * 删除埋点日活
     */
    @PreAuthorize("@ss.hasPermi('wallpaper:live:remove')")
    @Log(title = "埋点日活", businessType = BusinessType.DELETE)
    @DeleteMapping("/{dIds}")
    public AjaxResult remove(@PathVariable Long[] dIds) {
        return toAjax(wBuriedDayLiveService.deleteWBuriedDayLiveByDIds(dIds));
    }
}
