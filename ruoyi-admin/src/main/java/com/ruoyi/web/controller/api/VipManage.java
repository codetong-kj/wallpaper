package com.ruoyi.web.controller.api;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wallpaper.domain.WVip;
import com.ruoyi.wallpaper.service.IWVipService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

/**
 * 用户VIP管理
 */
@Api("用户VIP管理")
@RestController
@RequestMapping("/dev-api/vip")
public class VipManage extends BaseController {

    @Autowired
    IWVipService vipService;

    @ApiOperation("根据用户id查询用户vip")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "String", paramType = "path", dataTypeClass = String.class)
    @GetMapping("/{userId}")
    private AjaxResult selectUserVip(@PathVariable String userId) {
        WVip wVip = vipService.selectWVipByUserId(userId);
        if (wVip == null) {
            return error("未查询到任何信息");

        } else {
            return AjaxResult.success(wVip);
        }

    }

    @ApiOperation("根据vipId查询用户vip")
    @ApiImplicitParam(name = "vipId", value = "VipID", required = true, dataType = "Long", paramType = "path", dataTypeClass = Long.class)
    @GetMapping("selectUserVipByVipId/{vipId}")
    private AjaxResult selectUserVipByVipId(@PathVariable Long vipId) {
        WVip wVip = vipService.selectWVipByVipId(vipId);
        if (wVip == null) {
            return error("未查询到任何信息");

        } else {
            return AjaxResult.success(wVip);
        }

    }

    @ApiOperation("新增会员")
    @PostMapping("/insert")
    private AjaxResult insertVip(WVip vip) {
        if (vip == null || vip.getVipId() == 0) {
            return error("请填写用户id");

        }
        if (vip.getStartTime() == null) {
            vip.setStartTime(DateUtils.getNowDate());
        }
        return toAjax(vipService.insertWVip(vip));
    }

    @ApiOperation("修改会员信息")
    @PostMapping("/update")
    private AjaxResult update(WVip vip) {
        return toAjax(vipService.updateWVip(vip));
    }

    @ApiOperation("获取会员列表")
    @PostMapping("/list")
    private AjaxResult list(WVip vip) {
        return AjaxResult.success(vipService.selectWVipList(vip));
    }
    @ApiOperation("查询会员是否过期")
    @GetMapping("/isOutTime/{userId}")
    private AjaxResult isOutTime(@PathVariable String userId) {
        WVip wVip = vipService.selectWVipByUserId(userId);
        if (wVip == null) {
            return error("未查询到该会员信息");
        }
        Date startTime = DateUtils.getNowDate();
        Date endTime = wVip.getEndTime();
        boolean timeOut = DateUtils.isTimeOut(endTime, startTime);
        return toAjax(timeOut);

    }

}
