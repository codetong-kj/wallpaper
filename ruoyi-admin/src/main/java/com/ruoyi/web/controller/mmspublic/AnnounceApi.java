package com.ruoyi.web.controller.mmspublic;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.TableDataInfo;
import com.ruoyi.wallpaper.domain.Announce;
import com.ruoyi.wallpaper.service.IAnnounceService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api
@RestController
@RequestMapping("/dev-api/public/announce")
public class AnnounceApi extends BaseController {
    @Autowired
    private IAnnounceService announceService;

    /**
     * 查询公告列表
     */
    @GetMapping("/list")
    public TableDataInfo list(Announce announce) {
        startPage();
        List<Announce> list = announceService.selectAnnounceList(announce);
        return getDataTable(list);
    }


    /**
     * 获取公告详细信息
     */
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Integer id) {
        return AjaxResult.success(announceService.selectAnnounceByAid(id));
    }

}
