package com.ruoyi.web.controller.api;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.wallpaper.service.IFavoritesService;
import com.ruoyi.wallpaper.util.JwtUtils;
import com.ruoyi.util.Utils;
import com.ruoyi.wallpaper.domain.WUser;
import com.ruoyi.wallpaper.domain.WVip;
import com.ruoyi.wallpaper.service.IWUserService;
import com.ruoyi.wallpaper.service.IWVipService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 用户管理
 *
 * @author ke
 */
@Api("用户管理")
@RestController
@RequestMapping("/dev-api/user")
public class UserManage extends BaseController {
    @Autowired
    IWUserService userService;
    @Autowired
    IWVipService vipService;
    @Autowired
    IFavoritesService favoritesService;


    @ApiOperation("根据userId获取用户信息")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "String", paramType = "path", dataTypeClass = String.class)
    @GetMapping("/{userId}")
    private AjaxResult getUserInfo(@PathVariable String userId) {
        WUser wUser = userService.selectWUserByUserId(userId);
        WVip wVip = vipService.selectWVipByUserId(userId);
        HashMap<String, Object> resultMap = new HashMap<>(20);
        resultMap.put("userInfo", wUser);
        resultMap.put("vipInfo", wVip);

        if (wUser != null) {
            return AjaxResult.success(resultMap);
        } else {
            return error("该用户不存在");
        }

    }

    @ApiOperation("根据用户名和密码查询用户信息")
    @PostMapping("/login")
    private AjaxResult login(String userName, String password) {
        if (userName == null || password == null) {
            return error("用户名或密码不能为空");
        }
        password = DigestUtils.md5DigestAsHex(password.getBytes());
        WUser wUser1 = new WUser();
        wUser1.setUserName(userName);
        wUser1.setPassword(password);
        wUser1.setPhone(userName);
        WUser wUser = userService.selectWUserByNamePwd(wUser1);
        if (wUser != null) {
            //最后一次登录时间
            wUser.setLoginDate(DateUtils.getNowDate());
            userService.updateWUser(wUser);
            String jwtTokenByUserId = JwtUtils.getJwtTokenByUserId(wUser.getUserId());
            return AjaxResult.success(jwtTokenByUserId);
        } else {
            return error("用户名或密码错误！");
        }
    }


    @ApiOperation("用户注册")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "userName", value = "用户账号", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "password", value = "用户密码", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "nickName", value = "用户昵称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "wechat", value = "用户微信", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "phone", value = "手机号", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "idCart", value = "用户身份证", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "birthday", value = "用户生日", dataType = "Date", dataTypeClass = Date.class),
            @ApiImplicitParam(name = "alipay", value = "支付宝账号", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "email", value = "用户邮箱", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "sex", value = "用户性别（0男 1女 2未知）", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "avatar", value = "头像地址", dataType = "String", dataTypeClass = String.class)
    })
    @PostMapping("/register")
    private AjaxResult register(WUser wUser) {
        String regExp = "^[^0-9][\\w_]{5,9}$";
        String userName = wUser.getUserName();
        if (userName.isEmpty()) {
            return error("用户名不能为空");
        }
        if (!userName.matches(regExp)) {
            return error("用户名只能包括字母数字下划线且至少6位字符!");
        }
        if (!Utils.isChinaPhoneLegal(wUser.getPhone())) {
            return error("手机号验证失败!");
        }
        int countByUserName = userService.selectCountByUserName(userName);
        if (countByUserName > 0) {
            return error("用户名不能重复");
        }

        wUser.setPassword(DigestUtils.md5DigestAsHex(wUser.getPassword().getBytes()));


        String id = UUID.getId();
        if (null == wUser.getUserId()) {
            wUser.setUserId(id);
        }
        if (null == wUser.getNickName()) {
            wUser.setNickName("bzl_" + id);
        }
        return toAjax(userService.insertWUser(wUser));
    }

    @ApiOperation("修改用户信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "userName", value = "用户账号", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "password", value = "用户密码", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "nickName", value = "用户昵称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "wechat", value = "用户微信", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "phone", value = "手机号", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "idCart", value = "用户身份证", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "birthday", value = "用户生日", dataType = "Date", dataTypeClass = Date.class),
            @ApiImplicitParam(name = "alipay", value = "支付宝账号", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "email", value = "用户邮箱", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "sex", value = "用户性别（0男 1女 2未知）", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "avatar", value = "头像地址", dataType = "String", dataTypeClass = String.class)
    })
    @PostMapping("/update")
    private AjaxResult update(WUser wUser) {
        return toAjax(userService.updateWUser(wUser));
    }

    @ApiOperation("删除用户")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "String", paramType = "path", dataTypeClass = String.class)
    @PostMapping("/delete/{userId}")
    private AjaxResult deleteUser(@PathVariable String userId) {
        return toAjax(userService.deleteWUserByUserId(userId));
    }

    @ApiOperation("获取用户列表")
    @PostMapping("/list")
    private AjaxResult list(WUser user) {
        return AjaxResult.success(userService.selectWUserList(user));
    }

    @ApiOperation("获取用户昵称")
    @GetMapping("/getUserNickName/{userId}")
    private AjaxResult getUserNickName(@PathVariable String userId) {
        return AjaxResult.success(userService.selectUserNickName(userId));
    }
    @ApiOperation("获取用户总数")
    @GetMapping("/getUserCount")
    private AjaxResult getUserCount() {
        return AjaxResult.success(userService.selectUserCount());
    }

    @ApiOperation("根据token获取用户信息")
    @PostMapping("/getUserInfoByToken")
    private AjaxResult getUserInfoByToken(String token, HttpServletRequest request) throws UnsupportedEncodingException {
        if (StringUtils.isEmpty(token)) {
            token = request.getHeader("token");
        }
        Map<String, String> tokenInfo = JwtUtils.getTokenInfo(token);
        String userId = tokenInfo.get("id");
        WUser wUser = userService.selectWUserByUserId(userId);
        WVip wVip = vipService.selectWVipByUserId(userId);
        AjaxResult success = AjaxResult.success();
        success.put("userInfo", wUser);
        success.put("userVip", wVip);
        boolean isVip = false;
        String datePoor = "";
        if (wVip != null) {
            boolean timeOut = DateUtils.isTimeOut(wVip.getEndTime(), DateUtils.getNowDate());
            if (timeOut) {
                isVip = true;
                datePoor = DateUtils.getDatePoor(wVip.getEndTime(), DateUtils.getNowDate());
            }
        }
        success.put("isVip", isVip);
        success.put("datePoor", datePoor);

        return success;
    }


}
