package com.ruoyi.web.controller.mmspublic;

import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.RedisKeyUtils;
import com.ruoyi.common.utils.VerifyCodeUtils;
import com.ruoyi.wallpaper.service.ISendPhoneCode;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.TimeUnit;

@Api("手机验证码")
@RestController
@RequestMapping("/dev-api/public/code")
public class VerifyCodeApi {

    @Autowired
    ISendPhoneCode sendPhoneCode;

    @Autowired
    RedisCache redisCache;


    /**
     * @Description 发送验证码
     * @Author weiwei
     * @Date 2022/3/26 17:21
     * @Param
     * @Return
     * @Exception
     */
    @PostMapping("/{phoneNumber}")
    public AjaxResult send(@PathVariable("phoneNumber") String phoneNumber) {
        String code = VerifyCodeUtils.generatePhoneVerifyCode(4);
        String phoneCodeKey = RedisKeyUtils.getPhoneCodeKey(phoneNumber);
        boolean b = sendPhoneCode.sendCode(phoneNumber, code);
        if (b) {
            redisCache.setCacheObject(phoneCodeKey, code, 5, TimeUnit.MINUTES);
            return AjaxResult.success();
        }
        return AjaxResult.error();
    }
}
