package com.ruoyi.web.controller.pay;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wallpaper.domain.WBuriedPay;
import com.ruoyi.wallpaper.domain.WUser;
import com.ruoyi.wallpaper.domain.WVip;
import com.ruoyi.wallpaper.domain.WallpaperOrderDetail;
import com.ruoyi.wallpaper.service.*;
import com.ruoyi.wallpaper.service.pay.Alipay;
import com.ruoyi.web.controller.util.GenerateNum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api("开通会员支付接口")
@Controller
@RequestMapping("/dev-api/alipay")
public class AliPayController extends BaseController {

    @Autowired
    Alipay alipay;

    @Autowired
    private IWallpaperOrderDetailService wallpaperOrderDetailService;

    @Autowired
    private IWVipService vipService;

    @Autowired
    private IWBuriedPayService payService;

    @Autowired
    private IWBuriedService buriedService;


    @ResponseBody
    @ApiOperation("用户开通VIP请求")
    @GetMapping("/subscribe_vip/{userId}/{type}")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "String", paramType = "path", dataTypeClass = Integer.class)
    private String pay(@PathVariable String userId, @PathVariable Integer type, Model model) {
        if (userId == null || type == null) {
            model.addAttribute("info", "参数为空，错误，请联系管理员。");
        }
        BigDecimal total_amount = new BigDecimal(0);
        switch (type) {
            case 1:
                total_amount = total_amount.add(new BigDecimal("0.01"));
                break;
            case 2:
                total_amount = total_amount.add(new BigDecimal("2.88"));
                break;
            case 3:
                total_amount = total_amount.add(new BigDecimal(58));
                break;
            default:
                model.addAttribute("info", "错误的参数");
        }

        String orderNumber = GenerateNum.generateOrder();
        System.out.println("生成的订单号：" + orderNumber);


        String subject = "开通VIP";
        String ptype = "vip";
        WallpaperOrderDetail wallpaperOrderDetail = new WallpaperOrderDetail();
        wallpaperOrderDetail.setUserId(userId);
        wallpaperOrderDetail.setOrdertitle(subject);
        wallpaperOrderDetail.setOrderNumber(orderNumber);
        wallpaperOrderDetail.setPaymethod("1");
        int i = wallpaperOrderDetailService.insertWallpaperOrderDetail(wallpaperOrderDetail);
        String payPage = "";
        if (i >= 0) {
            payPage = alipay.getPayPage(userId, orderNumber, total_amount, subject, ptype);
            AjaxResult ajaxResult = new AjaxResult(200, "请求成功");
            ajaxResult.put("payPage", payPage);
            ajaxResult.put("orderNumber", orderNumber);
            System.out.println(payPage);
            model.addAttribute("payPage", payPage);
            model.addAttribute("orderNumber", orderNumber);
//            return ajaxResult;
        }
        return payPage;
    }

    @ResponseBody
    @RequestMapping("/notify_url")
    private String notify_url(HttpServletRequest request) throws Exception {
        //支付回调
        boolean result = alipayCallback(request);
        if (result) {
            return "success";
        } else {
            return "fail";
        }

    }

    private boolean alipayCallback(HttpServletRequest request) {
        //获取支付宝GET返回的信息
        HashMap<String, String> params = new HashMap<String, String>();
        Map<String, String[]> requestParams = request.getParameterMap();
        for (String name : requestParams.keySet()) {
            String[] values = requestParams.get(name);
            String valueStr = "";
            for (int i = 0; i < values.length; i++) {
                valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
            }
            params.put(name, new String(valueStr.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8));

        }
        //计算得出通知验证结果
        System.out.println("获取支付宝回传参数----" + params);
        //返回公共参数
        String tradeNo = params.get("trade_no");
        String out_trade_no = params.get("out_trade_no");
        String subject = params.get("subject");
        String buyer_id = params.get("buyer_id");
        String total_amount = params.get("total_amount");
        System.out.println("自定义订单号：----" + out_trade_no);
        if (out_trade_no == null) {
            return false;
        }
        WallpaperOrderDetail wallpaperOrderDetail = wallpaperOrderDetailService.selectWallpaperOrderDetailByOrderNum(out_trade_no);
        if (wallpaperOrderDetail == null) {
            return false;
        }
        try {
            //支付成功，保存订单交易明细
            if (subject != null && wallpaperOrderDetail.getOrdertitle().equalsIgnoreCase("开通VIP")) {
                wallpaperOrderDetail.setPaymethod("1");
                wallpaperOrderDetail.setTradeno(tradeNo);
                //设置用户id
                wallpaperOrderDetail.setCreateTime(DateUtils.getNowDate());
                wallpaperOrderDetail.setUsername(buyer_id);
                wallpaperOrderDetail.setPrice(total_amount);
                int i = wallpaperOrderDetailService.updateWallpaperOrderDetail(wallpaperOrderDetail);
                //修改用户vip状态
                WVip wVip1 = vipService.selectWVipByUserId(wallpaperOrderDetail.getUserId());

                if (wVip1 != null) {
                    boolean timeOut = DateUtils.isTimeOut(wVip1.getEndTime(), wVip1.getStartTime());
                    //如果有vip信息 则修改
                    wVip1.setVipLevel(1);
                    wVip1.setStartTime(DateUtils.getNowDate());
                    Date endTime = wVip1.getEndTime();
                    if (timeOut) {
                        switch (total_amount) {
                            case "0.01":
                                wVip1.setEndTime(DateUtils.addDays(endTime, 7));
                                break;
                            case "2.88":
                                wVip1.setEndTime(DateUtils.addDays(endTime, 30));
                                break;
                            case "58":
                                wVip1.setEndTime(DateUtils.addDays(endTime, 90));
                                break;
                            default:
                                break;

                        }


                    } else {
                        switch (total_amount) {
                            case "0.01":
                                wVip1.setEndTime(DateUtils.addDays(DateUtils.getNowDate(), 7));
                                break;
                            case "2.88":
                                wVip1.setEndTime(DateUtils.addDays(DateUtils.getNowDate(), 30));
                                break;
                            case "58":
                                wVip1.setEndTime(DateUtils.addDays(DateUtils.getNowDate(), 90));
                                break;
                            default:
                                break;

                        }


                    }
                    int i1 = vipService.updateWVip(wVip1);
                    if (i1 > 0) {
                        return true;
                    }
                } else {
                    WVip wVip = new WVip();
                    wVip.setUserId(wallpaperOrderDetail.getUserId());
                    wVip.setVipLevel(1);
                    wVip.setStartTime(DateUtils.getNowDate());
                    switch (total_amount) {
                        case "0.01":
                            wVip.setEndTime(DateUtils.addDays(DateUtils.getNowDate(), 7));
                            break;
                        case "2.88":
                            wVip.setEndTime(DateUtils.addDays(DateUtils.getNowDate(), 30));
                            break;
                        case "58":
                            wVip.setEndTime(DateUtils.addDays(DateUtils.getNowDate(), 90));
                            break;
                        default:
                            break;

                    }
                    int i1 = vipService.insertWVip(wVip);
                    if (i1 >= 0) {
                        return true;
                    }
                }


            }
        } catch (Exception ex) {
            System.out.println("支付宝出现异常。。。");
            return false;
        }


        return true;
    }

    @RequestMapping("/return_url")
    private String returnUrl(Model model, HttpServletRequest request) {
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("status", "true");
        model.addAttribute("info", "您已成功开通VIP，快去查看吧。");
        try {
            request.setAttribute("ajaxResult", new ObjectMapper().writeValueAsString(ajaxResult));
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "pay";

    }

    @ResponseBody
    @ApiOperation("轮询接口")
    @GetMapping("/roll_back/{userId}")
    private AjaxResult rollBack(@PathVariable String userId) {
        WVip wVip = vipService.selectWVipByUserId(userId);
        if (wVip != null && wVip.getVipLevel() > 0) {
            return AjaxResult.success(wVip);
        } else {
            return AjaxResult.error("支付失败");
        }


    }


}
