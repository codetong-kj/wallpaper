package com.ruoyi.web.controller.weixinpay;

import com.google.gson.Gson;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.http.HttpUtils;
import com.ruoyi.wallpaper.domain.WBuriedPay;
import com.ruoyi.wallpaper.domain.WVip;
import com.ruoyi.wallpaper.domain.WallpaperOrderDetail;
import com.ruoyi.wallpaper.service.IWBuriedPayService;
import com.ruoyi.wallpaper.service.IWBuriedService;
import com.ruoyi.wallpaper.service.IWVipService;
import com.ruoyi.wallpaper.service.IWallpaperOrderDetailService;
import com.ruoyi.wallpaper.service.pay.Alipay;
import com.ruoyi.web.controller.util.GenerateNum;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;
import java.io.*;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Api("开通会员支付接口")
@RestController
@RequestMapping("/dev-api/wexinpay")
public class WechatPayController extends BaseController {


    @Autowired
    private IWallpaperOrderDetailService wallpaperOrderDetailService;

    @Autowired
    private IWVipService vipService;

    @Autowired
    private IWBuriedPayService payService;

    @Autowired
    private IWBuriedService buriedService;


    @ApiOperation("用户开通VIP请求")
    @GetMapping("/subscribe_vip/{userId}/{type}")
    @ApiImplicitParam(name = "userId", value = "用户ID", required = true, dataType = "String", paramType = "path", dataTypeClass = String.class)
    private AjaxResult pay(@PathVariable String userId, @PathVariable Integer type) throws Exception {
        if (userId == null || type == null) {
            return null;
        }
        BigDecimal total_amount = new BigDecimal(0);
        switch (type) {
            case 1:
                total_amount = total_amount.add(new BigDecimal(1));
                break;
            case 2:
                total_amount = total_amount.add(new BigDecimal(288));
                break;
            case 3:
                total_amount = total_amount.add(new BigDecimal(5800));
                break;
            default:
                return error("支付金额错误");
        }

        String orderNumber = GenerateNum.generateOrder();
        System.out.println("生成的订单号：" + orderNumber);
        String subject = "开通VIP";
        String ptype = "vip";
        WallpaperOrderDetail wallpaperOrderDetail = new WallpaperOrderDetail();
        wallpaperOrderDetail.setUserId(userId);
        wallpaperOrderDetail.setOrdertitle(subject);
        wallpaperOrderDetail.setOrderNumber(orderNumber);
        wallpaperOrderDetail.setPaymethod("2");
        int i = wallpaperOrderDetailService.insertWallpaperOrderDetail(wallpaperOrderDetail);
        if (i >= 0) {
            MyConfig config = new MyConfig();
            com.ruoyi.web.controller.weixinpay.WXPay wxpay = new com.ruoyi.web.controller.weixinpay.WXPay(config);
            System.out.println("支付金额" + total_amount);
            Map<String, String> data = new HashMap<String, String>();
            data.put("body", subject);//商品名称
            data.put("out_trade_no", orderNumber);//内部订单号
            data.put("device_info", "");//设备名称
            data.put("fee_type", "CNY");
            data.put("total_fee", total_amount.toString());//2分钱
            data.put("spbill_create_ip", "123.12.12.123");//服务器IP
            data.put("notify_url", "https://bizhilou.com/dev-api/wexinpay/notify_url");//回调地址
            data.put("trade_type", "NATIVE");  // 此处指定为扫码支付
            data.put("product_id", "12");
            data.put("sign", MyConfig.SIGN);
            Map<String, String> resp = wxpay.unifiedOrder(data);
            String code_url = resp.get("code_url");
            if (code_url != null) {
                AjaxResult success = AjaxResult.success();
                success.put("code_url", code_url);
                success.put("orderNumber", orderNumber);
                return success;
            }
        }
        return error("获取二维码失败！");
    }

    @GetMapping("/hello")
    private String pay() {
        return "hello";
    }

    @PostMapping("/notify_url")
    private String notifyUrl(HttpServletRequest request, HttpServletResponse response) throws Exception {
        System.out.println("进入回调函数，，，，，，，，，，，，，，，，");


        Gson gson = new Gson();
        //创建应答
        Map<String, String> map = new HashMap<>();
        String body = HttpUtils.readData(request);
        boolean b = false;
        try {

            Map<String, String> params = WXPayUtil.xmlToMap(body);

            logger.info("支付通知的完整数据========================>{}", body);
//        Map<String, Object> bodyMap = gson.fromJson(params, HashMap.class);
            logger.info("支付通知的id====>{}", params.get("id"));
            b = weixinCallback(params);
        } catch (Exception e) {
            logger.info(e.getMessage());
        }

        response.setContentType("application/xml;charset=UTF-8");
        if (!b) {
            //成功应答
            response.setStatus(500);
            map.put("code", "ERROR");
            map.put("message", "失败");
            String s = gson.toJson(map);
            System.out.println("string............map haha" + s);
            return s;
            //成功应答
          /*  response.setContentType("application/json;charset=UTF-8");
            response.setStatus(200);
            map.put("code", "SUCCESS");
            map.put("message", "成功");
            System.out.println("成功。。。。。。。。。。");*/
        } /*else {
            response.setContentType("application/json;charset=UTF-8");
            //成功应答
            response.setStatus(500);
            map.put("code", "ERROR");
            map.put("message", "失败");
        }*/

        return "<xml>\n" +
                "  <return_code><![CDATA[SUCCESS]]></return_code>\n" +
                "  <return_msg><![CDATA[OK]]></return_msg>\n" +
                "</xml>";


    }

    private boolean weixinCallback(Map<String, String> params) {
        //计算得出通知验证结果
        System.out.println("获取微信回传参数----" + params);
        //返回公共参数
        String tradeNo = params.get("transaction_id");
        String out_trade_no = params.get("out_trade_no");
        String result_code = params.get("result_code");
        String buyer_id = params.get("transaction_id");
        String total_fee = params.get("total_fee");
        System.out.println("自定义订单号：----" + out_trade_no);
        if (out_trade_no == null) {
            return false;
        }
        WallpaperOrderDetail wallpaperOrderDetail = wallpaperOrderDetailService.selectWallpaperOrderDetailByOrderNum(out_trade_no);
        if (wallpaperOrderDetail == null) {
            return false;
        }
        try {
            //支付成功，保存订单交易明细
            if ("SUCCESS".equals(result_code)) {
                //转换金额
                BigDecimal bigDecimal = new BigDecimal(total_fee);
                BigDecimal total_amount = bigDecimal.multiply(new BigDecimal("0.01"));
                wallpaperOrderDetail.setPaymethod("1");
                wallpaperOrderDetail.setTradeno(tradeNo);
                //设置用户id
                wallpaperOrderDetail.setCreateTime(DateUtils.getNowDate());
                wallpaperOrderDetail.setUsername(buyer_id);
                wallpaperOrderDetail.setPrice(total_amount.toString());
                int i = wallpaperOrderDetailService.updateWallpaperOrderDetail(wallpaperOrderDetail);
                //修改用户vip状态
                WVip wVip1 = vipService.selectWVipByUserId(wallpaperOrderDetail.getUserId());
                if (wVip1 != null) {
                    boolean timeOut = DateUtils.isTimeOut(wVip1.getEndTime(), wVip1.getStartTime());
                    //如果有vip信息 则修改
                    wVip1.setVipLevel(1);
                    wVip1.setStartTime(DateUtils.getNowDate());
                    Date endTime = wVip1.getEndTime();
                    System.out.println("-------------------" + total_amount);

                    if (timeOut) {
                        switch (total_amount.toString()) {
                            case "0.01":
                                wVip1.setEndTime(DateUtils.addDays(endTime, 7));
                                break;
                            case "2.88":
                                wVip1.setEndTime(DateUtils.addDays(endTime, 30));
                                break;
                            case "58":
                                wVip1.setEndTime(DateUtils.addDays(endTime, 90));
                                break;
                            default:
                                break;

                        }


                    } else {
                        switch (total_amount.toString()) {
                            case "0.01":
                                wVip1.setEndTime(DateUtils.addDays(DateUtils.getNowDate(), 7));
                                break;
                            case "2.88":
                                wVip1.setEndTime(DateUtils.addDays(DateUtils.getNowDate(), 30));
                                break;
                            case "58":
                                wVip1.setEndTime(DateUtils.addDays(DateUtils.getNowDate(), 90));
                                break;
                            default:
                                break;

                        }


                    }

                    int i1 = vipService.updateWVip(wVip1);
                    if (i1 > 0) {
                        return true;
                    }
                } else {
                    WVip wVip = new WVip();
                    wVip.setUserId(wallpaperOrderDetail.getUserId());
                    wVip.setVipLevel(1);
                    wVip.setStartTime(DateUtils.getNowDate());
                    switch (total_amount.toString()) {
                        case "0.01":
                            wVip.setEndTime(DateUtils.addDays(DateUtils.getNowDate(), 7));
                            break;
                        case "2.88":
                            wVip.setEndTime(DateUtils.addDays(DateUtils.getNowDate(), 30));
                            break;
                        case "58":
                            wVip.setEndTime(DateUtils.addDays(DateUtils.getNowDate(), 90));
                            break;
                        default:
                            break;

                    }
                    int i1 = vipService.insertWVip(wVip);
                    return true;
                }


            }
        } catch (Exception ex) {
            System.out.println("微信出现异常。。。" + ex.getMessage());
            return false;
        }
        return true;

    }

    @RequestMapping("/return_url")
    private String returnUrl() {
        return "您已成功开通VIP，快去查看吧。";

    }

    @ApiOperation("轮询接口")
    @PostMapping("/roll_back/{orderNumber}")
    private AjaxResult rollBack(@PathVariable String orderNumber) {

        WallpaperOrderDetail wallpaperOrderDetail = wallpaperOrderDetailService.selectWallpaperOrderDetailByOrderNum(orderNumber);
        if (wallpaperOrderDetail.getTradeno() != null) {
            return AjaxResult.success("支付成功");
        } else {
            return AjaxResult.error("支付失败");
        }


    }


}
