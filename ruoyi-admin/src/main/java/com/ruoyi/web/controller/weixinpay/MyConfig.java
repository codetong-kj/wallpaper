package com.ruoyi.web.controller.weixinpay;


import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

public class MyConfig extends com.ruoyi.web.controller.weixinpay.WXPayConfig {

    private byte[] certData;
    private final String APPID = "wxf140e6efef75e752";
    private final String CERTPATH = "apiclient_cert.p12";
    private final String MCHID = "1616786940";
    private final String KEY =  "111111111111111111111111111111xX";
    public static final String SIGN ="4033AE93E5A7531A77F9332F0BE58B58EBC89791";
    public MyConfig() throws Exception {

        File file = new File(CERTPATH);
        InputStream certStream = new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();
    }

    @Override
    public String getAppID() {
        return APPID;
    }

    @Override
    public String getMchID() {
        return MCHID;
    }

    @Override
    public String getKey() {
        return KEY;
    }

    @Override
    public InputStream getCertStream() {
        ByteArrayInputStream certBis = new ByteArrayInputStream(this.certData);
        return certBis;
    }

    @Override
    public int getHttpConnectTimeoutMs() {
        return 8000;
    }

    @Override
    public int getHttpReadTimeoutMs() {
        return 10000;
    }

    @Override
    public com.ruoyi.web.controller.weixinpay.IWXPayDomain getWXPayDomain() {
        // 这个方法需要这样实现, 否则无法正常初始化WXPay
        com.ruoyi.web.controller.weixinpay.IWXPayDomain iwxPayDomain = new com.ruoyi.web.controller.weixinpay.IWXPayDomain() {

            @Override
            public void report(String domain, long elapsedTimeMillis, Exception ex) {

            }

            @Override
            public DomainInfo getDomain(com.ruoyi.web.controller.weixinpay.WXPayConfig config) {
                return new DomainInfo(com.ruoyi.web.controller.weixinpay.WXPayConstants.DOMAIN_API, true);
            }
        };
        return iwxPayDomain;

    }
}