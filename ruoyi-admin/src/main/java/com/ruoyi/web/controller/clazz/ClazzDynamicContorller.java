package com.ruoyi.web.controller.clazz;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.page.PageDomain;
import com.ruoyi.common.core.page.TableSupport;
import com.ruoyi.common.utils.http.HttpUtils;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/clazz/dynamic")
public class ClazzDynamicContorller extends BaseController {

    /**
     * 获取动态壁纸
     * @return
     */
    @GetMapping("/list")
    public AjaxResult getList() {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        String res = HttpUtils.sendGet("http://bzl.xook.top:1998/wp/v2/getwallpapers", "scategory=%E5%85%A8%E9%83%A8&category=%E5%8A%A8%E6%80%81%E5%A3%81%E7%BA%B8&&page=" + pageNum + "&limit=" + pageSize);
        JSONObject jsonObject = JSONObject.parseObject(res);
        return AjaxResult.success(jsonObject);
    }


    /**
     *  http://bzl.xook.top:1998/wp/v2/getwallpapers?scategory=%E5%85%A8%E9%83%A8&category=%E9%9D%99%E6%80%81%E5%A3%81%E7%BA%B8&page=1&limit=10
     * @return 获取静态壁纸
     */
    @GetMapping("/getStaticList")
    public AjaxResult getStaticList() {
        PageDomain pageDomain = TableSupport.buildPageRequest();
        Integer pageNum = pageDomain.getPageNum();
        Integer pageSize = pageDomain.getPageSize();
        String res = HttpUtils.sendGet("http://bzl.xook.top:1998/wp/v2/getwallpapers", "scategory=%E5%85%A8%E9%83%A8&category=%E9%9D%99%E6%80%81%E5%A3%81%E7%BA%B8&page=" + pageNum + "&limit=" + pageSize);
        JSONObject jsonObject = JSONObject.parseObject(res);
        return AjaxResult.success(jsonObject);
    }
    /**
     * 单个修改
     *
     * @param data
     * @return
     */
    @PutMapping("/update")
    public AjaxResult putDynamic(@RequestBody String data) {
        JSONObject jsonObject = JSONObject.parseObject(data);
        jsonObject.remove("_id");
        String string = jsonObject.toString();
        String post = HttpUtils.post("http://bzl.xook.top:1998/wp/v2/regwp", string);
        return success();
    }

    /**
     * 批量修改壁纸类别
     * @param data
     * @return
     */
    @PutMapping("/updateList")
    public AjaxResult putListDynamic(@RequestBody String data) {
        String post = HttpUtils.post("http://bzl.xook.top:1998/wp/v2/regwps", data);
        return success();
    }


}
