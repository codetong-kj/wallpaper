package com.ruoyi.web.controller.mmspublic;

import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.wallpaper.domain.WCategory;
import com.ruoyi.wallpaper.service.IWCategoryService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api
@RestController
@RequestMapping("/dev-api/public/category")
public class CategoryApi extends BaseController {

    @Autowired
    IWCategoryService categoryService;

    @Autowired
    RedisCache redisCache;

    /**
     * 查询文件分类列表
     */
    @GetMapping("/list")
    public AjaxResult list() {
        //先从redis里取  若没有则进行插入 若有则直接返回
        List<WCategory> imageCategories = redisCache.getCacheList(Constants.IMAGE_CATEGORY_KEY);
        List<WCategory> videoCategories = redisCache.getCacheList(Constants.VIDEO_CATEGORY_KEY);
        if (imageCategories.size() == 0 || videoCategories.size() == 0) {
            imageCategories = categoryService.selectNameList(0);
            videoCategories = categoryService.selectNameList(1);
            redisCache.setCacheList(Constants.IMAGE_CATEGORY_KEY, imageCategories);
            redisCache.setCacheList(Constants.VIDEO_CATEGORY_KEY, videoCategories);
        }
        AjaxResult ajaxResult = new AjaxResult();
        ajaxResult.put("imageCategories", imageCategories);
        ajaxResult.put("videoCategories", videoCategories);
        return ajaxResult;
    }

}
