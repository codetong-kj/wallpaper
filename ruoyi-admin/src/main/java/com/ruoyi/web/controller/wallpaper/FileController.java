package com.ruoyi.web.controller.wallpaper;

import com.ruoyi.common.config.RuoYiConfig;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.file.FileUploadUtils;
import com.ruoyi.framework.config.ServerConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 *  @author: weiwei
 *  @Date: 2022/1/10 13:14
 *  @Description: 文件上传与下载
 */
@RestController
@RequestMapping("/wallpaper/file")
public class FileController extends BaseController {
    @Autowired
    private ServerConfig serverConfig;
    /**
    * @Description
    * @Author  weiwei
    * @Date   2022/1/10 13:20
    * @Param
    * @Return
    * @Exception
    *
    */
    @PostMapping("/upload")
    public AjaxResult upload(MultipartFile file) throws Exception{
        try {
            // 上传文件路径
            String filePath = RuoYiConfig.getUploadPath();
            // 上传并返回新文件名称
            String fileName = FileUploadUtils.upload(filePath, file);

            String url = serverConfig.getUrl() + fileName;
            System.out.println(url+"<------------------");
            AjaxResult ajax = AjaxResult.success();
            ajax.put("fileName", fileName);
            ajax.put("url", url);
            return ajax;
        }catch (Exception e){
            return AjaxResult.error(e.getMessage());
        }

    }


}
