package com.ruoyi.web.controller.mmspublic;

import com.alibaba.fastjson.JSONObject;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruoyi.common.constant.Constants;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.core.redis.RedisCache;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.wallpaper.domain.Favorites;
import com.ruoyi.wallpaper.service.IFavoritesService;
import com.ruoyi.wallpaper.util.JwtUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.wallpaper.domain.SocialUser;
import com.ruoyi.wallpaper.domain.SocialUserAuth;
import com.ruoyi.wallpaper.domain.WUser;
import com.ruoyi.wallpaper.service.ISocialUserAuthService;
import com.ruoyi.wallpaper.service.ISocialUserService;
import com.ruoyi.wallpaper.service.IWUserService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import me.zhyd.oauth.config.AuthConfig;
import me.zhyd.oauth.enums.scope.AuthBaiduScope;
import me.zhyd.oauth.enums.scope.AuthGiteeScope;
import me.zhyd.oauth.exception.AuthException;
import me.zhyd.oauth.model.AuthCallback;
import me.zhyd.oauth.model.AuthResponse;
import me.zhyd.oauth.model.AuthToken;
import me.zhyd.oauth.model.AuthUser;
import me.zhyd.oauth.request.*;
import me.zhyd.oauth.utils.AuthScopeUtils;
import me.zhyd.oauth.utils.AuthStateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * @author ke
 */
@Api
@Slf4j
@Controller
@RequestMapping("/oauth")
public class SocialApi {
    @Autowired
    IWUserService userService;
    @Autowired
    RedisCache redisCache;
    @Autowired
    private ISocialUserAuthService socialUserAuthService;
    @Autowired
    private ISocialUserService socialUserService;
    @Autowired
    IFavoritesService favoritesService;


    /**
     * oauth平台中配置的授权回调地址，以本项目为例，在创建github授权应用时的回调地址应为：http://127.0.0.1:8443/oauth/callback/github
     */
    @RequestMapping("/callback/{source}")
    public String login(@PathVariable("source") String source, AuthCallback callback, HttpServletRequest request, Model model, HttpSession session) throws JsonProcessingException {
        log.info("进入callback：" + source + " callback params：" + JSONObject.toJSONString(callback));
        AuthRequest authRequest = getAuthRequest(source);
        AuthResponse<AuthUser> response = authRequest.login(callback);
        log.info(JSONObject.toJSONString(response));

        if (response.ok()) {
            //第一步先查询 系统中是否存在此用户，若存在，则登录成功
            WUser user = userService.selectUserBySUid(response.getData().getUuid());
            AjaxResult ajaxResult = new AjaxResult();
            if (null != user) {
                String token = userService.userLogin(user);
                ajaxResult.put("token", token);
            } else {
                //进行注册
                AuthUser authUser = response.getData();
                //未查询到用户，对用户进行自动化注册操作
                user = new WUser();
                String id = UUID.getId();
                if (null == user.getUserId()) {
                    user.setUserId(id);
                }
                if (null == user.getNickName()) {
                    user.setNickName("bzl_" + UUID.getId());
                }
                user.setLoginDate(DateUtils.getNowDate());
                user.setAvatar(authUser.getAvatar());
                user.setWechat(authUser.getNickname());
                userService.insertWUser(user);
                //插入第三方用户表、
                SocialUser socialUser = new SocialUser();
                socialUser.setUuid(authUser.getUuid());
                socialUser.setSource(authUser.getSource());
                AuthToken token = authUser.getToken();
                socialUser.setAccessToken(token.getAccessToken());
                int expireIn = token.getExpireIn();
                socialUser.setExpireIn(expireIn);
                socialUser.setRefreshToken(token.getRefreshToken());
                socialUser.setScope(token.getScope());
                try {
                    socialUserService.insertSocialUser(socialUser);
                    //中间表里插数据
                    SocialUserAuth socialUserAuth = new SocialUserAuth();
                    socialUserAuth.setSocialUserId(authUser.getUuid());
                    socialUserAuth.setUserId(id);
                    socialUserAuthService.insertSocialUserAuth(socialUserAuth);
                    System.out.println("注册成功---------");
                    //此处创建一个新线程，对用户收藏夹进行插入操作
                    new Thread(()->{
                        Favorites favorites = new Favorites();
                        favorites.setUserId(id);
                        favorites.setfName("默认图片收藏夹");
                        favorites.setType(0);
                        favoritesService.insertFavorites(favorites);
                    }).start();
                    new Thread(()->{
                        Favorites favorites = new Favorites();
                        favorites.setUserId(id);
                        favorites.setfName("默认视频收藏夹");
                        favorites.setType(1);
                        favoritesService.insertFavorites(favorites);
                    }).start();
                } catch (Exception e) {
                    System.out.println("error----------------->" + e.getMessage());
                }
                String jwtTokenByUserId = JwtUtils.getJwtTokenByUserId(id);
                redisCache.setCacheObject(Constants.LOGIN_USER_TOKEN_KEY + jwtTokenByUserId, user, 2, TimeUnit.DAYS);
                ajaxResult.put("token", jwtTokenByUserId);
            }
            request.setAttribute("info", new ObjectMapper().writeValueAsString(ajaxResult));
            log.info("获取到的结果为---：" + response.getData());
        }

        return "login";
    }

    @RequestMapping("/render/{source}")
    @ResponseBody
    public void renderAuth(@PathVariable("source") String source, HttpServletResponse response) throws IOException {
        System.out.println("进入render：" + source);
        AuthRequest authRequest = getAuthRequest(source);
        String authorizeUrl = authRequest.authorize(AuthStateUtils.createState());
        System.out.println(authorizeUrl);
        response.sendRedirect(authorizeUrl);
    }

    /**
    * @Description 第三方登录
    * @Author  weiwei
    * @Date   2022/1/22 12:15
    * @Param  资源类型
    * @Return
    * @Exception
    *
    */
    private AuthRequest getAuthRequest(String source) {
        AuthRequest authRequest;
        String callbackUrl="https://admin.uuu996.com/oauth/callback";
        switch (source.toLowerCase()) {
            case "baidu":
                authRequest = new AuthBaiduRequest(AuthConfig.builder()
                        .clientId("h96wSaiQrocbIoihEzF4SpWC")
                        .clientSecret("HprPfnSg5T2UcvLuVMz62puPM8fTOrzm")
                        .redirectUri(callbackUrl+"/baidu")
                        .scopes(Arrays.asList(
                                AuthBaiduScope.BASIC.getScope(),
                                AuthBaiduScope.SUPER_MSG.getScope(),
                                AuthBaiduScope.NETDISK.getScope()
                        ))
                        .build()
                );
                break;
            case "gitee":
                authRequest = new AuthGiteeRequest(AuthConfig.builder()
                        .clientId("fdf1a327aee058abe8e94e59ed1dbc7c0218d6ba5e4e2f3dc355a3b6c6563bbf")
                        .clientSecret("8b9373d88b9546b22cdccc7f1115b5679e9ebf76f9c925bf913d9b92793f3192")
                        .redirectUri("https://admin.uuu996.com/oauth/callback/gitee")
                        .scopes(AuthScopeUtils.getScopes(AuthGiteeScope.values()))
                        .build());
                break;
            case "qq":
                authRequest = new AuthQqRequest(AuthConfig.builder()
                        .clientId("101984790")
                        .clientSecret("9b4747bd6462c2f769af4208dfdeb3ca")
                        .redirectUri(callbackUrl+"/qq")
                        .build()
                );
                break;
            case "wechat":
                authRequest = new AuthWeChatOpenRequest(AuthConfig.builder()
                        .clientId("wxbc63522dab0ad86b")
                        .clientSecret("9beb93d4f6b6cb47986fabf2ae30bb6c")
                        .redirectUri("https://www.bizhilou.com/dev-api/oauth/callback/wechat_open")
                        .build()
                );
                break;
            default:
                throw new AuthException("未获得到有效的Auth配置");
        }
        return authRequest;


    }

}
