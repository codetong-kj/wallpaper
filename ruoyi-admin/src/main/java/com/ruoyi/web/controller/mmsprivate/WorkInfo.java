package com.ruoyi.web.controller.mmsprivate;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.wallpaper.domain.*;
import com.ruoyi.wallpaper.service.ICommentService;
import com.ruoyi.wallpaper.service.IWUserWorksService;
import com.ruoyi.wallpaper.service.IWVideoInfoService;
import com.ruoyi.wallpaper.util.JwtUtils;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api
@RestController
@RequestMapping("/dev-api/private/work")
public class WorkInfo extends BaseController {
    @Autowired
    IWUserWorksService worksService;
    @Autowired
    private ICommentService commentService;

    @Autowired
    private IWVideoInfoService videoInfoService;

    @PostMapping("/uploadWorks")
    private AjaxResult uploadWorks(@RequestBody String form) {
        WUserWorks wUserWork = JSONObject.parseObject(form, WUserWorks.class);
        String workId = UUID.getId();
        wUserWork.setWorkId(workId);
        wUserWork.setType(0);
        for (WFile item : wUserWork.getWFileList()
        ) {
            String fId = UUID.getId();
            item.setfId(fId);
            item.setWorkId(workId);
        }
        String userId = JwtUtils.getUserId();
        wUserWork.setUserId(userId);
        return toAjax(worksService.insertWUserWorks(wUserWork));
    }

    @PostMapping("/uploadVideo")
    private AjaxResult uploadVideo(@RequestBody String data) {
        WUserWorksVideo userWorksVideo = JSONObject.parseObject(data, WUserWorksVideo.class);
        WUserWorks wUserWorks = new WUserWorks();
        String workId = UUID.getId();
        wUserWorks.setWorkId(workId);
        wUserWorks.setType(1);
        wUserWorks.setTitle(userWorksVideo.getTitle());
        wUserWorks.setAbout(userWorksVideo.getAbout());
        wUserWorks.setcId(userWorksVideo.getcId());
        List<WVideoInfo> videoInfos = new ArrayList<>();
        List<WFile> files = new ArrayList<>();
        for (WFileVideoInfo item : userWorksVideo.getWFileList()
        ) {
            String fId = UUID.getId();
            WFile wFile = new WFile();
            wFile.setfId(fId);
            wFile.setFileName(item.getFileName());
            wFile.setDownloadUrl(item.getDownloadUrl());
            wFile.setWorkId(workId);
            WVideoInfo videoInfo = new WVideoInfo();
            videoInfo.setfId(fId);
            videoInfo.setThumbnail(item.getThumbnail() == null ? "" : item.getThumbnail());
            videoInfo.setDuration(item.getDuration());
            videoInfo.setHeight(item.getHeight());
            videoInfo.setWidth(item.getWidth());
            files.add(wFile);
            videoInfos.add(videoInfo);
        }
        wUserWorks.setWFileList(files);
        String userId = JwtUtils.getUserId();
        wUserWorks.setUserId(userId);
        int i = videoInfoService.insertVideoInfoList(videoInfos);
        if (i > 0) {
            System.out.println("视频详情插入了" + i);
        }
        return toAjax(worksService.insertWUserWorks(wUserWorks));
    }

    /**
     * 提交评论
     */
    @PostMapping(value = "/sendComment")
    public AjaxResult sendComment(@RequestBody Comment comment) {
        String userId = JwtUtils.getUserId();
        comment.setUserId(userId);
        System.out.println(comment);
        return AjaxResult.success(commentService.insertComment(comment));
    }

    @GetMapping("/selectWorkImgByFavorites/{fId}")
    public AjaxResult selectWorkImgByFavorites(@PathVariable("fId") Integer fId) {
        String userId = JwtUtils.getUserId();
        return AjaxResult.success(worksService.selectWorkImgByFavorites(userId, fId));
    }
    @GetMapping("/selectWorkVideoByFavorites/{fId}")
    public AjaxResult selectWorkVideoByFavorites(@PathVariable("fId") Integer fId) {
        String userId = JwtUtils.getUserId();
        return AjaxResult.success(worksService.selectWorkVideoByFavorites(userId, fId));
    }
}
