package com.ruoyi.web.controller.mmsprivate;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.StringUtils;
import com.ruoyi.common.utils.uuid.UUID;
import com.ruoyi.wallpaper.domain.WVip;
import com.ruoyi.wallpaper.service.IWVipService;
import com.ruoyi.wallpaper.util.JwtUtils;
import com.ruoyi.wallpaper.domain.WUser;
import com.ruoyi.wallpaper.service.IWUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Map;

@Api
@RestController
@RequestMapping("/dev-api/private/myuser")
public class UserInfo extends BaseController {
    @Autowired
    IWUserService userService;
    @Autowired
    IWVipService vipService;

    @ApiOperation("根据token获取用户信息")
    @PostMapping("/getUserInfoByToken")
    private AjaxResult getUserInfoByToken(@RequestBody String token, HttpServletRequest request) throws UnsupportedEncodingException {
        if (StringUtils.isEmpty(token)) {
            token = request.getHeader("token");
        }
        AjaxResult success = AjaxResult.success();

        Map<String, String> tokenInfo = JwtUtils.getTokenInfo(token);
        String userId = tokenInfo.get("id");
        WUser wUser = userService.selectWUserByUserId(userId);
        WVip wVip = vipService.selectWVipByUserId(userId);
        success.put("userVip", wVip);
        boolean isVip = false;
        String datePoor = "";
        if (wVip != null) {
            boolean timeOut = DateUtils.isTimeOut(wVip.getEndTime(), DateUtils.getNowDate());
            if (timeOut) {
                isVip = true;
                datePoor = DateUtils.getDatePoor(wVip.getEndTime(), DateUtils.getNowDate());
            }
        }
        success.put("isVip", isVip);
        success.put("datePoor", datePoor);
        success.put("userInfo", wUser);
        return success;
    }

    @ApiOperation("修改密码")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "userId", value = "用户id", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "userName", value = "用户账号", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "password", value = "用户密码", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "nickName", value = "用户昵称", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "wechat", value = "用户微信", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "phone", value = "手机号", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "idCart", value = "用户身份证", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "birthday", value = "用户生日", dataType = "Date", dataTypeClass = Date.class),
            @ApiImplicitParam(name = "alipay", value = "支付宝账号", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "email", value = "用户邮箱", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "sex", value = "用户性别（0男 1女 2未知）", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "avatar", value = "头像地址", dataType = "String", dataTypeClass = String.class)
    })
    @PostMapping("/updateUserInfo")
    private AjaxResult updatePass(@RequestBody WUser wUser) {
        String userId = JwtUtils.getUserId();
        if (wUser.getPassword() != null) {
            wUser.setPassword(DigestUtils.md5DigestAsHex(wUser.getPassword().getBytes()));
        }
        wUser.setUserId(userId);
        return toAjax(userService.updateWUser(wUser));
    }

    @PostMapping("/bind")
    private AjaxResult bind(@RequestBody WUser wUser) {
        String userId = JwtUtils.getUserId();

        wUser.setPassword(DigestUtils.md5DigestAsHex(wUser.getPassword().getBytes()));
//        userService.updateWUser()
        String id = UUID.getId();
        if (null == wUser.getUserId()) {
            wUser.setUserId(id);
        }
        if (null == wUser.getNickName()) {
            wUser.setNickName("bzl_" + id);
        }
        return toAjax(userService.insertWUser(wUser));
    }

}
