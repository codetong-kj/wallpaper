package com.ruoyi.web.controller.api;

import com.alibaba.fastjson.JSONObject;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.DateUtils;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.wallpaper.domain.*;
import com.ruoyi.wallpaper.service.IIpLocationService;
import com.ruoyi.wallpaper.service.IWBuriedDayLiveService;
import com.ruoyi.wallpaper.service.IWBuriedLoginService;
import com.ruoyi.wallpaper.service.IWBuriedService;
import com.ruoyi.web.controller.util.IpUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author: weiwei
 * @Date: 2021/12/23 9:36
 * @Description: 埋点接口
 */
@Api("埋点接口")
@RestController
@RequestMapping("/dev-api/buried")
public class BuriedManage extends BaseController {
    @Autowired
    private IWBuriedService buriedService;
    @Autowired
    private IWBuriedDayLiveService buriedDayLiveService;
    @Autowired
    private IWBuriedLoginService loginService;

    @Autowired
    private IIpLocationService ipLocationService;

    /**
     * @Description 软件启动插入数据
     * @Author weiwei
     * @Date 2021/12/23 12:38
     * @Param IP cpu dpi
     * @Return json结果
     */
    @ApiOperation("软件启动")
    @PostMapping("/start")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "cpu", value = "CPU", dataType = "String", dataTypeClass = String.class),
            @ApiImplicitParam(name = "dpi", value = "分辨率", dataType = "String", dataTypeClass = String.class)
    })
    public AjaxResult start(String cpu, String dpi) {
        HttpServletRequest request = ServletUtils.getRequest();
        String remoteAddr = IpUtils.getRemoteAddr(request);
        WBuried buried = new WBuried();
        WBuriedDayLive buriedDayLive = new WBuriedDayLive();
        System.out.println("获取到的IP为：" + remoteAddr);
        //根据IP判断当天是否已经启动过了
        Integer count = buriedService.getWBuriedByIp(remoteAddr);
        if (count <= 0) {
            try {
                buried.setIp(remoteAddr);
                buriedDayLive.setCpu(cpu);
                buriedDayLive.setDpi(dpi);
                buriedDayLive.setStartTime(DateUtils.getNowDate());
            } catch (Exception e) {
                e.printStackTrace();
            }
            int i = buriedService.insertWBuried(buried);
            if (i > 0) {
                buriedDayLive.setbId(buried.getId());
                //插入IP地域表
                int k = buriedDayLiveService.insertWBuriedDayLive(buriedDayLive);

                String ipLocationJson = IpUtils.getIpLocation(remoteAddr);
                if (ipLocationJson != null) {
                    ipLocationJson = ipLocationJson.replace("regionName", "province");
                    IpLocation ipLocation = JSONObject.parseObject(ipLocationJson, IpLocation.class);
                    ipLocation.setbId(buried.getId());
                    int i1 = ipLocationService.insertIpLocation(ipLocation);
                }
                return toAjax(k);
            }
        } else {
            return success("已有数据，禁止插入");
        }
        return error("插入失败");
    }

    @ApiOperation("获取用户地理位置")
    @PostMapping("/getLocation")
    public AjaxResult getLocation() {
        List<IpResult> ipResults = ipLocationService.selectLocation();
        //返回的结果
        List<Map<String,Object>> localValue=new ArrayList<>();

        Map<String,Double[]> localtions=new HashMap<>();


        for (IpResult item : ipResults
        ) {
            HashMap<String, Object> hashMap = new HashMap<>();
            hashMap.put("name",item.getCity());
            hashMap.put("value",item.getTotal());
            localValue.add(hashMap);
            Double[] doubles = new Double[2];
            doubles[0]= item.getLon();
            doubles[1]= item.getLat();
            localtions.put(item.getCity(), doubles);
        }
        Map<String, Object> result = new HashMap<>();
        result.put("localValue",localValue);
        result.put("localtions",localtions);
        return AjaxResult.success(result);
    }

    /**
     * @Description 用户登录
     * @Author weiwei
     * @Date 2021/12/23 12:52
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("用户登录")
    @PostMapping("/login")
    public AjaxResult login(String userId, Integer loginType) {
        HttpServletRequest request = ServletUtils.getRequest();
        String ip = IpUtils.getRemoteAddr(request);

        if (userId != null) {
            WBuried wBuried = new WBuried();
            wBuried.setUserId(userId);
            wBuried.setIp(ip);
            int i = buriedService.updateWBuriedByIp(wBuried);
            if (i > 0) {
                int bid = buriedService.selectWBuriedIdByUserId(userId);
                WBuriedLogin wBuriedLogin = new WBuriedLogin();
                wBuriedLogin.setLoginTime(DateUtils.getNowDate());
                wBuriedLogin.setLoginType(loginType);
                wBuriedLogin.setbId((long) bid);
                return toAjax(loginService.insertWBuriedLogin(wBuriedLogin));

            }
            return error("根据IP 修改用户id失败");
        }
        return error("用户id与IP不可为空");
    }

    /**
     * @Description 退出埋点
     * @Author weiwei
     * @Date 2021/12/23 13:11
     * @Param
     * @Return
     * @Exception
     */
    @ApiOperation("用户退出埋点")
    @PostMapping("/exit")
    public AjaxResult exit(String userId) {
        return toAjax(loginService.updateExitTime(userId));
    }


    @ApiOperation("软件关闭")
    @PostMapping("/end")
    public AjaxResult updateBuried(String userId) {
        HttpServletRequest request = ServletUtils.getRequest();
        String ip = IpUtils.getRemoteAddr(request);
        return toAjax(buriedDayLiveService.updateBuriedEndTime(userId, ip));
    }


}
