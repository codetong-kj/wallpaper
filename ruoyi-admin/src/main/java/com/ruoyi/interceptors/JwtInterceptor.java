package com.ruoyi.interceptors;

import com.auth0.jwt.exceptions.AlgorithmMismatchException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.wallpaper.util.JwtUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author: weiwei
 * @Date: 2022/1/27 17:57
 * @Description: 拦截器
 */
public class JwtInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        String msg = "";

        try {
            JwtUtils.verify(token);
            return true;
        } catch (SignatureVerificationException e) {
            e.printStackTrace();
            msg = "无效签名！请重新登录。";

        } catch (TokenExpiredException e) {
            e.printStackTrace();
            msg = "登录过期！请重新登录。";

        } catch (AlgorithmMismatchException e) {
            e.printStackTrace();
            msg = "登录算法不一致！请重新登录。";
        } catch (Exception e) {
            e.printStackTrace();
            msg = "登录无效！请重新登录。";
        }
        response.setContentType("application/json; charset=UTF-8");
        //设置状态
        AjaxResult ajaxResult = new AjaxResult(403, msg);
        response.getWriter().println(new ObjectMapper().writeValueAsString(ajaxResult));
        return false;
    }
}
