import com.ruoyi.common.utils.RedisKeyUtils;
import com.ruoyi.common.utils.VerifyCodeUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.data.redis.core.RedisTemplate;

public class MyTest {
//    RedisTemplate redisTemplate=new RedisTemplate();
    @Test
    public void t1() {
       /* String key = RedisKeyUtils.getLikedKey("491eb4a13928", "1bbf61394c9d");
        Integer integer = (Integer) redisTemplate.opsForHash().get("like:" + RedisKeyUtils.MAP_KEY_WORK_LIKED, key);
        System.out.println(integer);*/
        StringBuffer stringBuffer = new StringBuffer();
        StringBuilder stringBuilder = new StringBuilder();

        new Thread(()->System.out.println("abc")).start();
    }

    /**
     * @Description 第三题
     * @Author weiwei
     * @Date 2022/3/24 19:55
     * @Param
     * @Return
     * @Exception
     */
    public int searchInsert(int[] nums, int target) {
        int low = 0;
        int high = nums.length - 1;
        while (low <= high) {
            int mid = (high - low) / 2 + low;
            if (nums[mid] < target) {
                low = mid + 1;
            } else if (nums[low] == target) {
                return low;
            } else {
                high = mid-1;
            }
        }
        return low;


    }


    /* The isBadVersion API is defined in the parent class VersionControl.
      boolean isBadVersion(int version); */

    public int firstBadVersion(int n) {
        int low = 1;
        int high = n;
        //记录上一次的查询操作

        while (low < high) {
            int mid = (high - low) / 2 + low;
            if (isBadVersion(mid)) {
                //正确 低的向上跑
                low = mid + 1;
            } else {

                high = mid;

            }
        }
        return low;
    }

    private boolean isBadVersion(int mid) {
        return mid < 4;
    }

    /**
     * @Description 给定一个n个元素有序的（升序）整型数组nums 和一个目标值target ，写一个函数搜索nums中的 target，如果目标值存在返回下标，否则返回 -1。
     * <p>
     * 来源：力扣（LeetCode）
     * 链接：https://leetcode-cn.com/problems/binary-search
     * 著作权归领扣网络所有。商业转载请联系官方授权，非商业转载请注明出处。
     * @Author weiwei
     * @Date 2022/3/19 16:13
     * @Param
     * @Return
     * @Exception
     */
    public int search(int[] nums, int target) {
        int low = 0;
        int high = nums.length - 1;
        while (low <= high) {
            int mid = (high - low) / 2 + low;
            if (nums[mid] == target) {
                return mid;
            } else if (nums[mid] > target) {
                high = mid - 1;
            } else if (nums[mid] < target) {
                low = mid + 1;
            }
        }

        return -1;
    }
}
