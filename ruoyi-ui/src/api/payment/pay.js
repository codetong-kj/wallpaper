import request from "@/utils/request";

//获取支付接口
export function getPay(userid, type) {
  return request({
    url: '/dev-api/alipay/subscribe_vip/' + userid + '/' + type,
    method: 'get'
  })
}
//获取微信支付接口
export function getWXPay(userid, type) {
  return request({
    url: '/dev-api/wexinpay/subscribe_vip/' + userid + '/' + type,
    method: 'get',
    responseType: 'arraybuffer'
  })
}
