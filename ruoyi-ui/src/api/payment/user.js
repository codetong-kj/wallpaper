import request from "@/utils/request";

//获取用户昵称
export function getUser(userId) {
  return request({
    url:'/dev-api/user/getUserNickName/' + userId,
    method: 'get'
  })
}

