import request from '@/utils/request'

// 查询公告管理列表
export function listAnnounce(query) {
  return request({
    url: '/wallpaper/announce/list',
    method: 'get',
    params: query
  })
}

// 查询公告管理详细
export function getAnnounce(aid) {
  return request({
    url: '/wallpaper/announce/' + aid,
    method: 'get'
  })
}

// 新增公告管理
export function addAnnounce(data) {
  return request({
    url: '/wallpaper/announce',
    method: 'post',
    data: data
  })
}

// 修改公告管理
export function updateAnnounce(data) {
  return request({
    url: '/wallpaper/announce',
    method: 'put',
    data: data
  })
}

// 删除公告管理
export function delAnnounce(aid) {
  return request({
    url: '/wallpaper/announce/' + aid,
    method: 'delete'
  })
}

// 导出公告管理
export function exportAnnounce(query) {
  return request({
    url: '/wallpaper/announce/export',
    method: 'get',
    params: query
  })
}