import request from '@/utils/request'

// 查询会员管理列表
export function listVip(query) {
  return request({
    url: '/wallpaper/vip/list',
    method: 'get',
    params: query
  })
}

// 查询会员管理详细
export function getVip(vipId) {
  return request({
    url: '/wallpaper/vip/' + vipId,
    method: 'get'
  })
}

// 新增会员管理
export function addVip(data) {
  return request({
    url: '/wallpaper/vip',
    method: 'post',
    data: data
  })
}

// 修改会员管理
export function updateVip(data) {
  return request({
    url: '/wallpaper/vip',
    method: 'put',
    data: data
  })
}

// 删除会员管理
export function delVip(vipId) {
  return request({
    url: '/wallpaper/vip/' + vipId,
    method: 'delete'
  })
}

// 导出会员管理
export function exportVip(query) {
  return request({
    url: '/wallpaper/vip/export',
    method: 'get',
    params: query
  })
}