import request from '@/utils/request'

// 查询用户作品列表
export function listWorks(query) {
  return request({
    url: '/wallpaper/works/list',
    method: 'get',
    params: query
  })
}

// 查询用户作品详细
export function getWorks(workId) {
  return request({
    url: '/wallpaper/works/' + workId,
    method: 'get'
  })
}

// 新增用户作品
export function addWorks(data) {
  return request({
    url: '/wallpaper/works',
    method: 'post',
    data: data
  })
}

// 修改用户作品
export function updateWorks(data) {
  return request({
    url: '/wallpaper/works',
    method: 'put',
    data: data
  })
}

// 删除用户作品
export function delWorks(workId) {
  return request({
    url: '/wallpaper/works/' + workId,
    method: 'delete'
  })
}

// 导出用户作品
export function exportWorks(query) {
  return request({
    url: '/wallpaper/works/export',
    method: 'get',
    params: query
  })
}