import request from '@/utils/request'

// 查询文件分类列表
export function listCategory(query) {
  return request({
    url: '/wallpaper/category/list',
    method: 'get',
    params: query
  })
}

// 查询文件分类详细
export function getCategory(cId) {
  return request({
    url: '/wallpaper/category/' + cId,
    method: 'get'
  })
}

// 新增文件分类
export function addCategory(data) {
  return request({
    url: '/wallpaper/category',
    method: 'post',
    data: data
  })
}

// 修改文件分类
export function updateCategory(data) {
  return request({
    url: '/wallpaper/category',
    method: 'put',
    data: data
  })
}

// 删除文件分类
export function delCategory(cId) {
  return request({
    url: '/wallpaper/category/' + cId,
    method: 'delete'
  })
}

// 导出文件分类
export function exportCategory(query) {
  return request({
    url: '/wallpaper/category/export',
    method: 'get',
    params: query
  })
}