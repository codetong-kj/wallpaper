import request from '@/utils/request'

// 查询软件更新列表
export function listUpdate(query) {
  return request({
    url: '/wallpaper/update/list',
    method: 'get',
    params: query
  })
}

// 查询软件更新详细
export function getUpdate(updateId) {
  return request({
    url: '/wallpaper/update/' + updateId,
    method: 'get'
  })
}

// 新增软件更新
export function addUpdate(data) {
  return request({
    url: '/wallpaper/update',
    method: 'post',
    data: data
  })
}
//上传文件
export function uploadData(data){
  return request({
    url:'/wallpaper/file/upload',
    method:'post',
    data:data
  })
}

// 修改软件更新
export function updateUpdate(data) {
  return request({
    url: '/wallpaper/update',
    method: 'put',
    data: data
  })
}

// 删除软件更新
export function delUpdate(updateId) {
  return request({
    url: '/wallpaper/update/' + updateId,
    method: 'delete'
  })
}

// 导出软件更新
export function exportUpdate(query) {
  return request({
    url: '/wallpaper/update/export',
    method: 'get',
    params: query
  })
}
