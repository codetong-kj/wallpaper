import request from '@/utils/request'

// 查询用户评论列表
export function listComment(query) {
  return request({
    url: '/wallpaper/comment/list',
    method: 'get',
    params: query
  })
}

// 查询用户评论详细
export function getComment(commentId) {
  return request({
    url: '/wallpaper/comment/' + commentId,
    method: 'get'
  })
}

// 新增用户评论
export function addComment(data) {
  return request({
    url: '/wallpaper/comment',
    method: 'post',
    data: data
  })
}

// 修改用户评论
export function updateComment(data) {
  return request({
    url: '/wallpaper/comment',
    method: 'put',
    data: data
  })
}

// 删除用户评论
export function delComment(commentId) {
  return request({
    url: '/wallpaper/comment/' + commentId,
    method: 'delete'
  })
}

// 导出用户评论
export function exportComment(query) {
  return request({
    url: '/wallpaper/comment/export',
    method: 'get',
    params: query
  })
}