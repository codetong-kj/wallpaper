import request from "../../utils/request";

// 查询动态壁纸列表
export function listDynamic(query) {
  return request({
    url: '/wallpaper/dynamic/list',
    method: 'get',
    params: query
  })
}

// 查询动态壁纸详细
export function getDynamic(wId) {
  return request({
    url: '/wallpaper/dynamic/' + wId,
    method: 'get'
  })
}

// 新增动态壁纸
export function addDynamic(data) {
  return request({
    url: '/wallpaper/dynamic',
    method: 'post',
    data: data
  })
}

// 修改动态壁纸
export function updateDynamic(data) {
  return request({
    url: '/wallpaper/dynamic',
    method: 'put',
    data: data
  })
}

// 删除动态壁纸
export function delDynamic(wId) {
  return request({
    url: '/wallpaper/dynamic/' + wId,
    method: 'delete'
  })
}

// 导出动态壁纸
export function exportDynamic(query) {
  return request({
    url: '/wallpaper/dynamic/export',
    method: 'get',
    params: query
  })
}
