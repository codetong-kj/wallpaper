import request from "@/utils/request";
//查询今日启动次数
export function getStartCount(){
  return request({
    url:'/wallpaper/live/getDayStartCount',
    method:'get',
  })
}
export function getStartMoonCount(){
  return request({
    url:'/wallpaper/live/getStartMoonCount',
    method:'get',
  })
}
export function getLocation(){
  return request({
    url:'/wallpaper/location/getLocation',
    method:'get',
  })
}
export function getUserCount(){
  return request({
    url:'/dev-api/user/getUserCount',
    method:'get',
  })
}
