import request from "../../../utils/request";

//获取分类列表
export function listClazz() {
  return request({
    url: '/clazzManage/list',
    method: 'get'

  })

}
export function addOrPut(data){
  return request({
    url:'/clazzManage/put',
    method:'put',
    data:data
  })

}

