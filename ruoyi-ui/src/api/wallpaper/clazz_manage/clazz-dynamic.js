import request from "../../../utils/request";

//获取动态壁纸列表
export function listDynamic(query) {
  return request({
    url: '/clazz/dynamic/list',
    method: 'get',
    params: query
  })
}
//获取静态壁纸列表
export function listStatic(query) {
  return request({
    url: '/clazz/dynamic/getStaticList',
    method: 'get',
    params: query
  })
}

// 修改动态壁纸
export function updateDynamic(data) {
  return request({
    url: '/clazz/dynamic/update',
    method: 'put',
    data: data
  })
}

// 修改动态壁纸
export function updateDynamicList(data) {
  return request({
    url: '/clazz/dynamic/updateList',
    method: 'put',
    data: data
  })
}
