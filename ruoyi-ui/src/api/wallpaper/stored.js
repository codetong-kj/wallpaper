import request from '@/utils/request'

// 查询用户壁纸库存中间列表
export function listStored(query) {
  return request({
    url: '/wallpaper/stored/list',
    method: 'get',
    params: query
  })
}

// 查询用户壁纸库存中间详细
export function getStored(wPid) {
  return request({
    url: '/wallpaper/stored/' + wPid,
    method: 'get'
  })
}

// 新增用户壁纸库存中间
export function addStored(data) {
  return request({
    url: '/wallpaper/stored',
    method: 'post',
    data: data
  })
}

// 修改用户壁纸库存中间
export function updateStored(data) {
  return request({
    url: '/wallpaper/stored',
    method: 'put',
    data: data
  })
}

// 删除用户壁纸库存中间
export function delStored(wPid) {
  return request({
    url: '/wallpaper/stored/' + wPid,
    method: 'delete'
  })
}

// 导出用户壁纸库存中间
export function exportStored(query) {
  return request({
    url: '/wallpaper/stored/export',
    method: 'get',
    params: query
  })
}