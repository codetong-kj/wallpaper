import request from '@/utils/request'

// 查询静态壁纸列表
export function listStatics(query) {
  return request({
    url: '/wallpaper/statics/list',
    method: 'get',
    params: query
  })
}

// 查询静态壁纸详细
export function getStatics(wId) {
  return request({
    url: '/wallpaper/statics/' + wId,
    method: 'get'
  })
}

// 新增静态壁纸
export function addStatics(data) {
  return request({
    url: '/wallpaper/statics',
    method: 'post',
    data: data
  })
}

// 修改静态壁纸
export function updateStatics(data) {
  return request({
    url: '/wallpaper/statics',
    method: 'put',
    data: data
  })
}

// 删除静态壁纸
export function delStatics(wId) {
  return request({
    url: '/wallpaper/statics/' + wId,
    method: 'delete'
  })
}

// 导出静态壁纸
export function exportStatics(query) {
  return request({
    url: '/wallpaper/statics/export',
    method: 'get',
    params: query
  })
}